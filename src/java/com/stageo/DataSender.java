package com.stageo;

import java.util.Map;

/**
 *
 * @author Dave
 */
public interface DataSender {
    public void setData(Map<String, Object> data);
}
