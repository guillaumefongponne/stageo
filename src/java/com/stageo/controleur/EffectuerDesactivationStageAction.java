package com.stageo.controleur;

import com.stageo.DataSender;
import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import com.stageo.Action;
import com.util.Util;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import com.stageo.model.Utilisateur;
import com.stageo.dao.UtilisateurDAO;
import com.stageo.model.OffreStage;
import com.stageo.dao.StageDAO;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;

/**
 *
 * @author JP
 */
public class EffectuerDesactivationStageAction implements Action, RequestAware, RequirePRGAction, SessionAware, DataSender {

    private HttpServletRequest request;
    private HttpSession session;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {

        String action = "*.do?tache=afficherPageListeStage";
        String idOffre = request.getParameter("idOffre");

        System.out.println(request.getParameter("idOffre"));
        if ((request.getParameter("idOffre") != null)) {

            try {

                Connection cnx = Connexion.getInstance();
                session = request.getSession(true);

                StageDAO stageDao = new StageDAO(cnx);
                OffreStage stage = new OffreStage();
                stage.setIdOffre(idOffre);
                stage.setStatutOffre(false);
                stageDao.updateStatutOffre(stage);

                data.put("message", "Le stage <strong>" + stage.getTitre() + ")</strong> a bien été créé.");
                data.put("messageType", "Succes");

                System.out.println(request.getParameter("statutOffre"));

                System.out.println("Stage a ete accepte");
            } catch (SQLException ex) {
                Logger.getLogger(EffectuerDesactivationStageAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return action;

    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }
}
