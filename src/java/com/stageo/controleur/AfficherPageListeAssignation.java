/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.Action;
import com.stageo.DataReceiver;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tchui
 */
public class AfficherPageListeAssignation implements Action, RequestAware, SessionAware, DataReceiver {

    private HttpServletRequest request;
    private HttpServletResponse response;
     private HttpSession session;
     
    @Override
    public String execute() {
        if(session.getAttribute("connecte") != null && session.getAttribute("role") != null)
            request.setAttribute("vue", "liste-assignations-coordonnateur"+".jsp");
        
        return "/index.jsp";
    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }
    
}
