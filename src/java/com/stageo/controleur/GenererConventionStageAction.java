/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.Action;
import com.stageo.DataSender;
import com.stageo.RequestAware;
import com.stageo.RequirePRGAction;
import com.stageo.SessionAware;
import com.stageo.dao.CandidatureDAO;
import com.stageo.dao.ProfesseurDAO;
import com.stageo.jdbc.Connexion;
import com.stageo.model.Candidature;
import com.stageo.model.Coordonnateur;
import com.stageo.model.Professeur;
import com.stageo.services.ConventionStageService;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Guillaume
 */
public class GenererConventionStageAction implements Action, RequirePRGAction, RequestAware, SessionAware, DataSender{

    private HttpSession session;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HashMap data;
    
    @Override
    public String execute() {
        String action = "*.do?tache=afficherPageListeAssignation";
        String idOffre = request.getParameter("idOffre");
        String idEtudiant = request.getParameter("idEtudiant");
        
        try {
            Connection cnx = Connexion.getInstance();
            CandidatureDAO canDAO = new CandidatureDAO(cnx);
            ProfesseurDAO profDAO = new ProfesseurDAO(cnx);
            
            Candidature candidature = canDAO.read(idEtudiant, idOffre);
            Professeur coordonnateur = profDAO.read((String)session.getAttribute("connecte"));
            
            ConventionStageService conventionStage = new ConventionStageService(candidature, coordonnateur);
            if (conventionStage.generer()){
                data.put("message", "La génération de la convention s'est effectuée avec succès.");
                data.put("messageType", "Succes");
            }
            else {
                data.put("message", "Une erreur est survenue lors de la génération de la convention de stage.");
                data.put("messageType", "Echec");
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(GenererConventionStageAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GenererConventionStageAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        data.put("tacheEffectuee", true);
        return action;
    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }
    
}
