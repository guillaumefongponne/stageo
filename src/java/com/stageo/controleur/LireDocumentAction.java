/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.DataSender;
import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import com.stageo.Action;
import com.stageo.dao.DocumentDAO;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;

/**
 *
 * @author christopher, adapté du code de moumene aka the champ
*/
public class LireDocumentAction implements Action, RequestAware, SessionAware, RequirePRGAction, DataSender {

    private HttpSession session;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HashMap data;
    
    @Override
    public String execute() {
        String action=".do?tache=afficherPageDocument";
        String id = request.getParameter("id");
        InputStream fLecture = null;
        OutputStream fEcriture = null;
        try
        {
            Connection cnx = Connexion.getInstance();
                        
            PreparedStatement pstm = cnx.prepareStatement("SELECT * FROM document WHERE `ID_DOCUMENT` = ?");
            pstm.setString(1, id);
            
            ResultSet res = pstm.executeQuery();
            
            if (res.next()) {
                fLecture = res.getBinaryStream("FICHIER");
                fEcriture = response.getOutputStream();
                int n;
                byte[] buffer = new byte[1024];
                while ((n=fLecture.read(buffer)) > 0) {
                    fEcriture.write(buffer,0,n);
                }
            }          
            PreparedStatement pstm2 = cnx.prepareStatement("UPDATE document SET `NB_VUES` = ? WHERE `ID_DOCUMENT` = ?");
            pstm2.setInt(1, res.getInt("NB_VUES")+1);
            pstm2.setString(2, res.getString("ID_DOCUMENT"));
            pstm2.executeUpdate();
            
            //cnx.close();
            fLecture.close();
            fEcriture.close();
        } catch (SQLException | IOException ex) {
            request.setAttribute("MESSAGE", "ERREUR : " + ex.getMessage());
            Logger.getLogger(LireDocumentAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return action;       
    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }
    
}
