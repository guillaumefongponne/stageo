/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.Action;
import com.stageo.DataSender;
import com.stageo.RequestAware;
import com.stageo.RequirePRGAction;
import com.stageo.model.Utilisateur;
import com.stageo.dao.UtilisateurDAO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;
import java.util.Map;

/**
 *
 * @author soleil123
 */

public class DesactiverCompteEmployeurAction  implements Action, RequestAware, RequirePRGAction, DataSender {
    private HttpServletRequest request;
    private HttpSession session;
    
    private HashMap data;
    

    @Override
    public String execute() {
        String idEmployeur = request.getParameter("idEmployeurInput");
        boolean success;
        String action = "*.do?tache=afficherPageListeEntreprises";
        
        try {
            Connection cnx = Connexion.startConnection(Config.DB_USER,Config.DB_PWD,Config.URL,Config.DRIVER);
            session = request.getSession(true);

            UtilisateurDAO userdao = new UtilisateurDAO(cnx);
            Utilisateur user = userdao.read(idEmployeur);                               
            user.setEtatCompte(false);
            userdao.update_etat_compte(user);
            
            data.put("message", "L'employeur  <strong>" + user.getNom() + "</strong> a été désactivé.");
            data.put("messageType", "Succes");
            
            data.put("tacheEffectuee",true);
                
            
        } catch (SQLException ex) {
            Logger.getLogger(DesactiverCompteEmployeurAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DesactiverCompteEmployeurAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return action;
    }
    

   

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }   
}
