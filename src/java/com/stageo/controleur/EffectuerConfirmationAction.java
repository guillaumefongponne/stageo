/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.Action;
import com.stageo.DataSender;
import com.stageo.RequestAware;
import com.stageo.RequirePRGAction;
import com.stageo.SessionAware;
import com.stageo.dao.CandidatureDAO;
import com.stageo.jdbc.Connexion;
import com.stageo.model.Candidature;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Antoine
 */
public class EffectuerConfirmationAction implements Action, RequestAware, SessionAware, RequirePRGAction, DataSender { 
    
    private HttpSession session;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HashMap data;
    
    
    /**
     *
     * @return
     */
    @Override
    public String execute() {
        
        String action = "*.do?tache=afficherPageCandidaturesEtudiant";
        String identifiant_etudiant = request.getParameter("idEtudiant");
        String identifiant_offre = request.getParameter("idOffre");
        
        try{    
            Connection cnx = Connexion.getInstance();
            CandidatureDAO dao = new CandidatureDAO(cnx);
            Candidature candidature = dao.read(identifiant_etudiant, identifiant_offre);
            
            System.out.println(candidature.getIdEtudiant());
            System.out.println(candidature.getIdOffre());
            System.out.println(candidature.getStatut());
            candidature.setStatut("Confirmer");
            dao.update(candidature);

        } catch (SQLException ex) {
            Logger.getLogger(EffectuerConfirmationAction.class.getName()).log(Level.SEVERE, null, ex);
        }
            return action;
    }
    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }
}
