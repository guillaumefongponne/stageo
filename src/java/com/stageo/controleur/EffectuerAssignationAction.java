/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.Action;
import com.stageo.DataSender;
import com.stageo.RequestAware;
import com.stageo.RequirePRGAction;
import com.stageo.SessionAware;
import com.stageo.dao.CandidatureDAO;
import com.stageo.jdbc.Connexion;
import com.stageo.model.Candidature;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tchui
 */
public class EffectuerAssignationAction implements Action, RequestAware, SessionAware, RequirePRGAction, DataSender {

    private HttpSession session;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {
        String action = "*.do?tache=afficherPageListeAssignation";
        String idProfesseur = request.getParameter("idProfesseur");
        String idEtudiant = request.getParameter("Etudiant");
        String idOffre = request.getParameter("Offre");
        try {
            Connection cnx = Connexion.getInstance();
            CandidatureDAO dao = new CandidatureDAO(cnx);
            Candidature c = new Candidature();

            c.setIdEtudiant(idEtudiant);
            c.setIdOffre(idOffre);
            c.setIdProfesseur(idProfesseur);
            c.setStatut("Accepte");

            dao.updateProfesseur(c);
            
        } catch (SQLException ex) {
            Logger.getLogger(EffectuerAssignationAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return action;
    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }

}
