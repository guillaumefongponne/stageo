package com.stageo.controleur;

import com.stageo.DataSender;
import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import com.stageo.Action;
import com.util.Util;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import com.stageo.model.Utilisateur;
import com.stageo.dao.UtilisateurDAO;
import com.stageo.model.Candidature;
import com.stageo.dao.CandidatureDAO;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;

/**
 *
 * @author JP
 */
public class EffectuerPostulationStageAction implements Action, RequestAware, RequirePRGAction, SessionAware, DataSender{
    

   private HttpServletRequest request;
    private HttpSession session;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {

        String action = "*.do?tache=afficherPageListeStage";
             
        if((request.getParameter("idOffre") != null)){
             
            try{
                
                Connection cnx = Connexion.getInstance();
                session = request.getSession(true);
                 
                UtilisateurDAO dao = new UtilisateurDAO(cnx);
                Utilisateur user = dao.read((String) session.getAttribute("connecte"));
                
                CandidatureDAO canDao = new CandidatureDAO(cnx);
                Candidature can = new Candidature();
                
                String  idOffre = request.getParameter("idOffre"),
                        nomOffre = request.getParameter("titre"),
                        idUser = user.getIdUtilisateur();
                 
             
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String dateDeCreation  = dateFormat.format(timestamp);

                can.setIdOffre(idOffre);
                can.setIdEtudiant(idUser);
                can.setDate(timestamp);
                can.setStatut("En attente");
                canDao.create(can);
                
                data.put("message", "Vous avez bien postulé pour le stage");
                data.put("messageType", "Succes");

                data.put("tacheEffectuee",true);
                
    
            } catch (SQLException ex) {
                Logger.getLogger(EffectuerPostulationStageAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return action;

    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }   
}
