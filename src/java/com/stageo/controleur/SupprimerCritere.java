/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.DataSender;
import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import com.stageo.Action;
import com.stageo.model.Critere;
import com.stageo.dao.CritereDAO;
import com.stageo.model.Utilisateur;
import com.stageo.dao.UtilisateurDAO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;

/**
 *
 * @author JP
 */
public class SupprimerCritere implements Action, RequestAware, RequirePRGAction, SessionAware, DataSender {
     private HttpServletRequest request;
    private HttpSession session;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {
        

       
        
     
        String action = "*.do?tache=afficherPageProfil";
        //request.setAttribute("vue", "accueil.jsp");

             
             
          
        
             
             
            try{
                
                Connection cnx = Connexion.getInstance();
                session = request.getSession(true);
                 
              
                
                CritereDAO critDao = new CritereDAO(cnx);
               
                         
                    
           
             
               
                critDao.delete(request.getParameter("suppressionCrit"));
                
               System.out.println("Critere A ETE Supprimer");       
            } catch (SQLException ex) {
                Logger.getLogger(EffectuerPostulationStageAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        
        return action;

    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }   
}



    

