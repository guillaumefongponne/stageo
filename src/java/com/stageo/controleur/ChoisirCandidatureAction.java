/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.Action;
import com.stageo.DataSender;
import com.stageo.RequestAware;
import com.stageo.RequirePRGAction;
import com.stageo.dao.CandidatureDAO;
import com.stageo.model.Utilisateur;
import com.stageo.dao.UtilisateurDAO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;
import com.stageo.model.Candidature;
import java.util.Map;

public class ChoisirCandidatureAction implements Action, RequestAware, RequirePRGAction, DataSender {
    
    private HttpServletRequest request;
    private HttpSession session;
    
    private HashMap data;


    @Override
    public String execute() {
        String candidatureID = request.getParameter("candidatureID");
        String offreID = request.getParameter("offreID");
        boolean success;
        String action = "*.do?tache=afficherPageCandidaturesEmployeur";
        
        try{
            Connection connect = Connexion.startConnection(Config.DB_USER, Config.DB_PWD, Config.URL, Config.DRIVER);
            session = request.getSession(true);
            
            CandidatureDAO candidature = new CandidatureDAO(connect);
            Candidature candidat = candidature.read(candidatureID,offreID);
            
            candidat.setStatut("Accepte");
            candidature.update(candidat);
            
            data.put("message", "Le candidat a été choisi.");
            data.put("messageType", "Succes");
            
            data.put("tacheEffectuee",true);
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ChoisirCandidatureAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ChoisirCandidatureAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return action;
    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;

    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }   
}