/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;
import com.stageo.DataSender;
import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import com.stageo.Action;
import com.stageo.dao.AdresseDAO;
import com.stageo.dao.CompagnieDAO;
import com.stageo.model.Critere;
import com.stageo.dao.CritereDAO;
import com.stageo.dao.EmployeurDAO;
import com.stageo.model.Etudiant;
import com.stageo.dao.EtudiantDAO;
import com.stageo.model.Professeur;
import com.stageo.dao.ProfesseurDAO;
import com.util.Util;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import com.stageo.model.Utilisateur;
import com.stageo.dao.UtilisateurDAO;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;
import com.stageo.model.Adresse;
import com.stageo.model.Compagnie;
import com.stageo.model.Employeur;

/**
 *
 * @author JP
 */
public class EffectuerModificationUtilisateurAction implements Action, RequestAware, RequirePRGAction, SessionAware, DataSender{
    

    private HttpServletResponse response;
    private HttpServletRequest request;
    private HttpSession session;
    private HashMap data;

    @Override
    public String execute() {
        String action = "*.do?tache=afficherPageProfil";
        session = request.getSession(true);
        
        String idUtilisateur = (String)session.getAttribute("connecte"),
               email = request.getParameter("email"),
               prenom = request.getParameter("prenom"),
               nom = request.getParameter("nom"),
               role = (String) session.getAttribute("role"),
               statutRecherche= request.getParameter("statutRecherche"),
               telephone = request.getParameter("tel");
               List<Critere> listeCritereEtudiant= new ArrayList<>();
               List<Critere> ListeCritere = null;
               
                          
        boolean erreurSurvenue = false;
        try {
            Connection cnx = Connexion.getInstance();

            UtilisateurDAO dao = new UtilisateurDAO(cnx);
            Utilisateur user = dao.read(idUtilisateur);
            boolean success = false;
            
            switch (role){
                case "etudiant" :
                    EtudiantDAO etuDAO = new EtudiantDAO(cnx);
                    Etudiant etudiant = etuDAO.read(idUtilisateur);
                    System.out.println("Je suis un etudiant");

                    etudiant.setEmail(email);
                    etudiant.setNom(nom);
                    etudiant.setPrenom(prenom);
                    etudiant.setVille(request.getParameter("ville"));
                    etudiant.setTelephone(telephone);
                    etudiant.setProfilEtudes(request.getParameter("profilEtudes"));
                    etudiant.setStageSession(request.getParameter("stageSession"));
                    etudiant.setAnglaisEcrit(request.getParameter("anglaisEcrit"));
                    etudiant.setAnglaisParle(request.getParameter("anglaisParle"));
                    etudiant.setNumeroDA(request.getParameter("numeroDA"));
                    etudiant.setLogin365(request.getParameter("login365"));
                    etudiant.setDemarcheEnCours(request.getParameter("demarcheEnCours"));
                    etudiant.setPermisConduire(request.getParameter("permisConduire"));
                    etudiant.setMoyenTransport(request.getParameter("moyenTransport"));
                    etudiant.setPreferences(request.getParameter("preferences"));

                    // A corriger plus tard pendant la modification d'un compte
                    //user.setStatutRecherche(statutRecherche);

                    CritereDAO cDAO= new CritereDAO(cnx);
                    ListeCritere= cDAO.findAll();
                    for (int i = 0; i < ListeCritere.size(); i++){
                        String checked = request.getParameter(ListeCritere.get(i).getNom() + "Check");
                        //verifie que la valeur est cochée
                        if (checked != null){
                            listeCritereEtudiant.add(ListeCritere.get(i));
                        }
                    }
                    etudiant.setListeCritere(listeCritereEtudiant);
                    success = etuDAO.update(etudiant);
                    break;
                
                case "employeur" :
                    EmployeurDAO employeurDAO = new EmployeurDAO(cnx);
                    CompagnieDAO compagnieDAO = new CompagnieDAO(cnx);
                    AdresseDAO adresseDAO = new AdresseDAO(cnx);
                    
                    Employeur employeur = employeurDAO.read(idUtilisateur);
                    Compagnie compagnie = compagnieDAO.read(employeur.getCompagnie().getIdCompagnie());
                    Adresse adresse = adresseDAO.read(compagnie.getAdresse().getIdAdresse());
                    System.out.println("Je suis un employeur");
                    
                    // Preparation de l'employeur
                    employeur.setEmail(email);
                    employeur.setNom(nom);
                    employeur.setPrenom(prenom);
                    employeur.setTelephone(telephone);
                    employeur.setFonction(request.getParameter("fonction"));
                    
                    // Preparation de la compagnie
                    compagnie.setNom(request.getParameter("nomCompagnie"));
                    compagnie.setPageWeb(request.getParameter("siteWeb"));
                    compagnie.setSecteur(request.getParameter("secteur"));
                    compagnie.setTelEntreprise(request.getParameter("telEntreprise"));
                    compagnie.setLienGoogleMaps(request.getParameter("lienGoogleMaps"));
                    compagnie.setKilometrage((Integer.parseInt(request.getParameter("kilometrage"))));
                    compagnie.setTaille(request.getParameter("taille"));
                    compagnie.setNbEmployes(Integer.parseInt(request.getParameter("nbEmployes")));
                    
                    // Preparation de l'adresse
                    adresse.setAdresseLigne1(request.getParameter("adresseLigne1"));
                    adresse.setAdresseLigne2(request.getParameter("adresseLigne2"));
                    adresse.setVille(request.getParameter("villeCompagnie"));
                    adresse.setCodePostal(request.getParameter("codePostal"));
                    adresse.setProvince(request.getParameter("province"));
                    adresse.setPays(request.getParameter("pays"));
                    
                    employeur.setCompagnie(compagnie);
                    employeur.getCompagnie().setAdresse(adresse);
                    
                    success = employeurDAO.update(employeur);
                    break;
                
                case "coordonnateur" :
                    ProfesseurDAO coordonnateurDAO = new ProfesseurDAO(cnx);
                    Professeur coordonnateur = coordonnateurDAO.read(idUtilisateur);
                    System.out.println("Je suis un coordonnateur");
                    
                    coordonnateur.setEmail(email);
                    coordonnateur.setNom(nom);
                    coordonnateur.setPrenom(prenom);
                    coordonnateur.setVille(request.getParameter("ville"));
                    coordonnateur.setTelephone(telephone);
                    coordonnateur.setAnglaisEcrit(request.getParameter("anglaisEcrit"));
                    coordonnateur.setAnglaisParle(request.getParameter("anglaisParle"));
                    coordonnateur.setMoyenTransport(request.getParameter("moyenTransport"));
                    coordonnateur.setInfosPartagees(request.getParameter("infosPartagees"));
                    coordonnateur.setPreferenceSupervision(request.getParameter("preferenceSupervision"));
                    coordonnateur.setDispoStageEte(request.getParameter("dispoStageEte"));
                    
                    success = coordonnateurDAO.update(coordonnateur);
                    break;
                    
                case "professeur" :
                    ProfesseurDAO profDAO = new ProfesseurDAO(cnx);
                    Professeur professeur = profDAO.read(idUtilisateur);
                    System.out.println("Je suis un professeur");

                    professeur.setEmail(email);
                    professeur.setNom(nom);
                    professeur.setPrenom(prenom);
                    professeur.setVille(request.getParameter("ville"));
                    professeur.setTelephone(telephone);
                    professeur.setAnglaisEcrit(request.getParameter("anglaisEcrit"));
                    professeur.setAnglaisParle(request.getParameter("anglaisParle"));
                    professeur.setMoyenTransport(request.getParameter("moyenTransport"));
                    professeur.setInfosPartagees(request.getParameter("infosPartagees"));
                    professeur.setPreferenceSupervision(request.getParameter("preferenceSupervision"));
                    professeur.setDispoStageEte(request.getParameter("dispoStageEte"));

                    success = profDAO.update(professeur);
                    break;
                
                case "administrateur" :
                    user.setEmail(email);
                    user.setNom(nom);
                    user.setPrenom(prenom);
                    user.setTelephone(telephone);
                    
                    success = dao.update(user);
                    break;
            }
            
            if (success){
                data.put("message", "Le profil a été modifié avec succès.");
                data.put("messageType", "Succes");
            }
            else{
                data.put("message", "Une erreur est survenue lors de la modification du profil.");
                data.put("messageType", "Echec");
            }
            data.put("tacheEffectuee", true);
        }
        catch (SQLException ex) {
            System.out.println(ex);
            Logger.getLogger(EffectuerModificationUtilisateurAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return action;
    }
            
  

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

}
