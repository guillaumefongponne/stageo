/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.io.InputStream;
import java.util.UUID;

/**
 *
 * @author JP
 */
public class CV {

     private String idCv, //clé primaire
                langue,
                idEtudiant,
                date; // CURRENT_TIMESTAMP
     private int nbVues;
     private InputStream fichier;
     
     public CV(){}
                 
     public CV(String langue,String idEtudiant,String date,int nbVues, InputStream fichier){
            String uniqueID = UUID.randomUUID().toString();
            this.idCv = uniqueID;
            this.langue = langue;
            this.idEtudiant = idEtudiant;
            this.date = date; 
            this.nbVues = nbVues;
            this.fichier = fichier;
            }
    
     public CV(String idCv,String langue,String idEtudiant,String date,int nbVues, InputStream fichier){
            this.idCv = idCv;
            this.langue = langue;
            this.idEtudiant = idEtudiant;
            this.date = date; 
            this.nbVues = nbVues;
            this.fichier = fichier;
            }
   

    public String getIdCv() {
        return idCv;
    }

    public void setIdCv(String idCv) {
        this.idCv = idCv;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public String getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(String idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNbVues() {
        return nbVues;
    }

    public void setNbVues(int nbVues) {
        this.nbVues = nbVues;
    }

    public InputStream getFichier() {
        return fichier;
    }

    public void setFichier(InputStream fichier) {
        this.fichier = fichier;
    }
   
}

