/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.UUID;

/**
 *
 * @author JP
 */
public class Adresse {

    private String idAdresse, adresseLigne1, adresseLigne2, ville, province, codePostal, pays;
    
    @Override
    public String toString() {
        String adresse = this.adresseLigne1+" "+this.adresseLigne2+" "+this.ville+" "+this.codePostal+" "+this.province+" "+this.pays;
        return adresse;
    }

    //Constructeurs
    public Adresse() {
        String uniqueID = UUID.randomUUID().toString();
        this.idAdresse = uniqueID;
    }

    public Adresse(String adresseLigne1, String adresseLigne2, String ville, String province, String codePostal, String pays) {
        String uniqueID = UUID.randomUUID().toString();
        this.idAdresse = uniqueID;
        this.adresseLigne1 = adresseLigne1;
        this.adresseLigne2 = adresseLigne2;
        this.ville = ville;
        this.codePostal = codePostal;
        this.province = province;
        this.pays = pays;
    }

    public Adresse(String idAdresse, String adresseLigne1, String adresseLigne2, String ville, String province, String codePostal, String pays) {
        this.idAdresse = idAdresse;
        this.adresseLigne1 = adresseLigne1;
        this.adresseLigne2 = adresseLigne2;
        this.ville = ville;
        this.codePostal = codePostal;
        this.province = province;
        this.pays = pays;
    }

    public String getIdAdresse() {
        return idAdresse;
    }

    public void setIdAdresse(String idAdresse) {
        this.idAdresse = idAdresse;
    }

    public String getAdresseLigne1() {
        return adresseLigne1;
    }

    public void setAdresseLigne1(String adresseLigne1) {
        this.adresseLigne1 = adresseLigne1;
    }

    public String getAdresseLigne2() {
        return adresseLigne2;
    }

    public void setAdresseLigne2(String adresseLigne2) {
        this.adresseLigne2 = adresseLigne2;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

}
