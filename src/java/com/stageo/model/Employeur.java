/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

/**
 *
 * @author guill
 */
public class Employeur extends Utilisateur{
    private Compagnie compagnie;
    private String fonction;
    private String profilCree, profilModifie;
    
    public Employeur(){
        super();
        this.compagnie = new Compagnie();
    }
    
    public Employeur( String idUtilisateur,
                        String email,
                        String nomUtilisateur,
                        String motDePasse,
                        String nom,
                        String prenom,
                        String role){
        super(idUtilisateur, email, nomUtilisateur, motDePasse, nom, prenom, role);
        this.compagnie = new Compagnie();
    }
    
    public Employeur( String email,
                        String nomUtilisateur,
                        String motDePasse,
                        String nom,
                        String prenom,
                        String role){
        super(email, nomUtilisateur, motDePasse, nom, prenom, role);
        this.compagnie = new Compagnie();
    }
    
    public Compagnie getCompagnie() {
        return compagnie;
    }

    public void setCompagnie(Compagnie compagnie) {
        this.compagnie = compagnie;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public String getProfilCree() {
        return profilCree;
    }

    public void setProfilCree(String profilCree) {
        this.profilCree = profilCree;
    }

    public String getProfilModifie() {
        return profilModifie;
    }

    public void setProfilModifie(String profilModifie) {
        this.profilModifie = profilModifie;
    }
    
}
