/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author guill
 */
public class Etudiant extends Utilisateur{
    private CV cv;
    private String statutRecherche = "en cours";
    private List<Critere> listeCritere;
    private List<Candidature> liste_candidature;
    
    private String ville, profilEtudes, stageSession, anglaisEcrit, anglaisParle;
    private String permisConduire, demarcheEnCours;
    private String login365;
    private String numeroDA, moyenTransport, preferences;
    private String profilCree, profilModifie;
    
    public Etudiant(){
        super();
        this.listeCritere = new LinkedList<Critere>();
        this.liste_candidature = new LinkedList<Candidature>();
    }
    
    public Etudiant(String idUtilisateur,
                        String email,
                        String nomUtilisateur,
                        String motDePasse,
                        String nom,
                        String prenom,
                        String role){
        super(idUtilisateur, email, nomUtilisateur, motDePasse, nom, prenom, role);
        this.listeCritere = new LinkedList<Critere>();
        this.liste_candidature = new LinkedList<Candidature>();
    }
    
    public Etudiant( String email,
                        String nomUtilisateur,
                        String motDePasse,
                        String nom,
                        String prenom,
                        String role){
        super(email, nomUtilisateur, motDePasse, nom, prenom, role);
        this.listeCritere = new LinkedList<Critere>();
        this.liste_candidature = new LinkedList<Candidature>();
    }
    
    public CV getCv() {
        return cv;
    }

    public void setCv(CV cv) {
        this.cv = cv;
    }
    
    public String getStatutRecherche() {
        return statutRecherche;
    }

    public void setStatutRecherche(String statutRecherche) {
        this.statutRecherche = statutRecherche;
    }
    
    public List<Critere> getListeCritere() {
        return listeCritere;
    }

    public void setListeCritere(List<Critere> liste_critere) {
        this.listeCritere = liste_critere;
    }
    
    public List<Candidature> getListe_candidature() {
        return liste_candidature;
    }

    public void setListe_candidature(List<Candidature> liste_candidature) {
        this.liste_candidature = liste_candidature;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getProfilEtudes() {
        return profilEtudes;
    }

    public void setProfilEtudes(String profilEtudes) {
        this.profilEtudes = profilEtudes;
    }

    public String getStageSession() {
        return stageSession;
    }

    public void setStageSession(String stageSession) {
        this.stageSession = stageSession;
    }

    public String getAnglaisEcrit() {
        return anglaisEcrit;
    }

    public void setAnglaisEcrit(String anglaisEcrit) {
        this.anglaisEcrit = anglaisEcrit;
    }

    public String getAnglaisParle() {
        return anglaisParle;
    }

    public void setAnglaisParle(String anglaisParle) {
        this.anglaisParle = anglaisParle;
    }

    public String getPermisConduire() {
        return permisConduire;
    }

    public void setPermisConduire(String permisConduire) {
        this.permisConduire = permisConduire;
    }

    public String getDemarcheEnCours() {
        return demarcheEnCours;
    }

    public void setDemarcheEnCours(String demarcheEnCours) {
        this.demarcheEnCours = demarcheEnCours;
    }

    public String getLogin365() {
        return login365;
    }

    public void setLogin365(String login365) {
        this.login365 = login365;
    }

    public String getNumeroDA() {
        return numeroDA;
    }

    public void setNumeroDA(String numeroDA) {
        this.numeroDA = numeroDA;
    }

    public String getMoyenTransport() {
        return moyenTransport;
    }

    public void setMoyenTransport(String moyenTransport) {
        this.moyenTransport = moyenTransport;
    }

    public String getPreferences() {
        return preferences;
    }

    public void setPreferences(String preferences) {
        this.preferences = preferences;
    }

    public String getProfilCree() {
        return profilCree;
    }

    public void setProfilCree(String profilCree) {
        this.profilCree = profilCree;
    }

    public String getProfilModifie() {
        return profilModifie;
    }

    public void setProfilModifie(String profilModifie) {
        this.profilModifie = profilModifie;
    }
    
    
}
