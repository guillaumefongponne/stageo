/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.UUID;

/**
 *
 * @author JP
 */
public class Notification { 
    private String IdNotification, // Cle primaire
            type,
            message,
            IdCoordonnateur,
            date,
            heure;
    
    private int vue;

    public Notification() {
    }
    
    public Notification(String IdNotification, String type, String message, String IdCoordonnateur, String date, String heure, int vue) {
        this.IdNotification = IdNotification;
        this.type = type;
        this.message = message;
        this.IdCoordonnateur = IdCoordonnateur;
        this.date = date;
        this.heure = heure;
        this.vue = vue;
    }

    public Notification(String type, String message, String IdCoordonnateur, String date, String heure, int vue) {
        String uniqueID = UUID.randomUUID().toString();
        this.IdNotification = uniqueID;
        this.type = type;
        this.message = message;
        this.IdCoordonnateur = IdCoordonnateur;
        this.date = date;
        this.heure = heure;
        this.vue = vue;
    }

    public String getIdNotification() {
        return IdNotification;
    }

    public void setIdNotification(String IdNotification) {
        this.IdNotification = IdNotification;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdCoordonnateur() {
        return IdCoordonnateur;
    }

    public void setIdCoordonnateur(String IdCoordonnateur) {
        this.IdCoordonnateur = IdCoordonnateur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public int getVue() {
        return vue;
    }

    public void setVue(int vue) {
        this.vue = vue;
    }
    

    
    
    
}
