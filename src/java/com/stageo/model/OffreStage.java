/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author AC
 */
public class OffreStage {
    private String idOffre, titre, description, lienWeb, lienDocument, idEmployeur;
    private int nbVues = 0,dureeEnJours = 0, remunere = 0, active = 1, statut = 0;
    private Timestamp date = new Timestamp(System.currentTimeMillis()), dateDebut, dateFin;
    private InputStream fichier;
    private List<Critere> listeCritere;
    private boolean statutOffre;

    public OffreStage(String titre, String description, String lienWeb, String lienDocument, String idEmployeur, Timestamp dateDebut, Timestamp dateFin, Timestamp date, int nbVues, int active) {
        String uniqueID = UUID.randomUUID().toString();
        this.idOffre = uniqueID;
        this.titre = titre;
        this.description = description;
        this.lienWeb = lienWeb;
        this.lienDocument = lienDocument;
        this.idEmployeur = idEmployeur;
        this.date = date;
        this.nbVues = nbVues;
        this.active = active;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.listeCritere = new ArrayList<Critere>();
    }
    
    public OffreStage(){
    String uniqueID = UUID.randomUUID().toString();
    this.idOffre = uniqueID;
    Timestamp dateDebut = new Timestamp(System.currentTimeMillis());
    }

    public boolean isStatutOffre() {
        return statutOffre;
    }

    public void setStatutOffre(boolean statutOffre) {
        this.statutOffre = statutOffre;
    }

    public Timestamp getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Timestamp dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Timestamp getDateFin() {
        return dateFin;
    }

    public void setDateFin(Timestamp dateFin) {
        this.dateFin = dateFin;
    }

    public int getDureeEnJours() {
        return dureeEnJours;
    }

    public void setDureeEnJours(int dureeEnJours) {
        this.dureeEnJours = dureeEnJours;
    }

    public int getRemunere() {
        return remunere;
    }

    public void setRemunere(int remunere) {
        this.remunere = remunere;
    }

    public String getIdOffre() {
        return idOffre;
    }

    public void setIdOffre(String idOffre) {
        this.idOffre = idOffre;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLienWeb() {
        return lienWeb;
    }

    public void setLienWeb(String lienWeb) {
        this.lienWeb = lienWeb;
    }

    public String getLienDocument() {
        return lienDocument;
    }

    public void setLienDocument(String lienDocument) {
        this.lienDocument = lienDocument;
    }

    public String getIdEmployeur() {
        return idEmployeur;
    }

    public void setIdEmployeur(String idEmployeur) {
        this.idEmployeur = idEmployeur;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getNbVues() {
        return nbVues;
    }

    public void setNbVues(int nbVues) {
        this.nbVues = nbVues;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public InputStream getFichier() {
        return fichier;
    }

    public void setFichier(InputStream fichier) {
        this.fichier = fichier;
    }
    public List<Critere> getListeCritere() {
        return this.listeCritere;
    }

    public void setListeCritere(List<Critere> liste) {
        this.listeCritere = liste;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }   
    
}
