/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author guill
 */
public class Professeur extends Utilisateur{
    private String anglaisEcrit, anglaisParle, moyenTransport, infosPartagees, preferenceSupervision;
    private String dispoStageEte, ville;
    private String profilCree;
    private String profilModifie;
    
    private List<Etudiant> listeEtudiantSupervise;

    public Professeur(String idUtilisateur, String email, String nomUtilisateur, String motDePasse, String nom, String prenom, String role) {
        super(idUtilisateur, email, nomUtilisateur, motDePasse, nom, prenom, role);
        listeEtudiantSupervise = new ArrayList<>();
    }

    public Professeur(String email, String nomUtilisateur, String motDePasse, String nom, String prenom, String role) {
        super(email, nomUtilisateur, motDePasse, nom, prenom, role);
        listeEtudiantSupervise = new ArrayList<>();
    }

    public Professeur() {
        super();
        listeEtudiantSupervise = new ArrayList<>();
    }

    public String getAnglaisEcrit() {
        return anglaisEcrit;
    }

    public void setAnglaisEcrit(String anglaisEcrit) {
        this.anglaisEcrit = anglaisEcrit;
    }

    public String getAnglaisParle() {
        return anglaisParle;
    }

    public void setAnglaisParle(String anglaisParle) {
        this.anglaisParle = anglaisParle;
    }

    public String getMoyenTransport() {
        return moyenTransport;
    }

    public void setMoyenTransport(String moyenTransport) {
        this.moyenTransport = moyenTransport;
    }

    public String getInfosPartagees() {
        return infosPartagees;
    }

    public void setInfosPartagees(String infosPartagees) {
        this.infosPartagees = infosPartagees;
    }

    public String getPreferenceSupervision() {
        return preferenceSupervision;
    }

    public void setPreferenceSupervision(String preferenceSupervision) {
        this.preferenceSupervision = preferenceSupervision;
    }

    public String getDispoStageEte() {
        return dispoStageEte;
    }

    public void setDispoStageEte(String dispoStageEte) {
        this.dispoStageEte = dispoStageEte;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getProfilCree() {
        return profilCree;
    }

    public void setProfilCree(String profilCree) {
        this.profilCree = profilCree;
    }

    public String getProfilModifie() {
        return profilModifie;
    }

    public void setProfilModifie(String profilModifie) {
        this.profilModifie = profilModifie;
    }

    public List<Etudiant> getListeEtudiantSupervise() {
        return listeEtudiantSupervise;
    }

    public void setListeEtudiantSupervise(List<Etudiant> listeEtudiantSupervise) {
        this.listeEtudiantSupervise = listeEtudiantSupervise;
    }
    
    
    
}
