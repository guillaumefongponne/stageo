/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Dave
 */

public class Utilisateur {
       

    // Attributs propres à un utilisateur
    private String idUtilisateur,                  // clé primaire
                   nomUtilisateur,
                   motDePasse,
                   email,
                   nom,
                   prenom,
                   role, 
                   telephone;
    private Boolean etatCompte;
    
    private List<OffreStage> liste_offreStage;    
    
    // Constructeur sans argument
    public Utilisateur(){
        String uniqueID = UUID.randomUUID().toString();
        
        this.idUtilisateur = uniqueID;
        this.liste_offreStage = new LinkedList<OffreStage>();
    }
    
    // Constructeur AVEC ID
    public Utilisateur( String idUtilisateur,
                        String email,
                        String nomUtilisateur,
                        String motDePasse,
                        String nom,
                        String prenom,
                        String role){
        this.idUtilisateur = idUtilisateur;
        this.email = email;
        this.nomUtilisateur = nomUtilisateur;
        this.motDePasse = motDePasse;
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
        this.liste_offreStage = new LinkedList<OffreStage>();        
    }
    
    // Constructeur SANS ID
    public Utilisateur( String email,
                        String nomUtilisateur,
                        String motDePasse,
                        String nom,
                        String prenom,
                        String role){
        
        String uniqueID = UUID.randomUUID().toString();
        
        this.idUtilisateur = uniqueID;
        this.email = email;
        this.nomUtilisateur = nomUtilisateur;
        this.motDePasse = motDePasse;
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
        this.liste_offreStage = new LinkedList<OffreStage>();

    }

   public String getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(String idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public void setNomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public List<OffreStage> getListe_offreStage() {
        return liste_offreStage;
    }

    public void setListe_offreStage(List<OffreStage> liste_offreStage) {
        this.liste_offreStage = liste_offreStage;
    }
    
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }    

    public Boolean getEtatCompte() {
        return etatCompte;
    }

    public void setEtatCompte(Boolean etatCompte) {
        this.etatCompte = etatCompte;
    }
    
      
}
