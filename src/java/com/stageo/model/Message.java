/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.UUID;

/**
 *
 * @author Chris
 */
public class Message {
    
    private String idMessage,titre,message,idExpediteur, date, heure;
    private Boolean vu;
    
    // Peut-être un autre constructeur pour spécifier un id?
    public Message(){
    }
    public Message(String titre, String message, String idExpediteur, Boolean vu, String date, String heure) {
        String uniqueID = UUID.randomUUID().toString();
        this.idMessage = uniqueID;
        this.titre = titre;
        this.message = message;
        this.idExpediteur = idExpediteur;
        this.vu = vu;
        this.date = date;
        this.heure = heure;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(String idMessage) {
        this.idMessage = idMessage;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdExpediteur() {
        return idExpediteur;
    }

    public void setIdExpediteur(String idExpediteur) {
        this.idExpediteur = idExpediteur;
    }

    public Boolean getVu() {
        return vu;
    }

    public void setVu(Boolean vu) {
        this.vu = vu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }
}
