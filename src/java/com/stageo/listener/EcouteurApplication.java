/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.listener;

/**
 *
 * @author Dave
 */

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import com.stageo.jdbc.Connexion;

/**
 *
 * @author usager
 */
public class EcouteurApplication implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Application démarrée");
        ServletContext application = sce.getServletContext();
        
        Connexion.setUser(application.getInitParameter("userDB"));
        Connexion.setPassword(application.getInitParameter("passwordDB"));
        Connexion.setDriver(application.getInitParameter("piloteJDBC"));
        Connexion.setUrl(application.getInitParameter("urlDb"));
        
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Application terminée");
        Connexion.close();
    }
    
}
