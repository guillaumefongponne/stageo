/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo;

import com.stageo.controleur.AccepterCandidature;
import com.stageo.controleur.ActiverCompteEmployeurAction;
import com.stageo.controleur.ActiverCompteEtudiantAction;
import com.stageo.controleur.AfficherListeCompetences;
import com.stageo.controleur.AfficherListeProfesseurs;
import com.stageo.controleur.AfficherPageCandidatures;
import com.stageo.controleur.AfficherPageCreationStage;
import com.stageo.controleur.AfficherPageDocument;
import com.stageo.controleur.AfficherPageEntreprise;
import com.stageo.controleur.AfficherPageInscription;
import com.stageo.controleur.AfficherPageListeAssignation;
import com.stageo.controleur.AfficherPageListeCompagnies;
import com.stageo.controleur.AfficherPageListeStage;
import com.stageo.controleur.AfficherPageListeStagiaires;
import com.stageo.controleur.AfficherPageProfil;
import com.stageo.controleur.AjouterCritereAction;
import com.stageo.controleur.AjouterDocumentAction;
import com.stageo.controleur.AjouterOffreDeStage;
import com.stageo.controleur.ChoisirCandidatureAction;
import com.stageo.controleur.DefaultAction;
import com.stageo.controleur.DesactiverCompteEmployeurAction;
import com.stageo.controleur.DesactiverCompteEtudiantAction;
import com.stageo.controleur.EffectuerAcceptationStageAction;
import com.stageo.controleur.EffectuerAssignationAction;
import com.stageo.controleur.EffectuerConfirmationAction;
import com.stageo.controleur.EffectuerConnexionAction;
import com.stageo.controleur.EffectuerDeconnexionAction;
import com.stageo.controleur.EffectuerDesactivationStageAction;
import com.stageo.controleur.EffectuerInscriptionAction;
import com.stageo.controleur.EffectuerModificationCompagnieAction;
import com.stageo.controleur.EffectuerModificationUtilisateurAction;
import com.stageo.controleur.EffectuerPostulationStageAction;
import com.stageo.controleur.GenererConventionStageAction;
import com.stageo.controleur.LireDocumentAction;
import com.stageo.controleur.ModifierCompteUtilisateur;
import com.stageo.controleur.ModifierDocument;
import com.stageo.controleur.ModifierOffreDeStage;
import com.stageo.controleur.ModifierProfilEtudiant;
import com.stageo.controleur.SuggererCandidature;
import com.stageo.controleur.SupprimerCritere;
import com.stageo.controleur.SupprimerDocumentAction;
import com.stageo.controleur.SupprimerOffreDeStage;

/**
 *
 * @author AC
 */
public class ActionBuilder {

    public static Action getAction(String actionName) {
       //System.out.println("je sduis dans le actionbuilder");
        //System.out.print("entrer dans l'action builder");
        if (actionName != null) {
            switch (actionName) {
                
                case "afficherPageInscription":
                    return new AfficherPageInscription();
//Stage         
                case "afficherPageListeEntreprises":
                    return new AfficherPageListeCompagnies();
                case "afficherPageListeStage":
                    return new AfficherPageListeStage();
                case "afficherPageListeProfesseurs":
                    return new AfficherListeProfesseurs();
                case "afficherPageCreationStage":
                    return new AfficherPageCreationStage();
                case "afficherPageEntreprise":
                    return new AfficherPageEntreprise();
                case "afficherPageListeStagiaires":
                    return new AfficherPageListeStagiaires();
                case "afficherListeCompetences":
                    return new AfficherListeCompetences();
                case "ajouterOffreDeStage":
                    return new AjouterOffreDeStage();
                case "ajoutCritere":
                     return new AjouterCritereAction();
                case "supprimerCritere":
                     return new SupprimerCritere();
                case "supprimerOffreDeStage":
                    return new SupprimerOffreDeStage();
                case "modifierOffreDeStage":
                    return new ModifierOffreDeStage();
                case "suggererCandidature":
                    return new SuggererCandidature();
                case "accepterCandidature":
                    return new AccepterCandidature();  
                case "effectuerPostulationAction":
                    return new EffectuerPostulationStageAction();
                case "effectuerAcceptationAction":
                    return new EffectuerAcceptationStageAction();
                case "effectuerDesactivationAction":
                    return new EffectuerDesactivationStageAction();
                case "afficherPageCandidaturesEmployeur" :
                    return new AfficherPageCandidatures();
                case "afficherPageListeAssignation" :
                    return new AfficherPageListeAssignation();
                case "effectuerAssignationAction" :
                    return new EffectuerAssignationAction();
                    
//Profil
                case "afficherPageCandidaturesEtudiant":
                    return new AfficherPageCandidatures();
                case "afficherPageProfil":
                    return new AfficherPageProfil();
                case "effectuerModificationUtilisateur":
                    return new EffectuerModificationUtilisateurAction();
                case "modifierProfilEtudiant":
                    return new ModifierProfilEtudiant();
                case "modifierCompteUtilisateur" :
                    return new ModifierCompteUtilisateur();
//Documents
                case "afficherPageDocument":
                    return new AfficherPageDocument();
                case "ajouterDocument":
                    return new AjouterDocumentAction();
                case "modifierDocument":
                    return new ModifierDocument();   
                case "supprimerDocument":
                    return new SupprimerDocumentAction(); 
                case "genererConventionStage":
                    return new GenererConventionStageAction();
                    
// Connexion/Inscription
                case "effectuerInscription":
                    return new EffectuerInscriptionAction();   
                case "effectuerConnexion":
                    return new EffectuerConnexionAction();
                case "effectuerDeconnexion":
                    return new EffectuerDeconnexionAction();
                
                // confirmation du stage
                case "effectuerConfirmation":
                    return new EffectuerConfirmationAction();
                    
                case "effectuerModificationCompagnie":
                    return new EffectuerModificationCompagnieAction();
                    
                case "desactiverCompteEtudiant" :
                    return new DesactiverCompteEtudiantAction();
                case "desactiverCompteEmployeur" :
                    return new DesactiverCompteEmployeurAction();
                case "activerCompteEtudiant" :
                    return new ActiverCompteEtudiantAction();
                case "activerCompteEmployeur" :
                    return new ActiverCompteEmployeurAction();
                    
                case "choisirCandidature" :
                    return new ChoisirCandidatureAction();

//lire document               
                case "lireDocument":
                    return new LireDocumentAction();
                    
                default:
                    return new DefaultAction();

            }
        }
        
        return new DefaultAction();

    }
}
