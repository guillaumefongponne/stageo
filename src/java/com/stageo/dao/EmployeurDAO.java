/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.dao;

import com.stageo.model.Employeur;
import com.stageo.model.Utilisateur;
import com.util.Util;

import com.util.Util;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dave
 */
public class EmployeurDAO extends DAO<Employeur> {

    public EmployeurDAO() {
    }

    public EmployeurDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Employeur x) {
        String req = "INSERT INTO employeur (`ID_EMPLOYEUR`, `ID_COMPAGNIE`, `FONCTION`, `PROFIL_CREE`) VALUES (?,?,?,?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            if (x.getIdUtilisateur() != null && !"".equals(x.getIdUtilisateur().trim())) {
                paramStm.setString(1, (x.getIdUtilisateur()));

                if (x.getFonction() == null || "".equals(x.getFonction().trim())) {
                    paramStm.setString(3, "Undefined");
                } else {
                    paramStm.setString(3, x.getFonction());
                }

                if (x.getCompagnie() == null) {
                    paramStm.setString(2, "Undefined");
                } else {
                    paramStm.setString(2, x.getCompagnie().getIdCompagnie());
                }
                
                paramStm.setDate(4, Date.valueOf(LocalDate.now()));

                // Création/Initialisation de la compagnie de l'employeur
                CompagnieDAO compagnieDAO = new CompagnieDAO(cnx);
                boolean success;
                success = compagnieDAO.create(x.getCompagnie());

                int nbLignesAffectees = paramStm.executeUpdate();

                System.out.println("Je suis dans le create d'EmployeurDAO");
                if (nbLignesAffectees > 0) {
                    paramStm.close();
                    return true;
                }
            }
            else{
                
            }
            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EmployeurDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @Override
    public Employeur read(String id) {
        String req = "SELECT employeur.*, utilisateur.* FROM employeur, utilisateur "
                + "WHERE `ID_EMPLOYEUR` = ? AND ID_EMPLOYEUR = ID_UTILISATEUR";

        PreparedStatement paramStm = null;
        try {
            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, id);
            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if (resultat.next()) {

                Employeur employeur = new Employeur();
                employeur.setIdUtilisateur((resultat.getString("ID_EMPLOYEUR")));
                employeur.setNom((resultat.getString("NOM")));
                employeur.setPrenom((resultat.getString("PRENOM")));
                employeur.setEmail((resultat.getString("COURRIEL")));
                employeur.setNomUtilisateur((resultat.getString("USERNAME")));
                employeur.setTelephone((resultat.getString("TELEPHONE")));
                employeur.setRole((resultat.getString("TYPE_UTILISATEUR")));

                employeur.setFonction((resultat.getString("FONCTION")));
                employeur.setProfilCree((resultat.getString("PROFIL_CREE")));
                employeur.setProfilModifie((resultat.getString("PROFIL_MODIFIE")));
                employeur.getCompagnie().setIdCompagnie((resultat.getString("ID_COMPAGNIE")));
                

                resultat.close();
                paramStm.close();
                return employeur;
            }

            resultat.close();
            paramStm.close();
            return null;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;

    }

    @Override
    public boolean update(Employeur x) {
        String req = "UPDATE employeur SET `FONCTION` = ?, `PROFIL_MODIFIE` = ? WHERE `ID_EMPLOYEUR` = ?";
        String req1a = "UPDATE utilisateur SET `NOM` = ?, `PRENOM` = ?, `TELEPHONE` = ?, `COURRIEL` = ? WHERE `ID_UTILISATEUR` = ?";

        PreparedStatement paramStm = null;
        boolean modifierCompagnie;
        
        try {
            paramStm = cnx.prepareStatement(req);

            if (x.getIdUtilisateur() != null && !"".equals(x.getIdUtilisateur().trim())) {
                paramStm.setString(1, x.getFonction());
                paramStm.setDate(2, Date.valueOf(LocalDate.now()));
                paramStm.setString(3, Util.toUTF8(x.getIdUtilisateur()));
                
                int nbLignesAffectees = paramStm.executeUpdate();
                
                // Execution de la 1ere requete a
                paramStm = cnx.prepareStatement(req1a);
                paramStm.setString(1, x.getNom());
                paramStm.setString(2, x.getPrenom());
                paramStm.setString(3, x.getTelephone());
                paramStm.setString(4, x.getEmail());
                paramStm.setString(5, x.getIdUtilisateur());
                int nbLignesAffecteesUpdate1a = paramStm.executeUpdate();
                
                // Modification de la compagnie de l'employeur
                CompagnieDAO cdao = new CompagnieDAO(cnx);
                modifierCompagnie = cdao.update(x.getCompagnie());
                
                if (nbLignesAffectees > 0 || nbLignesAffecteesUpdate1a > 0 || modifierCompagnie) {
                    paramStm.close();
                    return true;
                }
            }
            return false;
        } catch (SQLException exp) {
            System.out.println(exp.getMessage());
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EmployeurDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    @Override
    public boolean delete(Employeur x) {
        String req = "DELETE FROM employeur WHERE `ID_EMPLOYEUR` = ?";

        PreparedStatement paramStm = null;

        try {
            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, x.getIdUtilisateur());

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } catch (Exception exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EmployeurDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @Override
    public List<Employeur> findAll() {

        List<Employeur> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM employeur");
            while (r.next()) {
                Employeur employeur = new Employeur();
                employeur.setIdUtilisateur(r.getString("ID_EMPLOYEUR"));
                employeur.setFonction(r.getString("FONCTION"));
                employeur.getCompagnie().setIdCompagnie(r.getString("ID_COMPAGNIE"));

                liste.add(employeur);
            }
            Collections.reverse(liste);
            r.close();
            stm.close();
        } catch (SQLException exp) {
        }
        return liste;

    }

    public Utilisateur findByIdCompagnie(String idCompagnie) {
        String req = "SELECT * FROM employeur WHERE `ID_COMPAGNIE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, Util.toUTF8(idCompagnie));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if (resultat.next()) {

                Employeur employeur = new Employeur();
                employeur.setIdUtilisateur(resultat.getString("ID_EMPLOYEUR"));
                employeur.setFonction(resultat.getString("FONCTION"));
                employeur.getCompagnie().setIdCompagnie(resultat.getString("ID_COMPAGNIE"));

                resultat.close();
                paramStm.close();
                return employeur;
            }

            resultat.close();
            paramStm.close();
            return null;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            }
        }
        return null;
    }

    @Override
    public Employeur read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
