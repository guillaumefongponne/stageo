/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.dao;

import com.stageo.model.Professeur;
import com.util.Util;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Guillaume
 */
public class ProfesseurDAO extends DAO<Professeur>{
    
    public ProfesseurDAO(){
        
    }
    
    public ProfesseurDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Professeur x) {
        String req = "INSERT INTO professeur (`ID_PROFESSEUR`, `PROFIL_CREE`) "
                + "VALUES (?,?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            if(x.getIdUtilisateur() != null && !"".equals(x.getIdUtilisateur().trim())){
                paramStm.setString(1, Util.toUTF8(x.getIdUtilisateur()));
                paramStm.setDate(2, Date.valueOf(LocalDate.now()));

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                    paramStm.close();
                    return true;
                }
            }
            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EtudiantDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
        return false;
    }

    @Override
    public Professeur read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Professeur read(String id) {
        String req = "SELECT professeur.*, utilisateur.* FROM professeur, utilisateur "
                + "WHERE `ID_PROFESSEUR` = ? AND `ID_UTILISATEUR` = `ID_PROFESSEUR`";
        
        PreparedStatement paramStm = null;
        try {
            
            // Execution de la 1ere requete
            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Professeur professeur = new Professeur();
                
                professeur.setNom((resultat.getString("NOM")));
                professeur.setPrenom((resultat.getString("PRENOM")));
                professeur.setEmail((resultat.getString("COURRIEL")));
                professeur.setNomUtilisateur((resultat.getString("USERNAME")));
                professeur.setTelephone((resultat.getString("TELEPHONE")));
                professeur.setRole((resultat.getString("TYPE_UTILISATEUR")));
                
                professeur.setIdUtilisateur((resultat.getString("ID_PROFESSEUR")));
                professeur.setVille((resultat.getString("VILLE")));
                professeur.setAnglaisEcrit((resultat.getString("ANGLAIS_ECRIT")));
                professeur.setAnglaisParle((resultat.getString("ANGLAIS_PARLE")));
                professeur.setMoyenTransport((resultat.getString("MOYEN_TRANSPORT")));
                professeur.setInfosPartagees((resultat.getString("INFOS_PARTAGEES")));
                professeur.setPreferenceSupervision((resultat.getString("PREFERENCE_SUPERVISION")));
                professeur.setDispoStageEte((resultat.getString("DISPO_STAGE_ETE")));
                professeur.setProfilCree((resultat.getString("PROFIL_CREE")));
                professeur.setProfilModifie((resultat.getString("PROFIL_MODIFIE")));
                
                resultat.close();
                paramStm.close();
                return professeur;
                    
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
            System.out.println("Erreur 1 SQL (classe ProfesseurDAO) :"+exp);
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
                System.out.println("Erreur 2 SQL (classe ProfesseurDAO) :"+exp);
            }
             catch (Exception e) {
                 System.out.println("Erreur 3 (classe ProfesseurDAO) :"+e);
            }
        }

        return null;
    }

    @Override
    public boolean update(Professeur x) {
        String req1 = "UPDATE professeur SET `VILLE` = ?, `INFOS_PARTAGEES` = ?, `PREFERENCE_SUPERVISION` = ?, "
                + "`ANGLAIS_ECRIT` = ?, `ANGLAIS_PARLE` = ?, `DISPO_STAGE_ETE` = ?,"
                + " `MOYEN_TRANSPORT` = ?, `PROFIL_MODIFIE` = ? WHERE `ID_PROFESSEUR` = ?";
        
        String req1a = "UPDATE utilisateur SET `NOM` = ?, `PRENOM` = ?, `TELEPHONE` = ?, `COURRIEL` = ? WHERE `ID_UTILISATEUR` = ?";
        
        PreparedStatement paramStm;
        try {
            // Execution de la 1ere requete
            paramStm = cnx.prepareStatement(req1);
            paramStm.setString(1, x.getVille());
            paramStm.setString(2, x.getInfosPartagees());
            paramStm.setString(3, x.getPreferenceSupervision());
            paramStm.setString(4, x.getAnglaisEcrit());
            paramStm.setString(5, x.getAnglaisParle());
            paramStm.setString(6, x.getDispoStageEte());
            paramStm.setString(7, x.getMoyenTransport());
            paramStm.setDate(8, Date.valueOf(LocalDate.now()));
            paramStm.setString(9, x.getIdUtilisateur());
            int nbLignesAffecteesUpdate= paramStm.executeUpdate();
            
            // Execution de la 1ere requete a
            paramStm = cnx.prepareStatement(req1a);
            paramStm.setString(1, x.getNom());
            paramStm.setString(2, x.getPrenom());
            paramStm.setString(3, x.getTelephone());
            paramStm.setString(4, x.getEmail());
            paramStm.setString(5, x.getIdUtilisateur());
            int nbLignesAffecteesUpdate1a = paramStm.executeUpdate();
            
            paramStm.close();
            
            if (nbLignesAffecteesUpdate > 0 || nbLignesAffecteesUpdate1a > 0){ 
                return true;
            }
        }
            
        catch (SQLException exp) {
           Logger.getLogger(EtudiantDAO.class.getName()).log(Level.SEVERE, null, exp);
        }
        return false;
    }

    @Override
    public boolean delete(Professeur x) {
        String req = "DELETE FROM professeur WHERE `ID_PROFESSEUR` = ?";

        PreparedStatement paramStm = null;

        try {
            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, x.getIdUtilisateur());

            int nbLignesAffectees= paramStm.executeUpdate();

            if (nbLignesAffectees>0) {
                paramStm.close();
                return true;
            }

            return false;
        }
        catch (SQLException exp) {
        }
        catch (Exception exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EtudiantDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
        }
        return false;
    }

    @Override
    public List<Professeur> findAll() {
        List<Professeur> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM professeur");
            while (r.next()) {
                Professeur professeur = new Professeur();
                professeur.setIdUtilisateur((r.getString("ID_PROFESSEUR")));
                professeur.setVille((r.getString("VILLE")));
                professeur.setAnglaisEcrit((r.getString("ANGLAIS_ECRIT")));
                professeur.setAnglaisParle((r.getString("ANGLAIS_PARLE")));
                professeur.setMoyenTransport((r.getString("MOYEN_TRANSPORT")));
                professeur.setInfosPartagees((r.getString("INFOS_PARTAGEES")));
                professeur.setPreferenceSupervision((r.getString("PREFERENCE_SUPERVISION")));
                professeur.setDispoStageEte((r.getString("DISPO_STAGE_ETE")));
                professeur.setProfilCree((r.getString("PROFIL_CREE")));
                professeur.setProfilModifie((r.getString("PROFIL_MODIFIE")));

                liste.add(professeur);
            }
            r.close();
            stm.close();
        }
        catch (SQLException exp){
        }
        return liste;
    }
    
}
