/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.dao;

/**
 *
 * @author Jp
 */
import com.stageo.model.Compagnie;
import com.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JP et Dave
 */
public class CompagnieDAO extends DAO<Compagnie>{

    public CompagnieDAO() {
    }

    public CompagnieDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Compagnie x) {
        String req = "INSERT INTO compagnie (`ID_COMPAGNIE`, `ID_ADRESSE`) VALUES (?,?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, (x.getIdCompagnie()));
            paramStm.setString(2, (x.getAdresse().getIdAdresse()));

            int nbLignesAffectees= paramStm.executeUpdate();

            // Création/Initialisation de l'adresse de la compagnie
            AdresseDAO adresseDao = new AdresseDAO(cnx);
            boolean success;
            success = adresseDao.create(x.getAdresse());

            System.out.println("Je suis dans le create d'CompagnieDAO");

            if (nbLignesAffectees>0) {
                paramStm.close();
                return true;
            }

            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CompagnieDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }

        }
        return false;
    }

    @Override
    public Compagnie read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Compagnie read(String id) {
        String req = "SELECT * FROM compagnie WHERE `ID_COMPAGNIE` = ?";
        
        PreparedStatement paramStm = null;
        try {
            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Compagnie c = new Compagnie();
                c.setIdCompagnie((resultat.getString("ID_COMPAGNIE")));
                c.setNom((resultat.getString("NOM")));
                c.setPageWeb((resultat.getString("SITE_WEB")));
                c.setSecteur((resultat.getString("SECTEUR")));
                c.setTelEntreprise((resultat.getString("TEL_ENTREPRISE")));
                c.setLienGoogleMaps((resultat.getString("LIEN_GOOGLE_MAPS")));
                c.setKilometrage(resultat.getInt("KILOMETRAGE"));
                c.setTaille((resultat.getString("TAILLE")));
                c.setNbEmployes(resultat.getInt("NB_EMPLOYES"));
                c.getAdresse().setIdAdresse((resultat.getString("ID_ADRESSE")));
                /*Il faut coder le adresseDAO pour setAdresse
                c.setAdresse((new CompteDAO(cnx)).countCompteByIdEquipe(id));*/

                resultat.close();
                paramStm.close();
                    return c;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;

    }

    @Override
    public boolean update(Compagnie x) {
        String req = "UPDATE compagnie SET `NOM` = ?, `SITE_WEB` = ?, `SECTEUR` = ?, "
                + "`TEL_ENTREPRISE` = ?, `LIEN_GOOGLE_MAPS` = ?, `KILOMETRAGE` = ?, `TAILLE` = ?, `NB_EMPLOYES` = ? "
                + "WHERE `ID_COMPAGNIE` = ?";
        System.out.println("dans compagnie dao update");
        PreparedStatement paramStm = null;
        boolean modifierAdresse;
        
        try {

                paramStm = cnx.prepareStatement(req);

                if(x.getIdCompagnie() == null || "".equals(x.getIdCompagnie().trim())){
                    paramStm.setString(9, null);
                }
                else{
                    paramStm.setString(1, x.getNom());
                    paramStm.setString(2, x.getPageWeb());
                    paramStm.setString(3, x.getSecteur());
                    paramStm.setString(4, x.getTelEntreprise());
                    paramStm.setString(5, x.getLienGoogleMaps());
                    paramStm.setInt(6, x.getKilometrage());
                    paramStm.setString(7, x.getTaille());
                    paramStm.setInt(8, x.getNbEmployes());
                    paramStm.setString(9, x.getIdCompagnie());
                }

                int nbLignesAffectees= paramStm.executeUpdate();
                
                // Modification de l'adresse
                AdresseDAO adao = new AdresseDAO(cnx);
                modifierAdresse = adao.update(x.getAdresse());

                if (nbLignesAffectees>0 && modifierAdresse) {
                    paramStm.close();
                    return true;
                }

            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CompagnieDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }

        }
        return false;
    }

    @Override
    public boolean delete(Compagnie x) {
        String req = "DELETE FROM compagnie WHERE `ID_COMPAGNIE` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);
                paramStm.setString(1, x.getIdCompagnie());

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                        paramStm.close();
                    return true;
                }

            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CompagnieDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }

        }
        return false;
    }


    @Override
    public List<Compagnie> findAll() {
        List<Compagnie> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM compagnie");
            while (r.next()) {
                Compagnie c = new Compagnie();
                c.setIdCompagnie(r.getString("ID_COMPAGNIE"));
                c.setNom(r.getString("NOM"));
                c.setPageWeb(r.getString("SITE_WEB"));
                c.setSecteur(r.getString("SECTEUR"));
                c.setTelEntreprise(r.getString("TEL_ENTREPRISE"));
                c.setLienGoogleMaps(r.getString("LIEN_GOOGLE_MAPS"));
                c.setKilometrage(r.getInt("KILOMETRAGE"));
                c.setTaille(r.getString("TAILLE"));
                c.setNbEmployes(r.getInt("NB_EMPLOYES"));
                c.getAdresse().setIdAdresse(r.getString("ID_ADRESSE"));

                liste.add(c);
            }
            Collections.reverse(liste);
            r.close();
            stm.close();
        }
        catch (SQLException exp) {
        }
        return liste;
    }
    
    public Compagnie findByIdEmployeur(String id) {
        
        String req = "SELECT * FROM compagnie WHERE `ID_EMPLOYEUR` = ?";
        
        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);      
            paramStm.setString(1, Util.toUTF8(id));

            ResultSet r = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if (r.next()) {
                Compagnie c = new Compagnie();
                c.setIdCompagnie(r.getString("ID_COMPAGNIE"));
                c.setNom(r.getString("NOM"));
                c.setPageWeb(r.getString("SITE_WEB"));
                c.setSecteur(r.getString("SECTEUR"));
                c.setTelEntreprise(r.getString("TEL_ENTREPRISE"));
                c.setLienGoogleMaps(r.getString("LIEN_GOOGLE_MAPS"));
                c.setKilometrage(r.getInt("KILOMETRAGE"));
                c.setTaille(r.getString("TAILLE"));
                c.setNbEmployes(r.getInt("NB_EMPLOYES"));
                c.getAdresse().setIdAdresse(r.getString("ID_ADRESSE"));
                
                r.close();
                paramStm.close();
                return c;
            }
            
            r.close();
            paramStm.close();
        }
        catch (SQLException exp) {
        }
        return null;
    }

    public Compagnie findByNom(String nom) {
        String req = "SELECT * FROM compagnie WHERE `NOM` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(nom));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Compagnie c = new Compagnie();
                c.setIdCompagnie(resultat.getString("ID_COMPAGNIE"));
                c.setNom(resultat.getString("NOM"));
                c.setPageWeb(resultat.getString("SITE_WEB"));
                c.setSecteur(resultat.getString("SECTEUR"));
                c.setTelEntreprise(resultat.getString("TEL_ENTREPRISE"));
                c.setLienGoogleMaps(resultat.getString("LIEN_GOOGLE_MAPS"));
                c.setKilometrage(resultat.getInt("KILOMETRAGE"));
                c.setTaille(resultat.getString("TAILLE"));
                c.setNbEmployes(resultat.getInt("NB_EMPLOYES"));
                c.getAdresse().setIdAdresse(resultat.getString("ID_ADRESSE"));

                resultat.close();
                paramStm.close();
                return c;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;
    }

}
