/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.dao;

import com.stageo.model.Adresse;
import com.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chris
 */
public class AdresseDAO extends DAO<Adresse>{

    public AdresseDAO() {
    }

    public AdresseDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Adresse x) {
        String req = "INSERT INTO adresse (`ID_ADRESSE`, `ADRESSE_LIGNE1`, `ADRESSE_LIGNE2`, `VILLE`, `PROVINCE`, `CODE_POSTAL`, `PAYS`) VALUES (?,?,?,?,?,?,?)";

        PreparedStatement paramStm = null;
        try {
            System.out.println(x.getIdAdresse());
            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, (x.getIdAdresse()));
            paramStm.setString(2, (x.getAdresseLigne1()));
            paramStm.setString(3, (x.getAdresseLigne2()));
            paramStm.setString(4, (x.getVille()));
            paramStm.setString(5, (x.getProvince()));
            paramStm.setString(6, (x.getCodePostal()));
            paramStm.setString(7, (x.getPays()));

            int nbLignesAffectees= paramStm.executeUpdate();
            System.out.println("Je suis dans le create d'AdresseDAO");
            System.out.println(nbLignesAffectees);
            if (nbLignesAffectees>0) {
                paramStm.close();
                return true;
            }

            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AdresseDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }

        }
        return false;
    }

    @Override
    public Adresse read(int id) {
        try{
            return this.read(id);
        }
        catch(NumberFormatException e){
            return null;
        }
    }

    @Override
    public Adresse read(String id) {
        String req = "SELECT * FROM adresse WHERE `ID_ADRESSE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Adresse a = new Adresse();
                a.setIdAdresse((resultat.getString("ID_ADRESSE")));
                a.setAdresseLigne1((resultat.getString("ADRESSE_LIGNE1")));
                a.setAdresseLigne2((resultat.getString("ADRESSE_LIGNE2")));
                a.setVille((resultat.getString("VILLE")));
                a.setCodePostal((resultat.getString("CODE_POSTAL")));
                a.setProvince((resultat.getString("PROVINCE")));
                a.setPays((resultat.getString("PAYS")));

                resultat.close();
                paramStm.close();
                return a;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;

    }

    @Override
    public boolean update(Adresse x) {
        String req = "UPDATE adresse SET `ADRESSE_LIGNE1` = ?, `ADRESSE_LIGNE2` = ?, `VILLE` = ?, `CODE_POSTAL` = ?, `PROVINCE` = ?, `PAYS` = ? WHERE `ID_ADRESSE` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);

                if(x.getIdAdresse() == null || "".equals(x.getIdAdresse().trim()))
                    paramStm.setString(7, null);
                else {
                    paramStm.setString(1, x.getAdresseLigne1());
                    paramStm.setString(2, x.getAdresseLigne2());
                    paramStm.setString(3, x.getVille());
                    paramStm.setString(4, x.getCodePostal());
                    paramStm.setString(5, x.getProvince());
                    paramStm.setString(6, x.getPays());
                    paramStm.setString(7, x.getIdAdresse());
                }

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                    paramStm.close();
                    return true;
                }

            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AdresseDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }

        }
        return false;
    }

    @Override
    public boolean delete(Adresse x) {
        String req = "DELETE FROM adresse WHERE `ID_ADRESSE` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);
                paramStm.setString(1, x.getIdAdresse());

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                    paramStm.close();
                    return true;
                }

            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AdresseDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }

        }
        return false;
    }


    @Override
    public List<Adresse> findAll() {
        List<Adresse> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM adresse");
            while (r.next()) {
                Adresse a = new Adresse();
                a.setIdAdresse(r.getString("ID_ADRESSE"));
                a.setAdresseLigne1(r.getString("ADRESSE_LIGNE1"));
                a.setAdresseLigne2(r.getString("ADRESSE_LIGNE2"));
                a.setVille(r.getString("VILLE"));
                a.setCodePostal(r.getString("CODE_POSTAL"));
                a.setProvince(r.getString("PROVINCE"));
                a.setPays(r.getString("PAYS"));

                liste.add(a);
            }
           // Collections.sort(liste);
            Collections.reverse(liste);
            r.close();
            stm.close();
        }
        catch (SQLException exp) {
        }
        return liste;
    }

    public List<Adresse> findByVille(String ville) {
        String req = "SELECT * FROM adresse WHERE `VILLE` = ?";
        List<Adresse> liste = new LinkedList<>();

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(ville));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            while (resultat.next()) {

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setAdresseLigne1(resultat.getString("ADRESSE_LIGNE1"));
                a.setAdresseLigne2(resultat.getString("ADRESSE_LIGNE2"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));

                liste.add(a);
                
            }
            
            Collections.reverse(liste);
            resultat.close();
            paramStm.close();
            
        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return liste;
    }

    public List<Adresse> findByProvince(String province){
        String req = "SELECT * FROM adresse WHERE `PROVINCE` = ?";
        List<Adresse> liste = new LinkedList<>();

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(province));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            while (resultat.next()) {

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setAdresseLigne1(resultat.getString("ADRESSE_LIGNE1"));
                a.setAdresseLigne2(resultat.getString("ADRESSE_LIGNE2"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));

                liste.add(a);
                
            }
            
            Collections.reverse(liste);
            resultat.close();
            paramStm.close();
            
        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return liste;
    }

    public List<Adresse> findByCodePostal(String codePostal) {
        String req = "SELECT * FROM adresse WHERE `CODE_POSTAL` = ?";
        List<Adresse> liste = new LinkedList<>();

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(codePostal));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            while (resultat.next()) {

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setAdresseLigne1(resultat.getString("ADRESSE_LIGNE1"));
                a.setAdresseLigne2(resultat.getString("ADRESSE_LIGNE2"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));

                liste.add(a);
                
            }
            
            Collections.reverse(liste);
            resultat.close();
            paramStm.close();
            
        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return liste;
    }
    
    public Adresse findByID(String idAdresse) {
        String req = "SELECT * FROM adresse WHERE `ID_ADRESSE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(idAdresse));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setAdresseLigne1(resultat.getString("ADRESSE_LIGNE1"));
                a.setAdresseLigne2(resultat.getString("ADRESSE_LIGNE2"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));

                resultat.close();
                paramStm.close();
                return a;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;
    }
}
