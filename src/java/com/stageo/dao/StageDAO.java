/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.dao;

import com.stageo.model.Critere;
import com.stageo.model.OffreStage;
import com.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chris
 * @author Alex :gestion des erreurs faite
 */
public class StageDAO extends DAO<OffreStage> {

    public StageDAO() {
    }

    public StageDAO(Connection cnx) {
        super(cnx);
    }

    /**
     *
     * @param x
     * @return
     */
    @Override
    public boolean create(OffreStage x) {
       
        String req = "INSERT INTO offrestage (`ID_OFFRE`,"
                + " `TITRE`,"
                + " `DESCRIPTION`,"
                + " `DATE_DEBUT`,"
                + " `DATE_FIN`,"
                + " `DUREE_EN_JOURS`, "
                + "`REMUNERE`,"
                + " `LIEN_WEB`, "
                + "`LIEN_DOCUMENT`,"
                + " `DATE`,"
                + " `NB_VUES`, "
                + "`ACTIVE`,"
                + " `ID_EMPLOYEUR`,"
                + " `FICHIER`,"
                + " `STATUT_OFFRE`) "
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        String req2 = "INSERT INTO offrestagecritere (`ID_OFFRE`, `ID_CRITERE`) VALUES (?,?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, x.getIdOffre());
            paramStm.setString(2, x.getTitre());
            paramStm.setString(3, x.getDescription());
            paramStm.setTimestamp(4, x.getDateDebut());
            paramStm.setTimestamp(5, x.getDateFin());
            paramStm.setInt(6, x.getDureeEnJours());
            paramStm.setInt(7, x.getRemunere());
            paramStm.setString(8, x.getLienWeb());
            paramStm.setString(9, x.getLienDocument());
            paramStm.setTimestamp(10, x.getDate());
            paramStm.setInt(11, x.getNbVues());
            paramStm.setInt(12, x.getActive());
            paramStm.setString(13, x.getIdEmployeur());
            paramStm.setBinaryStream(14, x.getFichier());
            paramStm.setBoolean(15, false);

            int nbLignesAffectees = paramStm.executeUpdate();

            
            for (int i = 0; i < x.getListeCritere().size(); i++) {
                System.out.println(x.getListeCritere().get(i));
                paramStm = cnx.prepareStatement(req2);
                paramStm.setString(1, x.getIdOffre());
                System.out.println(x.getListeCritere().get(i).getIdCritere());
                paramStm.setString(2, x.getListeCritere().get(i).getIdCritere());
                nbLignesAffectees += paramStm.executeUpdate();
            }


             
            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }
            paramStm.close();
            return false;
        } catch (SQLException exp) {
            System.out.println(exp);
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StageDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;

    }

    @Override
    public OffreStage read(String id) {
        String req = "SELECT * FROM offrestage WHERE `ID_OFFRE` = ?";
        PreparedStatement paramStm = null;
        try {
            paramStm = cnx.prepareStatement(req);
            
            paramStm.setString(1, id);

            ResultSet r = paramStm.executeQuery();
            
            // On vérifie s'il y a un résultat    
            if (r.next()) {

                OffreStage o = new OffreStage();
                o.setIdOffre(r.getString("ID_OFFRE"));
                o.setTitre(r.getString("TITRE"));
                o.setDescription(r.getString("DESCRIPTION"));
                o.setDateDebut(r.getTimestamp("DATE_DEBUT"));
                o.setDateFin(r.getTimestamp("DATE_FIN"));
                o.setDureeEnJours(r.getInt("DUREE_EN_JOURS"));
                o.setRemunere(r.getInt("REMUNERE"));
                o.setLienWeb(r.getString("LIEN_WEB"));
                o.setLienDocument(r.getString("LIEN_DOCUMENT"));
                o.setDate(r.getTimestamp("DATE"));
                o.setNbVues(r.getInt("NB_VUES"));
                o.setActive(r.getInt("ACTIVE"));
                o.setIdEmployeur(r.getString("ID_EMPLOYEUR"));
                o.setFichier(r.getBinaryStream("FICHIER"));
                r.close();
                paramStm.close();
                System.out.println("ici"+o.getIdOffre());
                return o;
            }

            r.close();
            paramStm.close();
            return null;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;
    }

    @Override
    public boolean update(OffreStage x) {
        String id_offre = x.getIdOffre();
        String req = "UPDATE offrestage SET "
                +"`TITRE` = ?, `DESCRIPTION` = ?,`DATE_DEBUT`= ?,"
                +"`DATE_FIN` = ?,`DUREE_EN_JOURS` = ?,`REMUNERE` = ?,"
                +"`LIEN_WEB` = ?,`DATE` = ?,`NB_VUES` = ?,"
                +"`ACTIVE` = ?,`ID_EMPLOYEUR` = ?  WHERE `ID_OFFRE` = ?";
        String req2 = "DELETE FROM offrestagecritere WHERE `ID_OFFRE` = ?";
        String req3 = "INSERT INTO offrestagecritere (`ID_OFFRE`, `ID_CRITERE`) VALUES (?,?)";
        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            if (x.getIdOffre() == null || "".equals(x.getIdOffre().trim())) {
                paramStm.setString(1, null);
            } else {

                paramStm.setString(1, Util.toUTF8(x.getTitre()));
                paramStm.setString(2, Util.toUTF8(x.getDescription()));
                paramStm.setTimestamp(3, x.getDateDebut());
                paramStm.setTimestamp(4, x.getDateFin());
                paramStm.setInt(5, x.getDureeEnJours());
                paramStm.setInt(6, x.getRemunere());
                paramStm.setString(7, Util.toUTF8(x.getLienWeb()));
                //paramStm.setString(8, Util.toUTF8(x.getLienDocument()));
                paramStm.setTimestamp(8, x.getDate());
                paramStm.setInt(9, x.getNbVues());
                paramStm.setInt(10, x.getActive());
                paramStm.setString(11, Util.toUTF8(x.getIdEmployeur()));
                paramStm.setString(12, id_offre);
                int nbLignesAffecteesUpdate = paramStm.executeUpdate();

                paramStm = cnx.prepareStatement(req2);
                paramStm.setString(1, id_offre);
                int nbLignesAffecteesDelete = paramStm.executeUpdate();

                int nbLignesCriteresAffecteesUpdate = 0;
                for (int i = 0; i < x.getListeCritere().size(); i++) {
                    paramStm = cnx.prepareStatement(req3);
                    paramStm.setString(1, id_offre);
                    paramStm.setString(2, x.getListeCritere().get(i).getIdCritere());
                    nbLignesCriteresAffecteesUpdate = paramStm.executeUpdate();
                }

                if (nbLignesAffecteesUpdate>0&&nbLignesCriteresAffecteesUpdate>0) {
                    paramStm.close();
                    return true;
                }
            }

            return false;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StageDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }
    
    public boolean updateStatutOffre(OffreStage x){
        String req = "UPDATE offrestage SET `STATUT_OFFRE`=?  WHERE `ID_OFFRE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            if (x.getIdOffre() == null || "".equals(x.getIdOffre().trim())) {
                paramStm.setString(2, null);
            } else {

                paramStm.setBoolean(1, x.isStatutOffre());
                paramStm.setString(2, x.getIdOffre());

                int nbLignesAffectees = paramStm.executeUpdate();

                if (nbLignesAffectees > 0) {
                    paramStm.close();
                    return true;
                }
            }

            return false;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StageDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    @Override
    public boolean delete(OffreStage x) {
        String req = "DELETE FROM offrestage WHERE `ID_OFFRE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, x.getIdOffre());

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(StageDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    @Override
    public List<OffreStage> findAll() {
        List<OffreStage> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM offrestage");
            while (r.next()) {

                OffreStage o = new OffreStage();
                o.setIdOffre(r.getString("ID_OFFRE"));
                o.setTitre(r.getString("TITRE"));
                o.setDescription(r.getString("DESCRIPTION"));
                o.setDateDebut(r.getTimestamp("DATE_DEBUT"));
                o.setDateFin(r.getTimestamp("DATE_FIN"));
                o.setDureeEnJours(r.getInt("DUREE_EN_JOURS"));
                o.setRemunere(r.getInt("REMUNERE"));
                o.setLienWeb(r.getString("LIEN_WEB"));
                o.setLienDocument(r.getString("LIEN_DOCUMENT"));
                o.setDate(r.getTimestamp("DATE"));
                o.setNbVues(r.getInt("NB_VUES"));
                o.setActive(r.getInt("ACTIVE"));
                o.setIdEmployeur(r.getString("ID_EMPLOYEUR"));
                o.setStatutOffre(r.getBoolean("STATUT_OFFRE"));

                liste.add(o);
            }
            // Collections.sort(liste);
            Collections.reverse(liste);
            r.close();
            stm.close();
        } catch (SQLException exp) {
        }
        return liste;
    }

    public OffreStage findById(String id) {
        String req = "SELECT * FROM offrestage WHERE `ID_OFFRE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(id));

            ResultSet r = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if (r.next()) {

                OffreStage o = new OffreStage();
                o.setIdOffre(r.getString("ID_OFFRE"));
                o.setTitre(r.getString("TITRE"));
                o.setDescription(r.getString("DESCRIPTION"));
                o.setDateDebut(r.getTimestamp("DATE_DEBUT"));
                o.setDateFin(r.getTimestamp("DATE_FIN"));
                o.setDureeEnJours(r.getInt("DUREE_EN_JOURS"));
                o.setRemunere(r.getInt("REMUNERE"));
                o.setLienWeb(r.getString("LIEN_WEB"));
                o.setLienDocument(r.getString("LIEN_DOCUMENT"));
                o.setDate(r.getTimestamp("DATE"));
                o.setNbVues(r.getInt("NB_VUES"));
                o.setActive(r.getInt("ACTIVE"));
                o.setIdEmployeur(r.getString("ID_EMPLOYEUR"));

                r.close();
                paramStm.close();
                return o;
            }

            r.close();
            paramStm.close();
            return null;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;
    }

    public List<OffreStage> findByIdEmployeur(String id) {

        List<OffreStage> liste = new LinkedList<>();
        String req = "SELECT * FROM offrestage WHERE `ID_EMPLOYEUR` = ?";
        String req2 = "SELECT * FROM offrestagecritere WHERE `ID_OFFRE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(id));

            ResultSet r = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            while (r.next()) {

                OffreStage o = new OffreStage();
                List<Critere> listeCritere = new ArrayList<Critere>();
                o.setIdOffre(r.getString("ID_OFFRE"));
                o.setTitre(r.getString("TITRE"));
                o.setDescription(r.getString("DESCRIPTION"));
                o.setDateDebut(r.getTimestamp("DATE_DEBUT"));
                o.setDateFin(r.getTimestamp("DATE_FIN"));
                o.setDureeEnJours(r.getInt("DUREE_EN_JOURS"));
                o.setRemunere(r.getInt("REMUNERE"));
                o.setLienWeb(r.getString("LIEN_WEB"));
                o.setLienDocument(r.getString("LIEN_DOCUMENT"));
                o.setDate(r.getTimestamp("DATE"));
                o.setNbVues(r.getInt("NB_VUES"));
                o.setActive(r.getInt("ACTIVE"));
                o.setIdEmployeur(r.getString("ID_EMPLOYEUR"));
                
                paramStm = cnx.prepareStatement(req2);
                paramStm.setString(1, (r.getString("ID_OFFRE")));
                ResultSet res = paramStm.executeQuery();

                while (res.next()) {
                    Critere critere = new Critere();
                    critere.setIdCritere(res.getString("ID_CRITERE"));
                    listeCritere.add(critere);

                }
                o.setListeCritere(listeCritere);


                liste.add(o);
            }
            // Collections.sort(liste);
            Collections.reverse(liste);
            r.close();
            paramStm.close();
        } catch (SQLException exp) {
        }
        return liste;
    }
    
    public List<OffreStage> findByStatutOffre(boolean statutOffre) {
        List<OffreStage> liste = new LinkedList<>();
        String req = "SELECT * FROM offrestage WHERE `STATUT_OFFRE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setBoolean(1, statutOffre);

            ResultSet r = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            while (r.next()) {

                OffreStage o = new OffreStage();
                o.setIdOffre(r.getString("ID_OFFRE"));
                o.setTitre(r.getString("TITRE"));
                o.setDescription(r.getString("DESCRIPTION"));
                o.setDateDebut(r.getTimestamp("DATE_DEBUT"));
                o.setDateFin(r.getTimestamp("DATE_FIN"));
                o.setDureeEnJours(r.getInt("DUREE_EN_JOURS"));
                o.setRemunere(r.getInt("REMUNERE"));
                o.setLienWeb(r.getString("LIEN_WEB"));
                o.setLienDocument(r.getString("LIEN_DOCUMENT"));
                o.setDate(r.getTimestamp("DATE"));
                o.setNbVues(r.getInt("NB_VUES"));
                o.setActive(r.getInt("ACTIVE"));
                o.setIdEmployeur(r.getString("ID_EMPLOYEUR"));
                o.setStatutOffre(r.getBoolean("STATUT_OFFRE"));
                
                liste.add(o);

                
            }
            Collections.reverse(liste);
            r.close();
            paramStm.close();

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return liste;
    }

    @Override
    public OffreStage read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
