/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.dao;

/**
 *
 * @author Jp
 */
import com.stageo.model.Document;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Christopher Fonctionnalités de FICHIER à faire
 */
public class DocumentDAO extends DAO<Document> {

    public DocumentDAO() {
    }

    public DocumentDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Document x) {
        String req = "INSERT INTO document (`ID_DOCUMENT`, `TITRE`, `TYPE`, `NB_VUES`, `ID_COORDONNATEUR`, `DATE`, `FICHIER`) VALUES (?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, x.getIdDocument());
            paramStm.setString(2, x.getTitre());
            paramStm.setString(3, x.getType());
            paramStm.setInt(4, x.getNbVues());
            paramStm.setString(5, x.getIdCoordonnateur());
            paramStm.setTimestamp(6, x.getDate());
            paramStm.setBinaryStream(7, x.getFichier());

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DocumentDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    @Override
    public Document read(int id) {
        String req = "SELECT * FROM document WHERE `ID_DOCUMENT` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setInt(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if (resultat.next()) {

                Document d = new Document();
                d.setIdDocument(resultat.getString("ID_DOCUMENT"));
                d.setTitre(resultat.getString("TITRE"));
                d.setType(resultat.getString("TYPE"));
                d.setNbVues(resultat.getInt("NB_VUES"));
                d.setIdCoordonnateur(resultat.getString("ID_COORDONNATEUR"));
                d.setDate(resultat.getTimestamp("DATE"));
                d.setFichier(resultat.getBinaryStream("FICHIER"));
                resultat.close();
                paramStm.close();
                return d;
            }

            resultat.close();
            paramStm.close();

        } catch (SQLException ex) {
            Logger.getLogger(DocumentDAO.class.getName()).log(Level.SEVERE, null, ex);
        } 

        return null;
    }

    @Override
    public Document read(String id) {

        try {
            return this.read(Integer.parseInt(id));
        } catch (NumberFormatException e) {
            return null;
        }

    }

    @Override
    public boolean update(Document x) {
        String req = "UPDATE document SET `TITRE` = ?, `TYPE` = ?,`NB_VUES` = ?,`ID_COORDONNATEUR` = ?,`DATE` = ?,`FICHIER` = ?, WHERE `ID_DOCUMENT` = ?";
        PreparedStatement paramStm ;
        try {

            paramStm = cnx.prepareStatement(req);
            
            // @Todo:Gerer le fait d'ajouter un titre dans le  html
            paramStm.setString(1, x.getTitre());
            paramStm.setString(2, x.getType());
            paramStm.setInt(3, x.getNbVues());
            paramStm.setString(4, x.getIdCoordonnateur());
            paramStm.setTimestamp(5, x.getDate());
            paramStm.setBinaryStream(6, x.getFichier());

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }
            return false;
        } catch (SQLException ex) {
                Logger.getLogger(DocumentDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
                 return false;
            }
       
    }

    @Override
    public boolean delete(Document x) {
        String req = "DELETE FROM document WHERE `ID_DOCUMENT` = ?";
        Boolean reussite=false;
        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, x.getIdDocument());

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                reussite= true;
            }
            return reussite;
        } catch (SQLException ex) {
            Logger.getLogger(Document.class.getName())
                        .log(Level.SEVERE, null, ex);
            return false;
        } 
    }

    @Override
    public List<Document> findAll() {
        List<Document> liste = new LinkedList<>();

        try (Statement stm = cnx.createStatement()) {
            ResultSet r = stm.executeQuery("SELECT * FROM document");
            while (r.next()) {
                Document d = new Document();
                d.setIdDocument(r.getString("ID_DOCUMENT"));
                d.setTitre(r.getString("TITRE"));
                d.setType(r.getString("TYPE"));
                d.setNbVues(r.getInt("NB_VUES"));
                d.setIdCoordonnateur(r.getString("ID_COORDONNATEUR"));
                d.setDate(r.getTimestamp("DATE"));
                d.setFichier(r.getBinaryStream("FICHIER"));
                liste.add(d);
            }
            // Collections.sort(liste);
            Collections.reverse(liste);
            r.close();
        } catch (SQLException ex) {
            Logger.getLogger(DocumentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return liste;
    }

    public Document findByTitre(String titre) {
         Document d = new Document();
        try {
            String req = "SELECT * FROM document WHERE `TITRE` = ?";
            PreparedStatement paramStm;
            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, titre);
            ResultSet r = paramStm.executeQuery();
            // On vérifie s'il y a un résultat
                if (r.next()) {
                    d.setIdDocument(r.getString("ID_DOCUMENT"));
                    d.setTitre(titre);
                    d.setType(r.getString("TYPE"));
                    d.setNbVues(r.getInt("NB_VUES"));
                    d.setIdCoordonnateur(r.getString("ID_COORDONNATEUR"));
                    d.setDate(r.getTimestamp("DATE"));
                    d.setFichier(r.getBinaryStream("FICHIER"));
                    r.close();
                    paramStm.close();
                    
                    
                }
                paramStm.close();
                return null;
            } catch (SQLException ex) {
                Logger.getLogger(DocumentDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        return d;
    }
    public boolean updateCompteur(Document x) {
        String req = "UPDATE document SET `NB_VUES` = ? WHERE `ID_DOCUMENT` = ?";
        PreparedStatement paramStm ;
        try {

            paramStm = cnx.prepareStatement(req);
            
            paramStm.setInt(1, (x.getNbVues()+1));  

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }
            return false;
        } catch (SQLException ex) {
                Logger.getLogger(DocumentDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
                 return false;
            }
    }

}
