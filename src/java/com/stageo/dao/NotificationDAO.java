
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.dao;

import com.stageo.model.Notification;
import com.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alex ; Correction gestion erreur terminée
 */
public class NotificationDAO extends DAO<Notification>{

    public NotificationDAO() {
    }

    public NotificationDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Notification x) {
        String req = "INSERT INTO notification (`TYPE`,`MESSAGE`,`VUE`,`ID_coordonnateur`,`DATE`,`HEURE`) VALUES (?,?,?,?,?,?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(x.getType()));
             paramStm.setString(2, Util.toUTF8(x.getMessage()));
              paramStm.setInt(2, x.getVue());
              paramStm.setString(3, Util.toUTF8(x.getIdCoordonnateur()));
               paramStm.setString(4, Util.toUTF8(x.getDate()));
                paramStm.setString(5, Util.toUTF8(x.getHeure()));


            int nbLignesAffectees= paramStm.executeUpdate();

            if (nbLignesAffectees>0) {
                    paramStm.close();
                    return true;
            }
            paramStm.close();
        }
        catch (SQLException ex) {
            Logger.getLogger(NotificationDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public Notification read(int id) {
        String req = "SELECT * FROM notification WHERE `ID_Notification` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setInt(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Notification n = new Notification();
                n.setIdNotification(resultat.getString("ID_Notification"));
                n.setType(resultat.getString("TYPE"));
                n.setMessage(resultat.getString("MESSAGE"));
                n.setVue(resultat.getInt("VUE"));
                n.setIdCoordonnateur(resultat.getString("ID_coordonnateur"));
                n.setDate(resultat.getString("DATE"));
                n.setHeure(resultat.getString("HEURE"));


                resultat.close();
                paramStm.close();
                    return n;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException ex) {
             Logger.getLogger(NotificationDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Notification read(String id) {

        try{
            return this.read(Integer.parseInt(id));
        }
        catch(NumberFormatException e){
            return null;
        }

    }

    @Override
    public boolean update(Notification x) {
        String req = "UPDATE notification SET `TYPE` = ?,`MESSAGE` = ?, `VUE` = ?,`ID_coordonnateur`= ?,`DATE` = ?,`HEURE` = ? WHERE `ID_NOTIFICATION` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);

                if(x.getIdNotification() == null || "".equals(x.getIdNotification().trim()))
                    paramStm.setString(1, null);
                else
                    paramStm.setString(1, x.getType());
                    paramStm.setString(2, x.getMessage());
                    paramStm.setInt(3, x.getVue());
                    paramStm.setString(4, x.getIdCoordonnateur());
                    paramStm.setString(5, x.getDate());
                    paramStm.setString(6, x.getHeure());

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                        paramStm.close();
                        return true;
                }
            paramStm.close();
        }
        catch (SQLException ex) {
             Logger.getLogger(NotificationDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean delete(Notification x) {
        String req = "DELETE FROM notification WHERE `ID_NOTIFICATION` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);
                paramStm.setString(1, x.getIdNotification());

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                        paramStm.close();
                    return true;
                }
                paramStm.close();
        }
        catch (SQLException ex) {
            Logger.getLogger(NotificationDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
        }
        return false;
    }


    @Override
    public List<Notification> findAll() {
        List<Notification> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM notification");
            while (r.next()) {
                Notification n = new Notification();
                n.setIdNotification(r.getString("ID_NOTIFICATION"));
                n.setType(r.getString("TYPE"));
                n.setMessage(r.getString("MESSAGE"));
                n.setVue(r.getInt("VUE"));
                n.setDate(r.getString("DATE"));
                n.setHeure(r.getString("HEURE"));

                liste.add(n);
            }
           // Collections.sort(liste);
            Collections.reverse(liste);
            r.close();
            stm.close();
        }
        catch (SQLException ex) {
            Logger.getLogger(NotificationDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
        }
        return liste;
    }

    public Notification findById(String id) {
        String req = "SELECT * FROM notification WHERE `ID_NOTIFICATION` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(id));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Notification n = new Notification();
                n.setIdNotification(resultat.getString("ID_NOTIFICATION"));
                n.setType(resultat.getString("TYPE"));
                n.setMessage(resultat.getString("MESSAGE"));
                n.setVue(resultat.getInt("VUE"));
                n.setDate(resultat.getString("DATE"));
		n.setHeure(resultat.getString("HEURE"));

                resultat.close();
                paramStm.close();
                    return n;
            }

            resultat.close();
            paramStm.close();

        }
        catch (SQLException exp) {
            Logger.getLogger(NotificationDAO.class.getName())
                            .log(Level.SEVERE, null, exp);
        }

        return null;
    }
}
