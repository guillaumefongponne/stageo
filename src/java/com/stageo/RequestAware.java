/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author AC
 */
public interface RequestAware {
    public void setRequest(HttpServletRequest request);
    public void setResponse(HttpServletResponse response);    
}
