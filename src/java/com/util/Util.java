/**
    This file is part of DefiLecture.

    DefiLecture is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DefiLecture is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DefiLecture.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.charset.StandardCharsets.UTF_16;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;

/**
 *
 * @author Joel
 */
public class Util {

    /**
     * Cette méthode retourne un String
     * qui représente la chaîne encodée en UTF-8
     *
     * @param someString La chaîne à encoder en UTF-8
     * @return String La chaîne encodée en UTF-8
     */
    public static String toUTF8(String someString){
        byte[] ptext = someString.getBytes(ISO_8859_1); //forme un tableau de bytes selon le format latin [format intermédiaire ± arbitraire]
        String value = new String(ptext, UTF_8);        //forme un String selon le format UTF-8 à partir du tableau de bytes récupéré.

        return value;
    }
    /**
     * Cette méthode retourne un String
     * qui représente la chaîne encodée en UTF-16
     *
     * @param someString La chaîne à encoder en UTF-16
     * @return String La chaîne encodée en UTF-16
     * @throws java.io.UnsupportedEncodingException
     */
    public static String toUTF16(String someString) throws UnsupportedEncodingException{
//        byte[] ptext = someString.getBytes(ISO_8859_1); //forme un tableau de bytes selon le format latin [format intermédiaire ± arbitraire]
        String value = new String(someString.getBytes("UTF-8"), "UTF-16");        //forme un String selon le format UTF-16 à partir du tableau de bytes récupéré.

        return value;
    }
    
    public static byte[] convertFileContentToBlob(String filePath) throws IOException {
	// create file object
	File file = new File(filePath);
	// initialize a byte array of size of the file
	byte[] fileContent = new byte[(int) file.length()];
	FileInputStream inputStream = null;
	try {
		// create an input stream pointing to the file
		inputStream = new FileInputStream(file);
		// read the contents of file into byte array
		inputStream.read(fileContent);
	} catch (IOException e) {
		throw new IOException("Unable to convert file to byte array. " + e.getMessage());
	} finally {
		// close input stream
		if (inputStream != null) {
			inputStream.close();
		}
	}
	return fileContent;
    }
}
