<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.Utilisateur"%>
<%@page import="com.stageo.dao.UtilisateurDAO"%>
<%@page import="com.stageo.model.ListeChoix"%>
<%@page import="com.util.Util"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="empdao" class="com.stageo.dao.EmployeurDAO">
    <jsp:setProperty name="empdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="cdao" class="com.stageo.dao.CompagnieDAO">
    <jsp:setProperty name="cdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="adao" class="com.stageo.dao.AdresseDAO">
    <jsp:setProperty name="adao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="listeCompagnies" value="${daoCompagnie.findByIdEmployeur(sessionScope.connecte)}"/>
<c:set var="employeur" scope="page" value="${empdao.read(sessionScope.connecte)}"/>
<c:set var="compagnie" scope="page" value="${cdao.read(employeur.compagnie.idCompagnie)}"/>
<c:set var="adresse" scope="page" value="${adao.read(compagnie.adresse.idAdresse)}"/>

<jsp:include page="modals/modal-modif-compte.jsp"></jsp:include>

<div class="jumbotron jumbotron-fluid stylish-color white-text">
    <section class="container">
        <article class="row justify-content-center">
            <div class="col-lg-6 col-12 text-center">
                <h1><i class="fas fa-user-circle" style="font-size:150px"></i></h1>
                <h3>${Util.toUTF8(employeur.prenom)} ${Util.toUTF8(employeur.nom)}</h3>
                <h4>@${employeur.nomUtilisateur}</h4>
                <h5>${employeur.email}</h5>
                <h5>${fn:toUpperCase(employeur.role)}</h5>
                <p>
                    Membre depuis : ${employeur.profilCree}
                    <br>
                    Dernière modification : ${employeur.profilModifie}
                </p>
                <a data-toggle="modal" href="#monModalMotDePasse" class="btn btn-success btn-sm" role="button">Changer mon mot de passe</a>
                <a data-toggle="modal" href="#monModalNomUtilisateur" class="btn btn-success btn-sm" role="button">Changer mon nom d'utilisateur</a>
        </article>
    </section>
</div>

<section class="container">
    <form action="*.do" method="post" class="needs-validation" novalidate>
        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                <h1><strong>Mes informations</strong></h1>
            </div>

            <div class="col-12 col-lg-6">
                <section class="container-fluid p-0">
                    <article class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Nom</label>
                                <input type="text" class="form-control" name="nom" value="${Util.toUTF8(employeur.nom)}" required>
                                <div class="invalid-feedback">Veuillez entrer votre nom.</div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Prénom</label>
                                <input type="text" class="form-control" name="prenom" value="${Util.toUTF8(employeur.prenom)}" required>
                                <div class="invalid-feedback">Veuillez entrer votre pr&eacute;nom.</div>
                            </div>
                        </div>
                    </article>
                </section>
                
                <div class="form-group">
                    <label for="">Adresse courriel</label>
                    <label class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Format : abcd@nom-de-domaine.com"></label>
                    <input type="email" class="form-control" name="email" value="${employeur.email}" required>
                    <div class="invalid-feedback">Veuillez entrer une adresse courriel valide.</div>
                </div>
                <div class="form-group">
                    <label for="">Numéro de téléphone</label>
                    <label class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Format : XXX-XXX-XXXX"></label>                    
                    <input type="tel" class="form-control" name="tel" value="${employeur.telephone}" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
                    <div class="invalid-feedback">Veuillez entrer un num&eacute;ro de t&eacute;l&eacute;phone valide.</div>
                </div>
                <div class="form-group">
                    <label for="">Fonction</label>
                    <input type="text" class="form-control" name="fonction" value="${Util.toUTF8(employeur.fonction)}" required>
                    <div class="invalid-feedback">Veuillez entrer votre fonction.</div>
                </div>
                
            </div>
        </article>

        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                <h1><strong>Coordonnées de mon entreprise</strong></h1>
            </div>

            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <label for="">Nom</label>
                    <input type="text" class="form-control" name="nomCompagnie" value="${Util.toUTF8(compagnie.nom)}" required>
                    <div class="invalid-feedback">Veuillez entrer le nom de votre entreprise.</div>
                </div>
                <div class="form-group">
                    <label for="">Secteur</label>
                    <input type="text" class="form-control" name="secteur" value="${Util.toUTF8(compagnie.secteur)}" required>
                    <div class="invalid-feedback">Veuillez entrer le secteur d'activit&eacute; de votre entreprise.</div>
                </div>
                <div class="form-group">
                    <label for="">Taille</label>
                    <select class='form-control' name="taille" required>
                        <option value=''>Choisir une taille...</option>
                        <c:forEach items="${ListeChoix.getListeTailleEntreprise()}" var="taille">
                            <c:choose>
                                <c:when test="${taille == Util.toUTF8(compagnie.taille)}">
                                    <option value='${taille}' selected>${taille}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value='${taille}'>${taille}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez sélectionner la taille qui correspond à celle de votre entreprise.</div>
                </div>
                <div class="form-group">
                    <label for="">Nombre d'employés</label>
                    <input type="number" min="1" class="form-control" name="nbEmployes" value="${compagnie.nbEmployes}" required>
                    <div class="invalid-feedback">Veuillez entrer le nombre approximatif d'employés de votre entreprise.</div>
                </div>
                <div class="form-group">
                    <label for="">Site web</label>
                    <input type="url" class="form-control" name="siteWeb" value="${compagnie.pageWeb}" required>
                    <div class="invalid-feedback">Veuillez entrer un lien web pour votre entreprise.</div>
                </div>
                <div class="form-group">
                    <label for="">Téléphone</label>
                    <label class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Format : XXX-XXX-XXXX"></label>
                    <input type="tel" class="form-control" name="telEntreprise" value="${compagnie.telEntreprise}" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
                    <div class="invalid-feedback">Veuillez un numéro de téléphone valide.</div>
                </div>
                <div class="form-group">
                    <label for="">Lien Google Maps</label>
                    <input type="url" class="form-control" name="lienGoogleMaps" value="${compagnie.lienGoogleMaps}" required>
                </div>
                <div class="form-group">
                    <label for="">Kilometrage</label>
                    <label class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Distance du collège Rosemont à l'entreprise"></label>                    
                    <input type="number" min="0" class="form-control not_required" name="kilometrage" value="${compagnie.kilometrage}" >
                </div>

            </div>
        </article>    

        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                <h1><strong>Adresse de mon entreprise</strong></h1>
            </div>

            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <label for="">Adresse ligne 1</label>
                    <input type="text" class="form-control" name="adresseLigne1" value="${Util.toUTF8(adresse.adresseLigne1)}" required>
                    <div class="invalid-feedback">Veuillez entrer l'adresse de votre entreprise.</div>
                </div>
                <div class="form-group">
                    <label for="">Adresse ligne 2</label>
                    <input type="text" class="form-control not_required" name="adresseLigne2" value="${Util.toUTF8(adresse.adresseLigne2)}" >
                </div>
                <div class="form-group">
                    <label for="">Ville</label>
                    <input type="text" class="form-control" name="villeCompagnie" value="${Util.toUTF8(adresse.ville)}" required>
                    <div class="invalid-feedback">Veuillez entrer la ville.</div>
                </div>
                <div class="form-group">
                    <label for="">Province</label>
                    <input type="text" class="form-control" name="province" value="${Util.toUTF8(adresse.province)}" required>
                    <div class="invalid-feedback">Veuillez entrer la province.</div>
                </div>
                <div class="form-group">
                    <label for="">Code postal</label>
                    <label i class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="X0X 0X0"></label>                    
                    <input type="text" class="form-control" name="codePostal" value="${adresse.codePostal}" pattern='[A-Z][0-9][A-Z] [0-9][A-Z][0-9]' required>
                    <div class="invalid-feedback">Veuillez entrer un code postal valide.</div>
                </div>
                <div class="form-group">
                    <label for="">Pays</label>
                    <input type="text" class="form-control" name="pays" value="${Util.toUTF8(adresse.pays)}" required>
                    <div class="invalid-feedback">Veuillez entrer votre pays.</div>
                </div>
            </div>
        </article>
        
        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                
            </div>
            
            <div class="col-12 col-lg-6 d-flex justify-content-between">
                <button type="submit" class="btn btn-success" name="tache" value="effectuerModificationUtilisateur">
                    Enregistrer les modifications
                </button>
                <button type="submit" class="btn btn-light" name="tache" value="afficherPageProfil">Annuler</button>
            </div>
            
        </article>
    </form>
</section>
