<!DOCTYPE html>
    
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="Accueil">
    <div class="container pt-5">
        <div class="row" >
            <div class="col-md-6 col-12">
                <div class="card">
                    <div class="card-header titreProfil2">
                        <h1 class="card-title mt-3">Bienvenue</h1>
                    </div>
                    <div class="card-body">
                        <p>Bienvenue sur la plateforme de gestion des Stages en TI du Collège de Rosemont</p>
                    </div>

                </div>
            </div>
            
            <div class='col-md-6 col-12'>
                <section class="form-elegant">
                    <form id="formConnection" class="my-2 my-lg-0 ml-auto" action="*.do?tache=effectuerConnexion" method="post">
                        <div class="card">
                            <div class="card-body mx-4">
                                <!--Header-->
                                <div class="text-center">
                                    <h3 class="dark-grey-text mb-5"><strong>Connexion</strong></h3>
                                </div>

                                <!--Body-->
                                <div class="md-form">
                                    <input type="text" class="form-control mr-sm-2" name="nomUtilisateurConnexion" id='nomUtilisateurConnexion' required>
                                    <label for="nomUtilisateurConnexion">Nom d'utilisateur</label>
                                </div>

                                <div class="md-form pb-3">
                                    <input type="password" class= "form-control mr-sm-2" name="motDePasseConnexion" id='motDePasseConnexion' required>
                                    <label for="motDePasseConnexion">Mot de passe</label>
                                </div>

                                <div class="text-center mb-3">
                                    <button type="submit" name="tache" value="effectuerConnexion" class="btn my-btn-outline-primary my-2 my-sm-0">Se connecter</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </section>
            </div>
        </div>
    </div>
</div>

<div class="container pt-5">
    <div class="row" >
        <div class="col-md-6 col-12">
            <div class="card">
                <div class="card-header titreProfil2">
                    <h1 class="card-title mt-3">Informatique de gestion</h1>
                </div>
                <div class="card-body">
                    <p>Pour les programmeurs-analyste, que ce soit dans le domaine pour développer des applications Web ou mobiles, la variété de stages est très grande </p>
                </div>
            </div>
        </div>
        
        <div class="col-md-6 col-12 py-5 py-lg-0">
            <div class="card">
                <div class="card-header titreProfil2">
                    <h1 class="card-title mt-3">Gestion de réseaux</h1>
                </div>
                <div class="card-body">
                    <p>Pour les gestionnaires en réseaux, la maintenance et la surveillance des réseaux informatiques est en demande élevée parmi les grandes entreprises </p>
                </div>
            </div>
        </div>          
    </div>
</div>
