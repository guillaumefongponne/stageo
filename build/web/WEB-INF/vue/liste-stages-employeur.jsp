<%-- 
    Document   : page-compagnie
    Created on : 2018-12-06, 10:48:57
    Author     : Dave
--%>

<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.stageo.model.Critere"%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="daoCompagnie" scope="page" class="com.stageo.dao.CompagnieDAO">
    <jsp:setProperty name="daoCompagnie" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoOffreStage" scope="page" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="daoOffreStage" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoCandidatureStage" scope="page" class="com.stageo.dao.CandidatureDAO">
    <jsp:setProperty name="daoCandidatureStage" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoUtilisateur" scope="page" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="daoUtilisateur" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="listeCriteres" value="${daoCritere.findAll()}"/>
<c:set var="listeOffres" value="${daoOffreStage.findByIdEmployeur(sessionScope.connecte)}"/>

<jsp:include page="/WEB-INF/vue/modals/modalDetailsOffres.jsp"></jsp:include>
        
<section class="container listeStages">
    <article class="row py-5">
        <div class="col-lg-6 col-12">
            <h3>Offres de stage</h3>
        </div>
        <!-- Barre de recherche -->
        <div class='col-lg-6 col-12'>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text"><i class="fas fa-search"></i></label>
                </div>
                <input id="inputRecherche" onkeyup = "rechercherStage()"type="text" class="form-control" placeholder="Rechercher une offre">
            </div>
        </div>
        </article>
        <!-- Fin de la barre de recherche -->
    <article class="row">
        <table id="stage" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th onclick="sortTable(0)">Titre </th>
                    <th>Date de début </th>
                    <th>Date de fin </th>
                    <th>Date de publication</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${listeOffres}" var="offres" >
                    <fmt:formatDate var="dateDebut" value="${offres.dateDebut}" pattern="yyyy-MM-dd" />
                    <fmt:formatDate var="dateFin" value="${offres.dateFin}" pattern="yyyy-MM-dd" />
                    <tr>
                        <td>${offres.titre}</td>
                        <td hidden>${offres.description}</td>
                        <td hidden>${offres.lienWeb}</td>
                        <td >${dateDebut}</td>
                        <td >${dateFin}</td>
                        <td>${offres.date}</td>
                        <td hidden>${offres.idOffre}</td>
                        <td hidden>${offres.remunere}</td>
                        <td hidden>${offres.lienDocument}</td>
                        <td hidden>${offres.nbVues}</td>
                        <td hidden><c:forEach items="${offres.listeCritere}" var="critere"><c:out value="${critere.idCritere}" /> </c:forEach></td>
                            
                        </tr>

                </c:forEach> 
            </tbody>
        </table>
    </article>
</section>

<jsp:include page="/WEB-INF/vue/modals/modalConfirmation.jsp"></jsp:include>
                                        
<script>
    function ResetForm() {
        $(':input', '#offrestage')
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .removeAttr('checked')
                .removeAttr('selected');
    }
    function animDetails(elemnt) {
        if ((document.getElementById("mainDetails").classList.contains("affichDetails")) &&
                (elemnt.textContent.includes('Masquer details'))) {
            hideDetails(elemnt);
        } else {
            showDetails(elemnt);
        }
    }
    function showDetails(elemnt) {
        liste_btn = document.getElementsByClassName("animDetails");
        for (var i = 0; i < liste_btn.length; i++) {
            liste_btn[i].innerHTML = "Afficher details"
        }
        document.getElementById("mainDetails").classList.remove("masquerDetails");
        document.getElementById("mainDetails").classList.add("affichDetails");
        document.getElementById("mainDetails").classList.add("col-md-5");
        elemnt.innerHTML = "Masquer details";
    }

    function hideDetails(elemnt) {
        document.getElementById("mainDetails").classList.remove("affichDetails");
        document.getElementById("mainDetails").classList.remove("col-md-5");
        document.getElementById("mainDetails").classList.add("masquerDetails");
        elemnt.innerHTML = "Afficher details";

    }

    function UncheckAll()
    {
        var checkboxes = new Array();
        checkboxes = document.getElementsByTagName('input');

        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type === 'checkbox') {
                checkboxes[i].checked = false;
            }
        }
    }
    var table = document.getElementById('stage');
    var listeCompetence;

    for (var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function ()
        {
            
            UncheckAll();
            console.log(this.cells[5].innerHTML);
            console.log(${dateFin});
            console.log(${dateDebut});
            listeCompetence = this.cells[10].innerHTML;
            listeCompetence = listeCompetence.trim();
            listeCompetence = listeCompetence.split(" ");
            console.log(listeCompetence);
            document.getElementById("titre").value = this.cells[0].innerHTML;
            document.getElementById("description").value = this.cells[1].innerHTML;
            document.getElementById("idOffre").value = this.cells[6].innerHTML;
            document.getElementById("lienWeb").value = this.cells[2].innerHTML;
            document.getElementById("dateDebut").value = this.cells[3].innerHTML;
            document.getElementById("dateFin").value = this.cells[4].innerHTML;
            document.getElementById("remuneration").value = this.cells[7].innerHTML;
            
            for (var i = 0; i < listeCompetence.length; i++) {
                if (listeCompetence[0] !== ""){
                    document.getElementById(listeCompetence[i]).checked = true;
                }
            }
            
            $('#modalDetailsOffres').modal('show');
        };

    }
    ;
    function rechercherStage() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("inputRecherche");
    filter = input.value.toUpperCase();
    table = document.getElementById("stage");
    tr = table.getElementsByTagName("tr");

    for (i = 1; i < tr.length; i++) {
        var td0 = tr[i].getElementsByTagName("td")[0].textContent.toUpperCase();
        var td1 = tr[i].getElementsByTagName("td")[1].textContent.toUpperCase();
        var td2 = tr[i].getElementsByTagName("td")[2].textContent.toUpperCase();
        var td3 = tr[i].getElementsByTagName("td")[3].textContent.toUpperCase();
        if (td0.indexOf(filter) > -1 || td1.indexOf(filter) > -1 || td2.indexOf(filter) > -1 || td3.indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }      
    }
    }
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("stage");
  switching = true;
  dir = "asc"; 

  while (switching) {
    switching = false;
    rows = table.rows;

    for (i = 1; i < (rows.length - 1); i++) {

      shouldSwitch = false;

      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      
      
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      switchcount ++;      
    } else {

      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
</script>

<style>
    th{
        cursor:pointer;
    }
</style>
