/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function enleverCompetence(i) {
    document.getElementById("competence" + i).remove();
}

function validerFormInscription() {
    var x = document.getElementById("ConfirmPassword").value;
    var y = document.getElementById("NewPassword").value;
    if (x !== y) {
        alert(" Vos Mots de passes sont différents");
        return false;
    }
}
// Disable form submissions if there are invalid fields
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Get the forms we want to add validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    
                    alert('Veuiller vérifier vos champs');
                    event.preventDefault();
                    event.stopPropagation();
                    
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();


