/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
//    $("#sidebar").mCustomScrollbar({
//        theme: "minimal"
//    });

    $('.miniConnexion').hide();

    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('.sidebarCollapse').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    
});

// Affichage du formulaire de connexion apres avoir fait un scroll de 500px
$(document).scroll(function () {
    var y = $(this).scrollTop();
    if (y > 500) {
        $('.miniConnexion').fadeIn();
    } else {
        $('.miniConnexion').fadeOut();
    }

});




