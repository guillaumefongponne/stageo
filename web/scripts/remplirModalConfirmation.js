/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function remplirModalConfirmation(){
    document.getElementById("msgModalConfirmation").innerHTML = document.getElementById("msgConfirmation").value;
    document.getElementById("tacheConfirmation").value = document.getElementById("tache").value;
    document.getElementById("idOffreConfirmation").value = document.getElementById("idOffre").value;
};

function remplirModalConfirmationDesactiver(){
    document.getElementById("msgModalConfirmation").innerHTML = document.getElementById("msgConfirmationDesactiver").value;
    document.getElementById("tacheConfirmation").value = document.getElementById("tacheDesactiver").value;
    document.getElementById("idOffreConfirmation").value = document.getElementById("idOffre").value;
};

function remplirModalConfirmationConfirmerCandidature(){
    document.getElementById("msgModalConfirmation").innerHTML = document.getElementById("msgConfirmation").value;
    document.getElementById("tacheConfirmation").value = document.getElementById("tache").value;
    document.getElementById("idOffreConfirmation").value = document.getElementById("idOffre").value;
    document.getElementById("idEtudiantConfirmation").value = document.getElementById("idEtudiant").value;
};

function remplirModalConfirmationModifierOffre(){
    document.getElementById("msgModalConfirmation").innerHTML = document.getElementById("msgConfirmation").value;
    document.getElementById("tacheConfirmation").value = document.getElementById("tache").value;
    document.getElementById("idOffreConfirmation").value = document.getElementById("idOffre").value;
    document.getElementById("titreConfirmation").value = document.getElementById("titre").value;
    document.getElementById("descriptionConfirmation").value = document.getElementById("description").value;
    document.getElementById("lienWebConfirmation").value = document.getElementById("lienWeb").value;
    document.getElementById("dateDebutConfirmation").value = document.getElementById("dateDebut").value;
    document.getElementById("dateFinConfirmation").value = document.getElementById("dateFin").value;
    document.getElementById("remunerationConfirmation").value = document.getElementById("remuneration").value;
    
    for (var i = 0; i < document.getElementsByClassName("critere").length; i++){
        if (document.getElementsByClassName("critere")[i].checked === true){
            document.getElementsByClassName("critereConfirmation")[i].checked = true;
        }
    }
};

function remplirModalConfirmationChoisirEtudiant(){
    document.getElementById("msgModalConfirmation").innerHTML = document.getElementById("msgConfirmation").value;
    document.getElementById("tacheConfirmation").value = document.getElementById("tache").value;
    document.getElementById("idOffreConfirmation").value = document.getElementById("offreID").value;
    document.getElementById("idCandidatureConfirmation").value = document.getElementById("candidatureID").value;
    
    document.getElementById("idCandidatureConfirmation").removeAttribute("name");
    document.getElementById("idOffreConfirmation").removeAttribute("name");
    document.getElementById("idCandidatureConfirmation").setAttribute("name", "candidatureID");
    document.getElementById("idOffreConfirmation").setAttribute("name", "offreID");
};


