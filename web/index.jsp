
<%@page import="java.util.Map"%>
<%--
    Author     : AC
--%>

<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.dao.UtilisateurDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="connexion" scope="application" class="com.stageo.jdbc.Connexion"></jsp:useBean>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Stages | Rosemont</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
<!--        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <!-- Bootstrap core CSS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
        
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/carousel.css">

    </head>
    
    <body>
        <header>
            <jsp:include page="/WEB-INF/vue/header.jsp"></jsp:include>
        </header>
        
        <main>
            <c:if test="${!empty data.message and data.tacheEffectuee == true}">
                <% ((Map<String, Object>) session.getAttribute("data")).put("tacheEffectuee", false); %>
            </c:if>
            <!-- Frame Modal Bottom -->
            <div class="modal fade bottom" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">

                <!-- Add class .modal-frame and then add class .modal-bottom (or other classes from list above) to set a position to the modal -->
                <div class="modal-dialog modal-frame modal-top" role="document">
                    <div id='typeModal' class="modal-content success-color">
                        <div class="modal-body">
                            <input id="tacheEffectuee" type="hidden" value="${data.tacheEffectuee}">
                            <input id="messageType" type="hidden" value="${data.messageType}">
                            <div class="row d-flex justify-content-center align-items-center">
                                <p id='message' class="pt-3 pr-2 white-text">${data.message}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Frame Modal Bottom -->
            <%-- <c:if test="${!empty requestScope.data}">
                <p><c:out value="${requestScope.data}"></c:out></p>
            </c:if> --%>

            <c:choose>
                <c:when test="${ !empty requestScope.vue }">
                    <c:set var="vue" value="/WEB-INF/vue/${requestScope.vue}"/>
                    <jsp:include page="${vue}" ></jsp:include>                
                </c:when>
                <c:otherwise>
                    <jsp:include page="/WEB-INF/vue/page-accueil.jsp"></jsp:include>
                </c:otherwise>
            </c:choose>
        </main>
        
        <footer>
            <jsp:include page="/WEB-INF/vue/footer.jsp"></jsp:include>
        </footer>

<!--        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>-->

        <!-- JQuery -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- Bootstrap tooltips -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <!-- MDB core JavaScript -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
        
        <script type="text/javascript" src="scripts/animations.js"></script>
        <script type="text/javascript" src="scripts/show-hide.js"></script>
        <script type="text/javascript" src="scripts/scripts.js"></script>
        <script type="text/javascript" src="scripts/alert-close.js"></script>
        <script type="text/javascript" src="scripts/remplirModalConfirmation.js"></script>
        <script type="text/javascript" src="scripts/sort.js"></script>
        <script type="text/javascript" src="scripts/carousel.js"></script>
        
        <script>
            $(document).ready(function(){
                if (document.getElementById('messageType').value === "Succes"){
                    document.getElementById('typeModal').className = "modal-content success-color";
                };
                
                if (document.getElementById('messageType').value === "Echec"){
                    document.getElementById('typeModal').className = "modal-content danger-color";
                };
                
                if (document.getElementById('message').innerHTML !== "" && document.getElementById('tacheEffectuee').value !== "false"){
                    $('#modalMessage').modal('show');
                    window.setTimeout(function(){
                        $('#modalMessage').modal('hide');
                    }, 2000);
                    document.getElementById('tacheEffectuee').value = "false";
                };
            });
        </script>

</body>
</html>
