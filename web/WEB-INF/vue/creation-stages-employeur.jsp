<%-- 
    Document   : page-compagnie
    Created on : 2018-12-06, 10:48:57
    Author     : Dave
--%>

<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="daoCompagnie" scope="page" class="com.stageo.dao.CompagnieDAO">
    <jsp:setProperty name="daoCompagnie" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoOffreStage" scope="page" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="daoOffreStage" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="listeCriteres" value="${daoCritere.findAll()}"/>
<c:set var="listeOffres" value="${daoOffreStage.findByIdEmployeur(sessionScope.connecte)}"/>
<div class="container py-5" >



    <div class="row justify-content-center" >

        <div class="col-md-7">
            <div class="card mb-3">
                <div class="card-header titreProfil2">
                    <h1 class="card-title ">  Nouvelle Offre de stage
                        <button type="reset" class="btn my-btn-outline-secondary"><i class="fas fa-sync-alt"></i></button></h1>
                </div>
                <div class="card-body">
                    <!-- FORMULAIRE D'AJOUT D'UN STAGE !!! FORMULAIRE D'AJOUT D'UN STAGE !!! FORMULAIRE D'AJOUT D'UN STAGE !!! -->
                    <form class="form-horizontal needs-validation" novalidate id="offrestage"action="*.do?tache=ajouterOffreDeStage" method="post" enctype="multipart/form-data"> 

                        <div class="form-group ">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Titre :</label>
                                </div>
                                <input type="text"  name="titre" class="form-control" id="titre"  placeholder="Titre" required>
                                <div class="invalid-feedback">Veuillez entrer un titre pour votre stage.</div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Description :</label>
                                </div>
                                <input type="text"  name="desc" class="form-control" id="desc" placeholder="Description" required>
                                <div class="invalid-feedback">Veuillez entrer une description pour le stage.</div>
                            </div>
                            <div class="form-row">
                                <div class="input-group mb-3 col-sm-6">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Début :</label>
                                    </div>
                                    <input type="date"  name="date_debut" class="form-control" id="date_debut"  placeholder="Date de début" required>
                                    <div class="invalid-feedback">Veuillez entrer une date de debut.</div>
                                </div>

                                <div class="input-group mb-3 col-sm-6">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Fin :</label>
                                    </div>
                                    <input type="date"  name="date_fin" class="form-control" id="date_fin"  placeholder="Date de fin" required>
                                    <div class="invalid-feedback">Veuillez entrer une date de fin.</div>
                                </div>
                            </div>

                            <div  hidden class="input-group mb-3 ">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Lien web :</label>
                                </div>
                                <input type="text"  name="lienWeb"  class="form-control not_required" id="lienWeb"  placeholder="Lien web" value="x" >

                            </div>
                            <div hidden="" class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Lien Document :</label>
                                </div>
                                <input type="text"  name="document"  class="form-control not_required" id="document"  placeholder="Document">

                            </div>
                            <div class="form-row">
                                <div class="input-group mb-3 col-sm-6">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Rémunération (en $/h):</label>
                                    </div>
                                    <input type="text"  name="remuneration" class="form-control not_required" id="remuneration" value="0"  placeholder="Rémunération" required>
                                </div>

                                <div class="input-group mb-3 col-sm-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">CV</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="form-control-file" accept=".pdf" id="fichier" name="fichier">

                                        </div>
                                    </div>


                                </div>
                            </div>
                            <table  id="competence" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Critère <a href="#" class="fa fa-arrows-alt-v"></a></th>
                                        <th scope="col">Maitrîse <a href="#" class="fa fa-arrows-alt-v"></a></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${listeCriteres}" var="critere">
                                        <tr>
                                            <td name="critereNom">${critere.nom}
                                            </td>
                                            <td>
                                                <input name="${critere.idCritere}" class="checkbox form-control mr-auto" type="checkbox" id="${critere.idCritere}">
                                            </td>
                                        </tr>
                                    </c:forEach>

                                </tbody>
                            </table>
                            <input type="hidden" name="tache" value="ajouterOffreDeStage">
                            <input type="hidden" id="idOffre" name="idOffre">
                            <button type="submit" class="btn btn-block my-btn-outline-primary">Ajouter un stage</button>
                            <!-- FIN FORMULAIRE D'AJOUT D'UN STAGE !!!  FIN FORMULAIRE D'AJOUT D'UN STAGE !!!  FIN FORMULAIRE D'AJOUT D'UN STAGE !!! -->
                        </div> 
                    </form>

                </div>
            </div>
        </div>
<!--    <div class="col-md-5">
        <div class="card">
            <div class="card-header titreProfil">
                <h1 class="card-title ">Mes stages</h1>
            </div>
            <div class="card-body">
                <table id="stage" class="table table-hover">
                    <thead>
                        <tr>
                            <td>Titre <a href="#" class="fa fa-arrows-alt-v titreProfil2"></a></td>
                        </tr>
                    </thead>

                    <tbody>
<%--<c:forEach items="${listeOffres}" var="offres">
                            <fmt:formatDate var="dateDebut" value="${offres.dateDebut}" pattern="yyyy-MM-dd" />
                            <fmt:formatDate var="dateFin" value="${offres.dateFin}" pattern="yyyy-MM-dd" />
                            <tr>
                                <td>${offres.titre}</td>
                                <td hidden>${offres.description}</td>
                                <td hidden>${offres.lienWeb}</td>
                                <td hidden>${dateDebut}</td>
                                <td hidden>${dateFin}</td>
                                <td hidden>${offres.idOffre}</td>
                                <td hidden>${offres.remunere}</td>
                                <td hidden>${offres.lienDocument}</td>
                                <td hidden>${offres.nbVues}</td>
                            </tr>
</c:forEach> --%>
                    </tbody>
                </table>
            </div>
        </div>

    </div>-->
</div>
</div>
</div>
<script>
    function ResetForm() {
        $(':input', '#offrestage')
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .removeAttr('checked')
                .removeAttr('selected');
    }
    var table = document.getElementById('stage');

    for (var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function ()
        {

            console.log(this.cells[5].innerHTML);
            console.log(${dateFin});
            console.log(${dateDebut});
            document.getElementById("titre").value = this.cells[0].innerHTML;
            document.getElementById("desc").value = this.cells[1].innerHTML;
            document.getElementById("idOffre").value = this.cells[5].innerHTML;
            document.getElementById("lienWeb").value = this.cells[2].innerHTML;
            document.getElementById("date_debut").value = this.cells[3].innerHTML;
            document.getElementById("date_fin").value = this.cells[4].innerHTML;
            document.getElementById("remuneration").value = this.cells[6].innerHTML;
        };

    }
    ;

</script>