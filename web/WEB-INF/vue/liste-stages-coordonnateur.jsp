<%-- 
    Document   : ListeStagesEleves
    Created on : Dec 6, 2018, 10:26:06 AM
    Author     : JP
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="dao" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeStage" value="${dao.findAll()}"/>
<jsp:useBean id="udao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="udao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="cdao" class="com.stageo.dao.CompagnieDAO">
    <jsp:setProperty name="cdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="odao" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="odao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="edao" class="com.stageo.dao.EmployeurDAO">
    <jsp:setProperty name="edao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="daoAdresse" scope="page" class="com.stageo.dao.AdresseDAO">
    <jsp:setProperty name="daoAdresse" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set  var="listeCriteres" value="${daoCritere.findAll()}"/>

<jsp:include page="/WEB-INF/vue/modals/modalDetailsOffres.jsp"></jsp:include>

<section class='container py-5'>
    <article class='row'>
        <div class='col-lg-6 col-12'>
            <h1>Rechercher Stage</h1>
        </div>
        <div class='col-lg-6 col-12'>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text"><i class="fas fa-search"></i></label>
                </div>
                <input id="inputRecherche" onkeyup= "rechercherStage()" type="text" class="form-control" placeholder="Recherche par mot-clef...">
            </div>
        </div>
    </article>
    
    <article class='row'>
        <div class='col-12'>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text">
                        <button class="btn my-btn-outline-secondary dropdown-toggle main-input" type="button" data-toggle="dropdown"> Filtrer par Compétences <span class="caret"></span></button>
                        <ul class="dropdown-menu" id="liste">
                            <c:forEach items="${listeCriteres}" varStatus="loop" var="critereRecherche">
                                <li class="elementCritere"name="${critereRecherche.nom}">${critereRecherche.nom}
                                </li>
                            </c:forEach>
                        </ul>
                    </label>
                </div>
                <!-- Conteneur de comperence -->
                <div class="col-sm-8" >
                    <div id="conteneurCompetences">

                    </div>
                </div>
            </div>
        </div>
    </article>
    
    <article class='row'>
        <table id="table" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th onclick="sortTable(0, 'table')">Nom Compagnie</th>
                    <th onclick="sortTable(1, 'table')">Titre</th>
                    <th>Remunération</th>
                    <th>Statut candidature</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach  varStatus="loop" items="${listeStage}" var="stage">
                    <fmt:formatDate var="dateDebut" value="${stage.dateDebut}" pattern="yyyy-MM-dd" />
                    <fmt:formatDate var="dateFin" value="${stage.dateFin}" pattern="yyyy-MM-dd" />
                    <c:set var="employeur"  value="${edao.read(stage.idEmployeur)}"/>
                    <c:set var="user"  value="${udao.read(stage.idEmployeur)}"/>
                    <c:set var="compagnieid"  value="${employeur.compagnie.idCompagnie}"/>
                    <c:set var="compagnie"  value="${cdao.read(compagnieid)}"/>
                    <c:set var="criteresStage"  value="${daoCritere.findByOffre(stage.idOffre)}"/>
                    <c:set var="adresse"  value="${daoAdresse.read(compagnie.adresse.idAdresse)}"/>                    
                    <tr id="${stage.idOffre}">
                    <td id="${stage.titre}">${compagnie.nom}</td>
                    <td id="${stage.titre}">${stage.titre}</td>
                        <td hidden>${stage.description}</td>
                        <td hidden>${dateDebut}</td>
                        <td hidden>${dateFin}</td>
                        <td hidden>${stage.lienWeb}</td>
                        <td hidden>${stage.lienDocument}</td>
                        <td>${stage.nbVues}</td>
                        <td hidden>${stage.fichier}</td>
                        <td hidden>${stage.idOffre}</td>
                        <td>${stage.remunere}</td>
                        <td hidden>${compagnie.adresse.toString()}</td>
                        <c:choose>
                            <c:when test="${criteresStage.size() != 0}">
                                <td name="crit" hidden>
                                    <c:forEach  items="${criteresStage}" var="crit">
                                        [${crit.nom}]  
                                    </c:forEach>
                                </td>
                            </c:when>
                            <c:otherwise>
                                <td name="crit" hidden></td>
                            </c:otherwise>
                        </c:choose>
                            
                        <td hidden>${user.nom}</td>
                        <td hidden>${user.prenom}</td>
                        <td hidden>${user.email}</td>
                        <td hidden>${user.telephone}</td>

                        <td hidden>${employeur.fonction}</td>
                        <td hidden>${compagnie.secteur}</td>
                        <td hidden>${compagnie.pageWeb}</td>
                        <td hidden>${compagnie.telEntreprise}</td>
                        <td hidden>${compagnie.lienGoogleMaps}</td>
                        <td hidden>${compagnie.taille}</td>
                        <td hidden>${compagnie.kilometrage}</td>
                        <td hidden>${compagnie.nbEmployes}</td>

                        <td hidden>${adresse.adresseLigne1}</td>
                        <td hidden>${adresse.adresseLigne2}</td>
                        <td hidden>${adresse.ville}</td>
                        <td hidden>${adresse.codePostal}</td>
                        <td hidden>${adresse.province}</td>
                        <td hidden>${adresse.pays}</td>
                        <td hidden>${stage.statutOffre}</td>
                    </tr>

                </c:forEach>

            </tbody>
        </table>
    </article>
</section>

<jsp:include page="/WEB-INF/vue/modals/modalConfirmation.jsp"></jsp:include>

<!-- Fin footer -->
<script>
    var nbComp = 0;


    var table = document.getElementById('table');

    for (var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function ()
        {
            document.getElementById("nomEntreprise").value = this.cells[0].innerHTML;
            document.getElementById("titre").value = this.cells[1].innerHTML;
            document.getElementById("description").value = this.cells[2].innerHTML;
//            document.getElementById("dateDebut").value = this.cells[3].innerHTML;
//            document.getElementById("dateFin").value = this.cells[4].innerHTML;
//            document.getElementById("lienWeb").value = this.cells[5].innerHTML;
//            document.getElementById("nbVues").textContent = this.cells[7].innerHTML;
//            document.getElementById("fichier").value = this.cells[8].innerHTML;
            document.getElementById("idOffre").value = this.cells[9].innerHTML;
//            document.getElementById("remuneration").value = this.cells[10].innerHTML;
            document.getElementById("nom").value = this.cells[13].innerHTML;
            document.getElementById("prenom").value = this.cells[14].innerHTML;
            document.getElementById("fonction").value = this.cells[17].innerHTML;
            document.getElementById("email").value = this.cells[15].innerHTML;
            document.getElementById("tel").value = this.cells[16].innerHTML;
            document.getElementById("secteur").value = this.cells[18].innerHTML;
            document.getElementById("telEntreprise").value = this.cells[20].innerHTML;
            document.getElementById("pageWeb").value = this.cells[19].innerHTML;
            document.getElementById("lienGoogleMaps").value = this.cells[21].innerHTML;
            document.getElementById("taille").value = this.cells[22].innerHTML;
            document.getElementById("kilometrage").value = this.cells[23].innerHTML;
            document.getElementById("nbEmployes").value = this.cells[24].innerHTML;
            document.getElementById("adresseLigne1").value = this.cells[25].innerHTML;
            document.getElementById("adresseLigne2").value = this.cells[26].innerHTML;
            document.getElementById("ville").value = this.cells[27].innerHTML;
            document.getElementById("codePostal").value = this.cells[28].innerHTML;
            document.getElementById("province").value = this.cells[29].innerHTML;
            document.getElementById("pays").value = this.cells[30].innerHTML;
            document.getElementById("criteresStageContent").innerHTML = "";
            $('#' + this.cells[9].innerHTML).children("td[name='crit']").each(function (index) {
                document.getElementById("criteresStageContent").innerHTML += "<h2 class='Catpill'><span class='badge badge-pill mb-1'>" + $(this).text() + "</span></h2>";
            });
            $(".Catpill").css({"display": "inline"});
            
            console.log(this.cells[31].innerHTML);
            if (this.cells[31].innerHTML === "true"){
                document.getElementById("btnAccepter").className = "btn my-btn-outline-primary d-none";
                document.getElementById("btnDesactiver").className = "btn my-btn-outline-primary";
            }
            else{
                document.getElementById("btnAccepter").className = "btn my-btn-outline-primary";
                document.getElementById("btnDesactiver").className = "btn my-btn-outline-primary d-none";
            }
            
            $("#modalDetailsOffres").modal('show');

            /*
             console.log(document.getElementsByName("crit"+this.cells[9].innerHTML));
             for (var x in document.getElementsByName("crit"+this.cells[9].innerHTML)){
             document.getElementById("criteresStageContent").innerHTML += "<span class='badge badge-pill badge-primary'>" + x + "</span>";
             }*/


        };

    };

    $(document).ready(function () {
        $(".elementCritere").click(function (event) {
            document.getElementById("conteneurCompetences").innerHTML += "<span class='competence badge badge-primary'  onclick='enleverCompetence(" + nbComp + ")' id='competence" + nbComp + "'>" + event.target.innerHTML + " <a class='fas fa-times' ></a></span>";
            $(".competence").addClass("alert alert-info ")
                    .css("margin", "2%");
            nbComp++;
        });

    });
    
    function rechercherStage() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("inputRecherche");
    filter = input.value.toUpperCase();
    table = document.getElementById("stage");
    tr = table.getElementsByTagName("tr");

    for (i = 1; i < tr.length; i++) {
        var td0 = tr[i].getElementsByTagName("td")[0].textContent.toUpperCase();
        var td1 = tr[i].getElementsByTagName("td")[1].textContent.toUpperCase();
        var td2 = tr[i].getElementsByTagName("td")[2].textContent.toUpperCase();
        var td3 = tr[i].getElementsByTagName("td")[3].textContent.toUpperCase();
        if (td0.indexOf(filter) > -1 || td1.indexOf(filter) > -1 || td2.indexOf(filter) > -1 || td3.indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }      
    }
    }
    

</script>

<style>
    th{
        cursor:pointer;
    }
</style>

