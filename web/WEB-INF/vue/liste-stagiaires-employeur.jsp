<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.*"%>
<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.dao.EtudiantDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeUser" value="${dao.findAll()}"/>
<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set  var="listeCriteres" value="${daoCritere.findAll()}"/>

<jsp:include page="/WEB-INF/vue/modals/modalDetailsEtudiants.jsp"></jsp:include>

<section class="container py-5">
    <article class="row">
        <div class="col-12 col-lg-6">
            <h1>Rechercher Stagiaire</h1>
        </div>
        
        <div class="col-12 col-lg-6">
            <!-- Barre de recherche -->
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text"><i class="fas fa-search"></i></label>
                </div>
                <input id="inputRecherche" type="text" class="form-control" placeholder="Recherche par mot-clef...">
            </div>
            <!-- Fin de la barre de recherche -->
        </div>
    </article>
    
    <article class="row">
        <div class="col-12">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text">
                        <button class="btn my-btn-outline-secondary dropdown-toggle main-input" type="button" data-toggle="dropdown"> Filtrer par Compétences <span class="caret"></span></button>

                        <ul class="dropdown-menu" id="liste">
                             <c:forEach items="${listeCriteres}" varStatus="loop" var="critereRecherche">
                                 <li class="elementCritere"name="${critereRecherche.nom}">${critereRecherche.nom}
                                 </li>
                             </c:forEach>
                        </ul>
                    </label>
                </div>
                <!-- Conteneur de comperence -->
                <div class="col-sm-8" >
                    <div id="conteneurCompetences">

                    </div>
                </div>
            </div>
        </div>
    </article>
    
    <article class="row">
        <div class="col-12">
            <table id="table" class="table table-hover">
                <thead>
                    <tr>
                        <th>Nom <a class="sortBy" href="#"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                        <th>prenom <a class="sortBy" href="#"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                        <th>Domaine <a class="sortBy" href="#"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listeUser}" var="user">
                        <c:if test = "${user.role == 'etudiant'}">
                            <c:if test = "${user.statutRecherche ne 'trouve'}">
                                <tr>

                                    <td>${user.nom}</td>
                                    <td>${user.prenom}</td>
                                    <td hidden>${user.email}</td>
                                    <td hidden>${user.statutRecherche}</td>
                                    <td hidden>${user.listeCritere}</td>
                                    
                                    <td hidden>${etudiant.telephone}</td>
                                    <td hidden>${Util.toUTF8((edao.read(etudiant.idUtilisateur)).ville)}</td>
                                    <td hidden>${Util.toUTF8((edao.read(etudiant.idUtilisateur)).profilEtudes)}</td>
                                    <td hidden>${Util.toUTF8((edao.read(etudiant.idUtilisateur)).stageSession)}</td>
                                    <td hidden>${Util.toUTF8((edao.read(etudiant.idUtilisateur)).anglaisEcrit)}</td>
                                    <td hidden>${Util.toUTF8((edao.read(etudiant.idUtilisateur)).anglaisParle)}</td>
                                    <td hidden>${Util.toUTF8((edao.read(etudiant.idUtilisateur)).permisConduire)}</td>
                                    <td hidden>${Util.toUTF8((edao.read(etudiant.idUtilisateur)).demarcheEnCours)}</td>
                                    <td hidden>${Util.toUTF8((edao.read(etudiant.idUtilisateur)).login365)}</td>
                                    <td hidden>${Util.toUTF8((edao.read(etudiant.idUtilisateur)).numeroDA)}</td>
                                    <td hidden>${Util.toUTF8((edao.read(etudiant.idUtilisateur)).moyenTransport)}</td>
                                    <td hidden>${Util.toUTF8(((edao.read(etudiant.idUtilisateur)).preferences))}</td>
                                </tr>
                            </c:if>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </article>
    
</section>

<div class="container" >
    <div class="row" >

        <div class="col-md-7">
            <div class="card">
                <div class="card-header titreProfil2">
                    <h1 class="card-title ">Rechercher Stagiaire</h1>
                </div>
                <!-- Section de recherche des stages -->
                <!-- Barre de recherche -->
                <div class="card-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text"><i class="fas fa-search"></i></label>
                        </div>
                        <input id="inputRecherche" type="text" class="form-control" placeholder="Recherche par mot-clef...">
                    </div>
                    <!-- Fin de la barre de recherche -->

                    <div   id='sectionCompetences'>
                        <!-- bouton pour la liste -->
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text">
                                    <button class="btn my-btn-outline-secondary dropdown-toggle main-input" type="button" data-toggle="dropdown"> Filtrer par Compétences <span class="caret"></span></button>
                                 
                                    <ul class="dropdown-menu" id="liste">
                                         <c:forEach items="${listeCriteres}" varStatus="loop" var="critereRecherche">
                                             <li class="elementCritere"name="${critereRecherche.nom}">${critereRecherche.nom}
                                             </li>
                                         </c:forEach>
                                    </ul>
                                </label>
                            </div>
                            <!-- Conteneur de comperence -->
                            <div class="col-sm-8" >
                                <div id="conteneurCompetences">

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Fin de section de recherche par competances -->


                    <div class="">
                        <table id="table" class="table table-hover borderTable">
                            <thead>
                                <tr>
                                    <th>Nom <a class="sortBy" href="#"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                                    <th>prenom <a class="sortBy" href="#"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                                    <th>Domaine <a class="sortBy" href="#"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listeUser}" var="user">
                                    <c:if test = "${user.role == 'etudiant'}">
                                        <c:if test = "${user.statutRecherche ne 'trouve'}">
                                            <tr>

                                                <td>${user.nom}</td>
                                                <td>${user.prenom}</td>
                                                <td hidden>${user.email}</td>
                                                <td hidden>${user.statutRecherche}</td>
                                                <td hidden>${user.listeCritere}</td>
                                            </tr>
                                        </c:if>
                                    </c:if>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- Fin de la section de recherche des stages -->
        <div class="col-md-5" >
            <div class='card'>
                <div class="card-header titreProfil">
                    <h1>Détails de l'étudiant</h1>
                    <small muted><span id='nbVues'>0</span> candidatures</small>
                    <form>
                        <input type="hidden" name="tache" value="EnvoyerNotificationRappel">
                        <input type="hidden" id="idOffre" name="idOffre">
                        <button type="submit" class="btn my-btn-outline-primary"><i class="fas fa-bell"></i></button>
                    </form>
                </div>
                <fieldset class="card-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Nom :</label>
                        </div>
                        <input disabled type="text"  name="nom" class="form-control" id="nom" value="${user.nom}" placeholder="${session.getAttribute("connecte")}">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Prénom :</label>
                        </div>
                        <input  disabled type="text"  name="prenom" class="form-control" id="prenom" value="${user.prenom}" placeholder="${user.prenom}">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Courriel :</label>
                        </div>
                        <input disabled type="email" name="email" class="form-control" id="email" value="${user.email}" placeholder="${user.email}" >
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" >Téléphone :</label>
                        </div>
                        <input disabled type="text"  placeholder=" pas dans la database" name="tel" disabled class="form-control" id="tel">
                    </div>

                    <div class="input-group mb-3">

                        <input type="text" class="form-control" disabled>
                        <div class="input-group-append ">
                            <label  class="input-group-text btn btn-primary ml-auto ">
                                <span class="">
                                    CV ...
                                    <input disabled type="file" class="form-control-file" accept=".pdf" style="display: none;" id="relNote">
                                </span>
                            </label>
                        </div>


                    </div>

                    <div class="input-group mb-3">

                        <input type="text" class="form-control" disabled>
                        <div class="input-group-append ">
                            <label  class="input-group-text btn btn-primary ml-auto ">
                                <span class="">
                                    Relevé de note ...
                                    <input disabled type="file" class="form-control-file" accept=".pdf" style="display: none;" id="relNote">
                                </span>
                            </label>
                        </div>


                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">https://fr.linkedin.com/in/</label>
                        </div>
                        <input disabled type="text" placeholder=" pas dans la database"  class="form-control" id="lK" >
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">https://gitlab.com/</label>
                        </div>
                        <input disabled type="text" placeholder=" pas dans la database"  class="form-control" id="git" >

                    </div>

                    <fieldset>
                        <legend><h4>Profil de stagiaire 
                            </h4>
                        </legend>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <label class="input-group-text " >Domaine :</label>
                            </div>
                            <input disabled type="text" placeholder=" pas dans la database"  class="form-control" id="domaine" >

                        </div>

                        <div class="form-group" id="critereEtudiant">

                            <c:forEach items="${listeCriteres}" var="critere"> 

                                <td name="critereNom">${critere.nom}

                                </c:forEach> 

                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <label class="input-group-text " >Ma Recherche : </label>
                            </div>
                            <input disabled type="text" class="form-control" id="statutRecherche" >

                        </div>
                    </fieldset>
                    </fieldset>
            </div>

        </div>
    </div>
</div>

<!-- Fin footer -->
<script>
    var nbComp = 0;

    var table = document.getElementById('table');

    for (var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function ()
        {
            //rIndex = this.rowIndex;
            document.getElementById("prenom").value = this.cells[0].innerHTML;
            document.getElementById("nom").value = this.cells[1].innerHTML;
            document.getElementById("email").value = this.cells[2].innerHTML;
            document.getElementById("statutRecherche").value = this.cells[3].innerHTML;
            var x =this.cells[4].innerHTML;
            for (var i = 0; i < x.length; i++)
            {
                console.log (x[i]);
            }
        };
    }
$(document).ready(function() {
    $(".elementCritere").click(function(event) {
      document.getElementById("conteneurCompetences").innerHTML += "<span class='competence badge badge-primary'  onclick='enleverCompetence("+nbComp+")' id='competence"+nbComp+"'>"+event.target.innerHTML+" <a class='fas fa-times' ></a></span>";
                $(".competence").addClass("alert alert-info ")
                        .css("margin","2%");
                nbComp++;
    });
    
});

</script>