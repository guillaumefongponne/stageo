
<!--
    Author: Alex -- La gestion des erreurs dans le form est effectuée.
-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<div class="Accueil">
    <div class="container py-5" >
        <div id="carteinscription" class="card inscription">
            <div class="card-body">
                <h1 class="card-title inscriptionTitre">Création du compte</h1>
                <form  id="formInscription" class='needs-validation' novalidate action="*.do?tache=effectuerInscription" method="post">
                    <div class="input-group mb-3 ml-auto">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Créer un compte pour un</label>
                        </div>
                        <select class="form-control"  id="role" name="role">
                            <option value="etudiant"> étudiant <i class="fas fa-caret-down"></i></option>
                            <option value="employeur" > employeur</option>
                            <option value="professeur" > professeur</option>
                            <option value="coordonnateur" > coordonnateur</option>
                        </select>
                    </div>
                    <div class="input-group mb-3 ml-auto">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Nom d'utilisateur</label>
                        </div>
                            <input type="text" class="form-control" id="username" name="username" required>
                            <div class='invalid-feedback'>Vous devez saisir un nom d'utilisateur</div>
                    </div>

                    <div class="input-group mb-3 ml-auto">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Mot de Passe</label>
                        </div>
                        <input  type="password" class="form-control" id="NewPassword" name="motDePasse" required>
                        <div class='invalid-feedback'>Vous devez saisir un mot de passe</div>
                    </div>

                    <div class="input-group mb-3 ml-auto">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Confirmer Mot de passe</label>
                        </div>
                        <input type="password" class="form-control" id="ConfirmPassword" name="motDePasseRepeat" required>
                        <div class='invalid-feedback'id="msgConfirmPassword">Vous devez confirmer votre mot de passe</div>
                    </div>
                    <button type="submit" name="tache" value="effectuerInscription" class="btn btn-block my-btn-outline-secondary">Créer</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var password = document.getElementById("NewPassword")
            , confirm_password = document.getElementById("ConfirmPassword")
            , message_error = document.getElementById('msgConfirmPassword');

    function validatePassword() {
        if (password.value !== confirm_password.value) {
            confirm_password.setCustomValidity("Les mots de passe doivent etre identiques");
            message_error.innerHTML = "Les mots de passe doivent etre identiques";
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>
