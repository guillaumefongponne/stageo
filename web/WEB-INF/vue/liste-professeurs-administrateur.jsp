<%-- 
    Document   : liste-professeurs-administrateur
    Created on : Dec 6, 2019, 12:44:49 PM
    Author     : Nicole
--%>

<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.*"%>
<%@page import="com.util.Util"%>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="udao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="udao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoProfesseur" scope="page" class="com.stageo.dao.ProfesseurDAO">
    <jsp:setProperty name="daoProfesseur" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set  var="listeProfesseur" value="${daoProfesseur.findAll()}"/>

<jsp:include page="/WEB-INF/vue/modals/modalDetailsProfesseurs.jsp"></jsp:include>

<section class='container py-5'>
    <article class='row'>
        <div class='col-12 col-lg-6'>
            <h1>Rechercher Professeurs</h1>
        </div>
        <div class='col-12 col-lg-6'>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text"><i class="fas fa-search"></i></label>
                </div>
                <input id="inputRecherche" type="text" class="form-control" placeholder="Recherche par mot-clef...">
            </div>
        </div>
    </article>
    
    <article class='row'>
        <table id="table" class="table table-hover">
            <thead>
                <tr>
                    <th>Nom <a href="#" class="fa fa-arrows-alt-v"></a></th>
                    <th>Prénom <a href="#" class="fa fa-arrows-alt-v"></a></th>
                    <th>Courriel <a href="#" class="fa fa-arrows-alt-v"></a></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${listeProfesseur}" var="professeur">
                    <c:set var="user" value="${udao.read(professeur.idUtilisateur)}"></c:set>
                        <tr>
                            <td>${user.nom}</td>
                            <td>${user.prenom}</td>
                            <td>${user.email}</td>
                                                        
                            <td hidden>${user.nom}</td>
                            <td hidden>${user.prenom}</td>
                            <td hidden>${user.email}</td>
                            <td hidden>${user.telephone}</td>
                            
                            <td hidden>${Util.toUTF8(professeur.ville)}</td>
                            <td hidden>${Util.toUTF8(professeur.anglaisEcrit)}</td>
                            <td hidden>${Util.toUTF8(professeur.anglaisParle)}</td>
                            <td hidden>${Util.toUTF8(professeur.moyenTransport)}</td>
                            <td hidden>${Util.toUTF8(professeur.infosPartagees)}</td>
                            <td hidden>${Util.toUTF8(professeur.preferenceSupervision)}</td>
                            <td hidden>${Util.toUTF8(professeur.dispoStageEte)}</td>                            
                        </tr>
                </c:forEach>
            </tbody>
        </table>
    </article>
</section>

<script>
    var nbComp = 0;

    var table = document.getElementById('table');

    for (var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function ()
        {
            
            //rIndex = this.rowIndex;
            document.getElementById("prenom").value = this.cells[4].innerHTML;
            document.getElementById("nom").value = this.cells[3].innerHTML;
            document.getElementById("email").value = this.cells[5].innerHTML;
            document.getElementById("tel").value = this.cells[6].innerHTML;
            document.getElementById("ville").value = this.cells[7].innerHTML; 
            document.getElementById("anglaisEcrit").value = this.cells[8].innerHTML; 
            document.getElementById("anglaisParle").value = this.cells[9].innerHTML; 
            document.getElementById("moyenTransport").value = this.cells[10].innerHTML; 
            document.getElementById("infoPartagees").value = this.cells[11].innerHTML; 
            document.getElementById("preferenceSupervision").value = this.cells[12].innerHTML; 
            document.getElementById("dispoStageEte").value = this.cells[13].innerHTML;      
            $(".Catpill").addClass("blue")
                    .css({"display": "inline"});
            
            $('#modalDetailsProfesseurs').modal('show');
        };
    }
</script>
