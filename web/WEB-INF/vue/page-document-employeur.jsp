<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="daoDocument" scope="session" class="com.stageo.dao.DocumentDAO">
    <jsp:setProperty name="daoDocument" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeDocument" value="${daoDocument.findAll()}"/>

<div class="container py-5">
    <div class="row">
        <div class="col-6 mx-auto">
            <div class="card">
                <div class="card-header titreProfil2">
                    <h1>Documents</h1>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Nom</th>
                                <th scope="col">Date</th>
                                <th scope="col">Vues</th>
                                <th scope="col">Consulter</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listeDocument}" var="documents"> 
                                <fmt:formatDate var="dateDoc" value="${documents.date}" pattern="yyyy-MM-dd" />
                                <tr >
                                    <td>${documents.titre}</td>
                                    <td>${dateDoc}</td>
                                    <td>${documents.nbVues}</td>
                                    <td><a target="Télécharger" href="?tache=lireDocument&id=${documents.idDocument}"><i class="fas fa-download titreProfil2"></i></a></li></td>
                                </tr>
                            </c:forEach> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>