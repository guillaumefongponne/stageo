<%-- 
    Document   : footer
    Created on : Nov 17, 2019, 11:54:43 AM
    Author     : guill
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!-- Footer -->
<footer class="page-footer font-small deep-purple darken-2">
    <!-- Footer Elements -->
    <div class="container">
        <div class='row align-items-center'>
            <div class='col-lg-4 black-text py-3'>
                <a class="my-3" href="?tache=afficherPageAccueil">
                    <img  class="d-inline-block align-top" id="logo" src="./images/logoCegep.png"/>
                </a>
                <p class='py-3 white-text'>6400 16e Avenue, Montréal, QC H1X 2S9</p>
            </div>
            <div class='col-lg-8 py-3 black-text text-uppercase'>
                <nav class='nav justify-content-end white-text'>
                    <a class='nav-link' href="https://www.crosemont.qc.ca/nous-contacter/" target="_blank">Nous joindre</a>
                    <a class='nav-link' href="https://www.crosemont.qc.ca/" target="_blank">Site web du collège</a>
                    <a class='nav-link' href="https://crosemont.omnivox.ca/Login/Account/Login?ReturnUrl=%2fintr" target="_blank">Portail Omnivox</a>
                </nav>

            </div>
        </div>



        <!-- Grid row-->
        <div class="row">
            <!-- Grid column -->
            <div class="col-md-12 pb-5">
                <div class="mb-5 flex-center">
                    <!-- Facebook -->
                    <a class="fb-ic" href="https://www.facebook.com/collegederosemont?rf=108092609224792" target="_blank">
                        <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <!-- Twitter -->
                    <a class="tw-ic">
                        <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <!-- Google +-->
                    <a class="gplus-ic">
                        <i class="fab fa-google-plus-g fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <!--Linkedin -->
                    <a class="li-ic">
                        <i class="fab fa-linkedin-in fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <!--Instagram-->
                    <a class="ins-ic">
                        <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <!--Pinterest-->
                    <a class="pin-ic">
                        <i class="fab fa-pinterest fa-lg white-text fa-2x"> </i>
                    </a>
                </div>
            </div>
            <!-- Grid column -->
        </div>
        <!-- Grid row-->
    </div>
    <!-- Footer Elements -->
</footer>
<!-- Footer -->
<!-- Footer -->
<footer class="page-footer font-small deep-purple darken-2">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2019 Copyright:
        <a href="https://www.crosemont.qc.ca/" target="_blank"> Collège de Rosemont</a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->
