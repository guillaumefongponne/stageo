<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.*"%>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="udao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="udao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoCompagnie" scope="page" class="com.stageo.dao.CompagnieDAO">
    <jsp:setProperty name="daoCompagnie" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoOffreStage" scope="page" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="daoOffreStage" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoAdresse" scope="page" class="com.stageo.dao.AdresseDAO">
    <jsp:setProperty name="daoAdresse" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoEmployeur" scope="page" class="com.stageo.dao.EmployeurDAO">
    <jsp:setProperty name="daoEmployeur" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set  var="listeCompagnie" value="${daoCompagnie.findAll()}"/>
<c:set var="listeEmployeurs" value="${daoEmployeur.findAll()}"></c:set>
<c:set var="listeOffres" value="${daoOffreStage.findByIdEmployeur(sessionScope.connecte)}"/>

<jsp:include page="/WEB-INF/vue/modals/modalDetailsEmployeurs.jsp"></jsp:include>

<section class='container py-5'>
    <article class='row'>
        <div class='col-12 col-lg-6'>
            <h1>Rechercher Compagnies</h1>
        </div>
        <div class='col-12 col-lg-6'>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text"><i class="fas fa-search"></i></label>
                </div>
                <input id="inputRecherche" onkeyup= "rechercherEntreprise()" type="text" class="form-control" placeholder="Recherche par mot-clef...">
            </div>
        </div>
    </article>
    
    <article class='row'>
        <table id="table" class="table table-hover">
            <thead>
                <tr>
                    <th onclick="sortTable(0, 'table')">Nom</th>
                    <th onclick="sortTable(1, 'table')">Contact</th>
                    <th onclick="sortTable(2, 'table')">Site Web</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${listeEmployeurs}" var="employeur">
                    <c:set var="user" value="${udao.read(employeur.idUtilisateur)}"></c:set>
                    <c:set var="compagnie" value="${daoCompagnie.read(employeur.compagnie.idCompagnie)}"></c:set>
                    <c:set var="adresse" value="${daoAdresse.read(compagnie.adresse.idAdresse)}"></c:set>
                        <tr>
                            <td>${compagnie.nom}</td>
                            <td>${user.nom} ${user.prenom}</td>
                            <td>${compagnie.pageWeb}</td>
                            
                            <td hidden>${user.nom}</td>
                            <td hidden>${user.prenom}</td>
                            <td hidden>${user.email}</td>
                            <td hidden>${user.telephone}</td>
                            
                            <td hidden>${employeur.fonction}</td>
                            <td hidden>${compagnie.nom}</td>
                            <td hidden>${compagnie.secteur}</td>
                            <td hidden>${compagnie.pageWeb}</td>
                            <td hidden>${compagnie.telEntreprise}</td>
                            <td hidden>${compagnie.lienGoogleMaps}</td>
                            <td hidden>${compagnie.taille}</td>
                            <td hidden>${compagnie.kilometrage}</td>
                            <td hidden>${compagnie.nbEmployes}</td>
                            
                            <td hidden>${adresse.adresseLigne1}</td>
                            <td hidden>${adresse.adresseLigne2}</td>
                            <td hidden>${adresse.ville}</td>
                            <td hidden>${adresse.codePostal}</td>
                            <td hidden>${adresse.province}</td>
                            <td hidden>${adresse.pays}</td>
                        </tr>
                </c:forEach>
            </tbody>
        </table>
    </article>
</section>

<!--ANCIEN CODE-->
<!--<div class="container" >
    <div class="row" >
        <div class="col-md-7">
            <div class="card">
                <div class="card-header titreProfil2">
                    <h1 class="card-title ">Rechercher Compagnies</h1>
                </div>
                 Section de recherche des compagnies 
                 Barre de recherche 
                <div class="card-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text"><i class="fas fa-search"></i></label>
                        </div>
                        <input id="inputRecherche" type="text" class="form-control" placeholder="Recherche par mot-clef...">
                    </div>
                     Fin de la barre de recherche 

                    <div class="">
                        <table id="table" class="table table-hover borderTable">
                            <thead>
                                <tr>
                                    <th>Nom <a href="#" class="fa fa-arrows-alt-v"></a></th>
                                    <th>Site Web <a href="#" class="fa fa-arrows-alt-v"></a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listeCompagnie}" var="compagnie">
                                        <tr>
                                            <td>${compagnie.nom}</td>
                                            <td>${compagnie.pageWeb}</td>
                                        </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>-->
        <!-- Fin de la section de recherche des stages -->
        
<!-- Fin footer -->
<script>
    var nbComp = 0;

    var table = document.getElementById('table');

    for (var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function ()
        {
            
            //rIndex = this.rowIndex;
            document.getElementById("prenom").value = this.cells[4].innerHTML;
            document.getElementById("nom").value = this.cells[3].innerHTML;
            document.getElementById("email").value = this.cells[5].innerHTML;
            document.getElementById("tel").value = this.cells[6].innerHTML;
            document.getElementById("fonction").value = this.cells[7].innerHTML; 
            document.getElementById("nomEntreprise").value = this.cells[8].innerHTML; 
            document.getElementById("secteur").value = this.cells[9].innerHTML; 
            document.getElementById("pageWeb").value = this.cells[10].innerHTML; 
            document.getElementById("telEntreprise").value = this.cells[11].innerHTML; 
            document.getElementById("lienGoogleMaps").value = this.cells[12].innerHTML; 
            document.getElementById("taille").value = this.cells[13].innerHTML; 
            document.getElementById("kilometrage").value = this.cells[14].innerHTML; 
            document.getElementById("nbEmployes").value = this.cells[15].innerHTML; 
            document.getElementById("adresseLigne1").value = this.cells[16].innerHTML; 
            document.getElementById("adresseLigne2").value = this.cells[17].innerHTML;
            document.getElementById("ville").value = this.cells[18].innerHTML;
            document.getElementById("codePostal").value = this.cells[19].innerHTML;
            document.getElementById("province").value = this.cells[20].innerHTML;
            document.getElementById("pays").value = this.cells[21].innerHTML;
//            $('#' + this.cells[4].innerHTML).children("td[name='crit']").each(function (index) {
//                document.getElementById("criteresStageContent").innerHTML += "<h2 class='Catpill'><span class='badge badge-pill mb-1'>" + $(this).text() + "</span></h2>";
//            });
            $(".Catpill").addClass("blue")
                    .css({"display": "inline"});
            
            $('#modalDetailsEmployeurs').modal('show');
        };
    }
    
    function rechercherEntreprise() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("inputRecherche");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");

    for (i = 1; i < tr.length; i++) {
        var td0 = tr[i].getElementsByTagName("td")[0].innerHTML.toUpperCase();
        var td1 = tr[i].getElementsByTagName("td")[1].textContent.toUpperCase();
        var td2 = tr[i].getElementsByTagName("td")[2].textContent.toUpperCase();
        if (td0.indexOf(filter) > -1 || td1.indexOf(filter) > -1 || td2.indexOf(filter) > -1 ) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }      
    }
    }
    
    function sortTableActif(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("table");
  switching = true;
  dir = "asc"; 

  while (switching) {
    switching = false;
    rows = table.rows;

    for (i = 1; i < (rows.length - 1); i++) {

      shouldSwitch = false;

      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {

      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      switchcount ++;      
    } else {

      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}


</script>

<style>
    th{
        cursor:pointer;
    }
</style>
        
