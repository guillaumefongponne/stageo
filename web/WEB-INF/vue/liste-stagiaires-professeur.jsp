<%-- 
    Document   : liste-stagiaires-professeur
    Created on : 2019-10-10, 15:39:27
    Author     : soleil123
--%>

<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.*"%>
<%@page import="com.util.Util"%>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="etudao" class="com.stageo.dao.EtudiantDAO">
    <jsp:setProperty name="etudao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="cadao" class="com.stageo.dao.CandidatureDAO">
    <jsp:setProperty name="cadao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="pdao" class="com.stageo.dao.ProfesseurDAO">
    <jsp:setProperty name="pdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="listeCandidatureProf" value="${cadao.findByProfesseur(sessionScope.connecte)}"/>
<c:set var="listeUser" value="${dao.findAll()}"/>
<c:set var="listeEtudiant" value="${etudao.findAll()}"/>

<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set  var="listeCriteres" value="${daoCritere.findAll()}"/>

<jsp:useBean id="canddao" class="com.stageo.dao.CandidatureDAO">
    <jsp:setProperty name="canddao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeCandidatures" value="${canddao.findAll()}"/>

<jsp:useBean id="sdao" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="sdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeStage" value="${sdao.findAll()}"/>

<jsp:useBean id="cdao" class="com.stageo.dao.CompagnieDAO">
    <jsp:setProperty name="cdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="odao" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="odao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="edao" class="com.stageo.dao.EmployeurDAO">
    <jsp:setProperty name="edao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="daoAdresse" scope="page" class="com.stageo.dao.AdresseDAO">
    <jsp:setProperty name="daoAdresse" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:include page="/WEB-INF/vue/modals/modalDetailsEtudiants.jsp"></jsp:include>

<section class="container py-5">
    <article class="row">
        <div class="col-12 col-lg-6">
            <h1>Rechercher Stagiaire</h1>
        </div>
        
        <div class="col-12 col-lg-6">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text"><i class="fas fa-search"></i></label>
                </div>
                <input id="inputRecherche" onkeyup="rechercherEtudiant()" type="text" class="form-control" placeholder="Recherche par mot-clef...">
            </div>
        </div>
    </article>
    
    <article class="row">
        <div class="col-12">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text">
                        <button class="btn my-btn-outline-secondary dropdown-toggle main-input" type="button" data-toggle="dropdown"> Filtrer par Compétences <span class="caret"></span></button>
                        <ul class="dropdown-menu" id="liste">
                            <c:forEach items="${listeCriteres}" varStatus="loop" var="critereRecherche">
                                <li class="elementCritere"name="${critereRecherche.nom}">${critereRecherche.nom}
                                </li>
                            </c:forEach>    
                        </ul>
                    </label>
                </div>
                <!-- Conteneur de comperence -->
                <div class="col-sm-8" >
                    <div id="conteneurCompetences">

                    </div>
                </div>
            </div>
        </div>
    </article>
    
    <article class="row">
        <div class="col-12">
            <table id="table-etudiant" class="table table-hover">
                <thead>
                    <tr>
                        <th onclick="sortTable(0, 'table-etudiant')">Nom <span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                        <th onclick="sortTable(1, 'table-etudiant')">Prenom <span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                        <th onclick="sortTable(2, 'table-etudiant')">Courriel <span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listeCandidatureProf}" var="candidature">
                        <c:set var="etudiant" value="${etudao.read(candidature.idEtudiant)}"></c:set>
                        <c:set var="user" value="${dao.read(candidature.idEtudiant)}"></c:set>
                        <tr>

                            <td>${user.nom}</td>
                            <td>${user.prenom}</td>
                            <td>${user.email}</td>
                            <td hidden>${etudiant.idUtilisateur}</td>
                            <td hidden>${user.telephone}</td>
                            <td hidden>${Util.toUTF8(etudiant.ville)}</td>
                            <td hidden>${Util.toUTF8(etudiant.profilEtudes)}</td>
                            <td hidden>${Util.toUTF8(etudiant.stageSession)}</td>
                            <td hidden>${Util.toUTF8(etudiant.anglaisEcrit)}</td>
                            <td hidden>${Util.toUTF8(etudiant.anglaisParle)}</td>
                            <td hidden>${Util.toUTF8(etudiant.permisConduire)}</td>
                            <td hidden>${Util.toUTF8(etudiant.demarcheEnCours)}</td>
                            <td hidden>${Util.toUTF8(etudiant.login365)}</td>
                            <td hidden>${Util.toUTF8(etudiant.numeroDA)}</td>
                            <td hidden>${Util.toUTF8(etudiant.moyenTransport)}</td>
                            <td hidden>${Util.toUTF8(etudiant.preferences)}</td>

                            <td hidden name="crit" hidden>
                            <c:forEach  items="${criteresStage}" var="crit">
                                ${crit.nom},
                            </c:forEach>
                            </td>

                            <td hidden>${etudiant.statutRecherche}</td>

                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            
            <!--TEST POUR AVOIR STAGE DE ETUDIANT -->
            <table id="table-stage" >
            <tbody>
                <c:forEach items="${listeCandidatures}" var="candidature">
                    <c:if test = "${candidature.statut eq 'Confirme'}">
                        <c:forEach  varStatus="loop" items="${listeStage}" var="stage">
                            <c:set var="stage" value="${sdao.findById(candidature.idOffre)}"/>
                                <!--c:set var="stage" value="${canddao.findByIdOffre(candidature.idOffre)}"/-->
                                <fmt:formatDate var="dateDebut" value="${stage.dateDebut}" pattern="yyyy-MM-dd" />
                                <fmt:formatDate var="dateFin" value="${stage.dateFin}" pattern="yyyy-MM-dd" />
                                <c:set var="employeur"  value="${edao.read(stage.idEmployeur)}"/>
                                <c:set var="user"  value="${udao.read(stage.idEmployeur)}"/>
                                <c:set var="compagnieid"  value="${employeur.compagnie.idCompagnie}"/>
                                <c:set var="compagnie"  value="${cdao.read(compagnieid)}"/>
                                <c:set var="criteresStage"  value="${daoCritere.findByOffre(stage.idOffre)}"/>
                                <c:set var="adresse"  value="${daoAdresse.read(compagnie.adresse.idAdresse)}"/>
                                <tr id="${stage.idOffre}" hidden>
                                    <td id="${stage.titre}" hidden>${compagnie.nom}</td>
                                    <td id="${stage.titre}" hidden>${stage.titre}</td>
                                    <td hidden>${stage.description}</td>
                                    <td hidden>${dateDebut}</td>
                                    <td hidden>${dateFin}</td>
                                    <td hidden>${stage.lienWeb}</td>
                                    <td hidden>${stage.lienDocument}</td>
                                    <td>${stage.nbVues}</td>
                                    <td hidden>${stage.fichier}</td>
                                    <td hidden>${stage.idOffre}</td>
                                    <td>${stage.remunere}</td>
                                    <td hidden>${compagnie.adresse.toString()}</td>
                                    <c:choose>
                                        <c:when test="${criteresStage.size() != 0}">
                                            <c:forEach  items="${criteresStage}" var="crit">
                                                <td name="crit" hidden>${crit.nom}</td>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>
                                            <td name="crit" hidden></td>
                                        </c:otherwise>
                                    </c:choose>

                                    <td hidden>${user.nom}</td>
                                    <td hidden>${user.prenom}</td>
                                    <td hidden>${user.email}</td>
                                    <td hidden>${user.telephone}</td>

                                    <td hidden>${employeur.fonction}</td>
                                    <td hidden>${compagnie.secteur}</td>
                                    <td hidden>${compagnie.pageWeb}</td>
                                    <td hidden>${compagnie.telEntreprise}</td>
                                    <td hidden>${compagnie.lienGoogleMaps}</td>
                                    <td hidden>${compagnie.taille}</td>
                                    <td hidden>${compagnie.kilometrage}</td>
                                    <td hidden>${compagnie.nbEmployes}</td>

                                    <td hidden>${adresse.adresseLigne1}</td>
                                    <td hidden>${adresse.adresseLigne2}</td>
                                    <td hidden>${adresse.ville}</td>
                                    <td hidden>${adresse.codePostal}</td>
                                    <td hidden>${adresse.province}</td>
                                    <td hidden>${adresse.pays}</td>
                                </tr>

                        </c:forEach>
                    </c:if>
                </c:forEach>

            </tbody>
        </table>
        </div>
    </article>
    
</section>
<jsp:include page="/WEB-INF/vue/modals/modalDetailsOffres.jsp"></jsp:include>


<!-- Fin footer -->
<script>  
    var nbComp = 0;

    var table1 = document.getElementById('table-etudiant');

    for (var i = 1; i < table1.rows.length; i++)
    {
        table1.rows[i].onclick = function ()
        {
            //rIndex = this.rowIndex;
            document.getElementById("prenom").value = this.cells[1].innerHTML;
            document.getElementById("nom").value = this.cells[0].innerHTML;
            document.getElementById("email").value = this.cells[2].innerHTML;
            document.getElementById("idUser").value = this.cells[3].innerHTML;
            document.getElementById("tel").value = this.cells[4].innerHTML;
            document.getElementById("ville").value = this.cells[5].innerHTML;
            document.getElementById("profilEtudes").value = this.cells[6].innerHTML;
            document.getElementById("stageSession").value = this.cells[7].innerHTML;
            document.getElementById("anglaisEcrit").value = this.cells[8].innerHTML;
            document.getElementById("anglaisParle").value = this.cells[9].innerHTML;
            document.getElementById("permisConduire").value = this.cells[10].innerHTML;
            document.getElementById("demarcheEnCours").value = this.cells[11].innerHTML;
            document.getElementById("login365").value = this.cells[12].innerHTML;
            document.getElementById("numeroDA").value = this.cells[13].innerHTML;
            document.getElementById("moyenTransport").value = this.cells[14].innerHTML;
            document.getElementById("preferences").innerHTML = this.cells[15].innerHTML;
            
            listeCriteres = this.cells[16].innerHTML;
            listeCriteres = listeCriteres.trim();
            listeCriteres = listeCriteres.split(",");
            console.log(listeCriteres);
            document.getElementById("criteresStageContent").innerHTML = "";
            for (var i = 0; i < listeCriteres.length; i++){
                document.getElementById("criteresStageContent").innerHTML += "<h2 class='Catpill'><span class='badge badge-pill mb-1'>" + listeCriteres[i] + "</span></h2>";
            }
            
//            $('#' + this.cells[4].innerHTML).children("td[name='crit']").each(function (index) {
//                document.getElementById("criteresStageContent").innerHTML += "<h2 class='Catpill'><span class='badge badge-pill mb-1'>" + $(this).text() + "</span></h2>";
//            });
            $(".Catpill").css({"display": "inline"});
            
            $('#modalDetailsEtudiants').modal('show');

        };
    }
    
    $(document).ready(function () {
        $(".elementCritere").click(function (event) {
            document.getElementById("conteneurCompetences").innerHTML += "<span class='competence badge badge-primary'  onclick='enleverCompetence(" + nbComp + ")' id='competence" + nbComp + "'>" + event.target.innerHTML + " <a class='fas fa-times' ></a></span>";
            $(".competence").addClass("alert alert-info ")
                    .css("margin", "2%");
            nbComp++;
        });

    });

    var nbComp2 = 0;
    var table2 = document.getElementById('table-stage');
    for (var i = 1; i < table2.rows.length; i++)
    {
        table2.rows[i].onclick = function ()
        {
            document.getElementById("nomEntreprise").value = this.cells[0].innerHTML;
            document.getElementById("titre").value = this.cells[1].innerHTML;
            document.getElementById("description").value = this.cells[2].innerHTML;
//            document.getElementById("dateDebut").value = this.cells[3].innerHTML;
//            document.getElementById("dateFin").value = this.cells[4].innerHTML;
//            document.getElementById("lienWeb").value = this.cells[5].innerHTML;
//            document.getElementById("nbVues").textContent = this.cells[7].innerHTML;
//            document.getElementById("fichier").value = this.cells[8].innerHTML;
            document.getElementById("idOffre").value = this.cells[9].innerHTML;
//            document.getElementById("remuneration").value = this.cells[10].innerHTML;
            document.getElementById("nom").value = this.cells[13].innerHTML;
            document.getElementById("prenom").value = this.cells[14].innerHTML;
            document.getElementById("fonction").value = this.cells[17].innerHTML;
            document.getElementById("email").value = this.cells[15].innerHTML;
            document.getElementById("tel").value = this.cells[16].innerHTML;
            document.getElementById("secteur").value = this.cells[18].innerHTML;
            document.getElementById("telEntreprise").value = this.cells[20].innerHTML;
            document.getElementById("pageWeb").value = this.cells[19].innerHTML;
            document.getElementById("lienGoogleMaps").value = this.cells[21].innerHTML;
            document.getElementById("taille").value = this.cells[22].innerHTML;
            document.getElementById("kilometrage").value = this.cells[23].innerHTML;
            document.getElementById("nbEmployes").value = this.cells[24].innerHTML;
            document.getElementById("adresseLigne1").value = this.cells[25].innerHTML;
            document.getElementById("adresseLigne2").value = this.cells[26].innerHTML;
            document.getElementById("ville").value = this.cells[27].innerHTML;
            document.getElementById("codePostal").value = this.cells[28].innerHTML;
            document.getElementById("province").value = this.cells[29].innerHTML;
            document.getElementById("pays").value = this.cells[30].innerHTML;
            document.getElementById("criteresStageContent").innerHTML = "";
            $('#' + this.cells[9].innerHTML).children("td[name='crit']").each(function (index) {
                document.getElementById("criteresStageContent").innerHTML += "<h2 class='Catpill'><span class='badge badge-pill mb-1'>" + $(this).text() + "</span></h2>";
            });
            $(".Catpill").css({"display": "inline"});
            
            
            
            $("#modalDetailsOffres").modal('show');



            /*
             console.log(document.getElementsByName("crit"+this.cells[9].innerHTML));
             for (var x in document.getElementsByName("crit"+this.cells[9].innerHTML)){
             document.getElementById("criteresStageContent").innerHTML += "<span class='badge badge-pill badge-primary'>" + x + "</span>";
             }*/


        };

    };

    $(document).ready(function () {
        $(".elementCritere").click(function (event) {
            document.getElementById("conteneurCompetences").innerHTML += "<span class='competence badge badge-primary'  onclick='enleverCompetence(" + nbComp + ")' id='competence" + nbComp + "'>" + event.target.innerHTML + " <a class='fas fa-times' ></a></span>";
            $(".competence").addClass("alert alert-info ")
                    .css("margin", "2%");
            nbComp2++;
        });

    });
    
    function rechercherEtudiant() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("inputRecherche");
        filter = input.value.toUpperCase();
        table = document.getElementById("table-etudiant");
        tr = table.getElementsByTagName("tr");

        for (i = 1; i < tr.length; i++) {
            var td0 = tr[i].getElementsByTagName("td")[0].textContent.toUpperCase();
            var td1 = tr[i].getElementsByTagName("td")[1].textContent.toUpperCase();
            var td2 = tr[i].getElementsByTagName("td")[2].textContent.toUpperCase();
            if (td0.indexOf(filter) > -1 || td1.indexOf(filter) > -1 || td2.indexOf(filter) > -1 ) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }      
        }
    }
    
</script>

<style>
    th{
        cursor:pointer;
    }
</style>
           
