<%-- 
    Document   : modal-modif-compte
    Created on : Oct 30, 2019, 5:56:08 PM
    Author     : guill
--%>

<div class="modal" id="monModalNomUtilisateur">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-titre">Changement du nom d'utilisateur</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="Reinitialiser" action="*.do?tache=modifierCompteUtilisateur" method="post" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label for="uname">Ancien nom d'utilisateur:</label>
                        <input type="text" class="form-control" name="AncienUsername" id="AncienUsername" required>
                        <div class="invalid-feedback">Veuiller entrer vote ancien nom d'utilisateur.</div>
                    </div>
                    <div class="form-group">
                        <label for="uname">Nouveau d'utilisateur:</label>
                        <input type="text" class="form-control" name="NouveauUsername" id="NouveauUsername" required>
                        <div class="invalid-feedback">Veuiller entrer votre nouveau nom d'utilisateur.</div>
                    </div>
                    <div class="modal-footer border-top-0">
                        <button type="submit" class="btn btn-success" name="Enregistrer" value="EnregistrerUsername">Enregistrer</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="monModalMotDePasse">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-titre">Changement de mot de passe</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="Reinitialiser" action="*.do?tache=modifierCompteUtilisateur" method="post" class="needs-validation" novalidate>
                    <div class="form-group">
                        <label for="uname">Ancien mot de passe:</label>
                        <input type="text" class="form-control validate" name="AncienPassword" id="AncienPassword" required>
                        <div class="invalid-feedback">Veuiller entrer vote ancien mot de passe.</div>
                    </div>
                    <div class="form-group">
                        <label for="uname">Nouveau mot de passe:</label>
                        <input type="text" class="form-control validate" name="NouveauPassword" id="NewPassword" required>
                        <div class="invalid-feedback">Veuiller entrer votre nouveau mot de passe.</div>
                    </div>
                    <div class="form-group">
                        <label for="uname">Confirmer nouveau mot de passe:</label>
                        <input type="text" class="form-control" name="ConfirmationPassword" id="ConfirmPassword" required>
                        <div class="invalid-feedback">Les mots de passe doivent etre identiques.</div>

                    </div>
                    <div class="modal-footer border-top-0">
                        <button type="submit" class="btn btn-success" name="Enregistrer" value="EnregistrerPassword">Enregistrer</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    var password = document.getElementById("NewPassword")
            , confirm_password = document.getElementById("ConfirmPassword");

    function validatePassword() {
        if (password.value !== confirm_password.value) {
            confirm_password.setCustomValidity("Les mots de passe doivent etre identiques");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>