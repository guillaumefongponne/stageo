    <%-- 
    Document   : modalDetailsEtudiants
    Created on : Nov 18, 2019, 4:55:10 PM
    Author     : guill
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<!-- modalDetailsEtudiants -->
<div class="modal fade right" id="modalDetailsEtudiants" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header deep-purple darken-2 justify-content-between">
                <div class="container-fluid">
                    <div class="row">
                        <h1 class="white-text">Détails de l'étudiant</h1>
                        <form>
                            <input type="hidden" name="tache" value="EnvoyerNotificationRappel">
                            <input type="hidden" id="idOffre" name="idOffre">
                        </form>
                        
                        <c:if test="${sessionScope.role == 'employeur'}">
                            <form>
                                <button type="button" onclick="remplirModalConfirmationChoisirEtudiant()" class="btn btn-success" data-toggle="modal" data-target="#modalConfirmation">Choisir cet étudiant</button>
                                <input class="form-control" type="hidden" name="candidatureID" value="" id="candidatureID">
                                <input class="form-control" type="hidden" name="offreID" value="" id="offreID">
                                <input type="hidden" id="msgConfirmation" value="Voulez-vous vraiment choisir cet(te) étudiant(e) pour le stage ?">
                                <input type="hidden" id="tache" value="choisirCandidature">
                            </form>
                        </c:if>
                        
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class='white-text'>&times;</span>
                        </button>
                    </div>
                    
                    <c:if test="${sessionScope.role != 'employeur'}">
                        <div class= "row">
                            <button type="button" class='btn my-btn-outline-primary' data-target="#modalDetailsOffres" data-toggle="modal" data-dismiss="modal">Détails du stage</button>
                        </div>
                    </c:if>
                </div>
            </div>

            <div class="modal-body">
                
                <div class="md-form">
                    <input value="default" type="text" name="nom" class="form-control" id="nom" disabled>
                    <label for="nom">Nom</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="prenom" class="form-control" id="prenom" disabled>
                    <label for="prenom">Prénom</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="email" class="form-control" id="email" disabled>
                    <label for="email">Courriel</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="tel" class="form-control" id="tel" disabled>
                    <label for="tel">Téléphone</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="ville" class="form-control" id="ville" disabled>
                    <label for="ville">Ville</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="profilEtudes" class="form-control" id="profilEtudes" disabled>
                    <label for="profilEtudes">Profil études</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="stageSession" class="form-control" id="stageSession" disabled>
                    <label for="stageSession">Stage Session</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="anglaisEcrit" class="form-control" id="anglaisEcrit" disabled>
                    <label for="anglaisEcrit">Anglais écrit</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="anglaisParle" class="form-control" id="anglaisParle" disabled>
                    <label for="anglaisParle">Anglais parlé</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="permisConduire" class="form-control" id="permisConduire" disabled>
                    <label for="permisConduire">Permis de conduire</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="demarcheEnCours" class="form-control" id="demarcheEnCours" disabled>
                    <label for="demarcheEnCours">Démarches en cours</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="login365" class="form-control" id="login365" disabled>
                    <label for="login365">Login 365</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="numeroDA" class="form-control" id="numeroDA" disabled>
                    <label for="numeroDA">Numéro DA</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="moyenTransport" class="form-control" id="moyenTransport" disabled>
                    <label for="moyenTransport">Moyen de transport</label>
                </div>
                <div class="md-form">
                    <textarea name="preferences" class="md-textarea form-control" id="preferences" disabled>default</textarea>
                    <label for="preferences">Préférences</label>
                </div>

                <fieldset>
                    <legend><h4>Profil de stagiaire</h4>
                    </legend>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend ">
                            <label class="input-group-text " >Domaine :</label>
                        </div>
                        <input disabled type="text" class="form-control" id="domaine" >

                    </div>

                    <div id="criteresStageContent">

                    </div>
                    <input type="hidden" id="idUser" name="idEtudiant">
                    

                </fieldset>
            </div>
        </div>
    </div>
</div>
<!-- modalDetailsEtudiants -->
