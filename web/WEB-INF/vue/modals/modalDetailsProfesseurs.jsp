<%-- 
    Document   : modalDetailsProfesseurs
    Created on : Nov 21, 2019, 3:49:06 PM
    Author     : Nicole
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<div class="modal fade right" id="modalDetailsProfesseurs" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header deep-purple darken-2 justify-content-between">
                <h1 class="white-text">Détails du professeur</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class='white-text'>&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="md-form">
                    <input value="default" type="text" name="nom" class="form-control" id="nom" disabled>
                    <label for="nom">Nom</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="prenom" class="form-control" id="prenom" disabled>
                    <label for="prenom">Prénom</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="email" class="form-control" id="email" disabled>
                    <label for="email">Courriel</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="tel" class="form-control" id="tel" disabled>
                    <label for="tel">Téléphone</label>
                </div>
                
                <div class="md-form">
                    <input value="default" type="text" name="ville" class="form-control" id="ville" disabled>
                    <label for="ville">Ville</label>
                </div>
                
                <div class="md-form">
                    <input value="default" type="text" name="anglaisEcrit" class="form-control" id="anglaisEcrit" disabled>
                    <label for="anglaisEcrit">Anglais Écrit</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="anglaisParle" class="form-control" id="anglaisParle" disabled>
                    <label for="anglaisParle">Anglais Parlé</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="moyenTransport" class="form-control" id="moyenTransport" disabled>
                    <label for="moyenTransport">Moyen de Transport</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="infoPartagees" class="form-control" id="infoPartagees" disabled>
                    <label for="infoPartagees">Information partagées</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="preferenceSupervision" class="form-control" id="preferenceSupervision" disabled>
                    <label for="preferenceSupervision">Préférence de Supervision</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="dispoStageEte" class="form-control" id="dispoStageEte" disabled>
                    <label for="dispoStageEte">disponible pour les stages en été</label>
                </div>
                               
            </div>
        </div>
    </div>
</div>
