<%-- 
    Document   : modalDetailsEmployeurs
    Created on : Nov 18, 2019, 5:33:27 PM
    Author     : guill
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<!--modalDetailsEmployeurs-->
<div class="modal fade right" id="modalDetailsEmployeurs" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header deep-purple darken-2 justify-content-between">
                <h1 class="white-text">Détails de l'employeur</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class='white-text'>&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="md-form">
                    <input value="default" type="text" name="nom" class="form-control" id="nom" disabled>
                    <label for="nom">Nom</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="prenom" class="form-control" id="prenom" disabled>
                    <label for="prenom">Prénom</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="email" class="form-control" id="email" disabled>
                    <label for="email">Courriel</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="tel" class="form-control" id="tel" disabled>
                    <label for="tel">Téléphone</label>
                </div>
                
                <div class="md-form">
                    <input value="default" type="text" name="fonction" class="form-control" id="fonction" disabled>
                    <label for="fonction">Fonction</label>
                </div>
                
                <div class="md-form">
                    <input value="default" type="text" name="nomEntreprise" class="form-control" id="nomEntreprise" disabled>
                    <label for="nomEntreprise">Nom de l'entreprise</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="secteur" class="form-control" id="secteur" disabled>
                    <label for="secteur">Secteur</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="pageWeb" class="form-control" id="pageWeb" disabled>
                    <label for="pageWeb">Site Web</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="telEntreprise" class="form-control" id="telEntreprise" disabled>
                    <label for="telEntreprise">Téléphone de l'entreprise</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="lienGoogleMaps" class="form-control" id="lienGoogleMaps" disabled>
                    <label for="lienGoogleMaps">Localisation (Google Maps)</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="taille" class="form-control" id="taille" disabled>
                    <label for="taille">Taille</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="kilometrage" class="form-control" id="kilometrage" disabled>
                    <label for="kilometrage">Kilometrage</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="nbEmployes" class="form-control" id="nbEmployes" disabled>
                    <label for="nbEmployes">Nombre d'employés</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="adresseLigne1" class="form-control" id="adresseLigne1" disabled>
                    <label for="adresseLigne1">Adresse ligne 1</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="adresseLigne2" class="form-control" id="adresseLigne2" disabled>
                    <label for="adresseLigne2">Adresse ligne 2</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="ville" class="form-control" id="ville" disabled>
                    <label for="ville">Ville</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="codePostal" class="form-control" id="codePostal" disabled>
                    <label for="codePostal">Code postal</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="province" class="form-control" id="province" disabled>
                    <label for="province">Province</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="pays" class="form-control" id="pays" disabled>
                    <label for="pays">Pays</label>
                </div>

                
            </div>
        </div>
    </div>
</div>
<!--modalDetailsEmployeurs-->
