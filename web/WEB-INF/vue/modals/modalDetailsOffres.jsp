<%-- 
    Document   : modalDetailsOffres
    Created on : Nov 18, 2019, 6:51:29 PM
    Author     : guill
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoStage" scope="page" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="daoStage" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="listeCriteres" value="${daoCritere.findAll()}"/>

<!DOCTYPE html>

<!--modalDetailsOffres-->

<div class="modal fade right" id="modalDetailsOffres" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-full-height ${sessionScope.role == 'professeur' ? 'modal-left' : 'modal-right'}" modal-notify modal-info role="document">
        <div class="modal-content">
            <div class="modal-header deep-purple darken-2 justify-content-between">
                <div class="container-fluid">
                    <div class="row">
                    <h1 class="white-text">Détails de l'offre de stage</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class='white-text'>&times;</span>
                    </button>
                    </div>
                </div>
            </div>
            <c:if test="${sessionScope.role != 'employeur'}">
                <c:set var="disabled" value="disabled"></c:set>
            </c:if>
            <c:if test="${sessionScope.role == 'employeur'}">
                <form action="*.do?tache=modifierOffreDeStage" method="post" enctype="multipart/form-data">
            </c:if>
            <div class="modal-body">
                
                <!--Informations sur l'offre de stage-->
                <div id="criteresStageContent">

                </div>
                <div class="md-form">
                    <input value="default" type="text" name="titre" class="form-control" id="titre" ${disabled}>
                    <label for="titre">Titre</label>
                </div>
                <div class="md-form">
                    <textarea name="desc" class="md-textarea form-control" id="description" ${disabled}>default</textarea>
                    <label for="description">Description</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="lienWeb" class="form-control" id="lienWeb" ${disabled}>
                    <label for="lienWeb">Lien web</label>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <div class="md-form">
                            <input type="date" name="date_debut" class="form-control" id="dateDebut" ${disabled}>
                            <label for="dateDebut">Date de début</label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="md-form">
                            <input type="date" name="date_fin" class="form-control" id="dateFin" ${disabled}>
                            <label for="dateFin">Date de fin</label>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="md-form">
                            <input value="default" type="text" name="remuneration" class="form-control" id="remuneration" ${disabled}>
                            <label for="remuneration">Remunéré</label>
                        </div>
                    </div>
                </div>
                <div class="custom-file py-2">
                    <label for="document">Pièce jointe</label>
                    <input value="default" type="file" name="fichier" class="form-control-file" id="fichier" ${disabled}>
                </div>
                <div class="row py-2">
                    <div class="col-12">
                        <label>Critères</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <c:set var="ligneDebut" value="<tr>" />
                        <c:set var="ligneFin" value="</tr>" />
                        <c:forEach items="${listeCriteres}" var="critere">            
                            <div id='listeCriteres' class="custom-control custom-checkbox custom-control-inline pr-5">
                                <input name="${critere.idCritere}" class="custom-control-input mr-auto critere" type="checkbox" id="${critere.idCritere}" ${disabled}>
                                <label class="custom-control-label" for="${critere.idCritere}">${critere.nom}</label>
                            </div>
                        </c:forEach>
                    </div>
                </div>

                <c:if test="${sessionScope.role != 'employeur'}">
                    <!--Informations sur la compagnie-->
                    <div class="row">
                        <div class='col-lg-4 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="nom" class="form-control" id="nom" disabled>
                                <label for="nom">Nom</label>
                            </div>
                        </div>
                        <div class="col-lg-4 col-12">
                            <div class="md-form">
                                <input value="default" type="text" name="prenom" class="form-control" id="prenom" disabled>
                                <label for="prenom">Prénom</label>
                            </div>
                        </div>
                        <div class='col-lg-4 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="fonction" class="form-control" id="fonction" disabled>
                                <label for="fonction">Fonction</label>
                            </div>
                        </div>
                    </div>

                    <div class='row'>
                        <div class='col-lg-6 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="email" class="form-control" id="email" disabled>
                                <label for="email">Courriel</label>
                            </div>
                        </div>
                        <div class='col-lg-6 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="tel" class="form-control" id="tel" disabled>
                                <label for="tel">Téléphone</label>
                            </div>
                        </div>
                    </div>

                    <div class="md-form">
                        <input value="default" type="text" name="nomEntreprise" class="form-control" id="nomEntreprise" disabled>
                        <label for="nomEntreprise">Nom de l'entreprise</label>
                    </div>

                    <div class='row'>
                        <div class='col-lg-6 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="secteur" class="form-control" id="secteur" disabled>
                                <label for="secteur">Secteur</label>
                            </div>
                        </div>
                        <div class='col-lg-6 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="telEntreprise" class="form-control" id="telEntreprise" disabled>
                                <label for="telEntreprise">Téléphone de l'entreprise</label>
                            </div>
                        </div>
                    </div>

                    <div class="md-form">
                        <input value="default" type="text" name="pageWeb" class="form-control" id="pageWeb" disabled>
                        <label for="pageWeb">Site Web</label>
                    </div>

                    <div class="md-form">
                        <input value="default" type="text" name="lienGoogleMaps" class="form-control" id="lienGoogleMaps" disabled>
                        <label for="lienGoogleMaps">Localisation (Google Maps)</label>
                    </div>
                    <div class='row'>
                        <div class='col-lg-4 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="taille" class="form-control" id="taille" disabled>
                                <label for="taille">Taille</label>
                            </div>
                        </div>
                        <div class='col-lg-4 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="kilometrage" class="form-control" id="kilometrage" disabled>
                                <label for="kilometrage">Kilometrage</label>
                            </div>
                        </div>
                        <div class='col-lg-4 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="nbEmployes" class="form-control" id="nbEmployes" disabled>
                                <label for="nbEmployes">Nombre d'employés</label>
                            </div>
                        </div>
                    </div>

                    <div class="md-form">
                        <input value="default" type="text" name="adresseLigne1" class="form-control" id="adresseLigne1" disabled>
                        <label for="adresseLigne1">Adresse ligne 1</label>
                    </div>
                    <div class="md-form">
                        <input value="default" type="text" name="adresseLigne2" class="form-control" id="adresseLigne2" disabled>
                        <label for="adresseLigne2">Adresse ligne 2</label>
                    </div>
                    <div class='row'>
                        <div class='col-lg-3 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="ville" class="form-control" id="ville" disabled>
                                <label for="ville">Ville</label>
                            </div>
                        </div>
                        <div class='col-lg-3 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="codePostal" class="form-control" id="codePostal" disabled>
                                <label for="codePostal">Code postal</label>
                            </div>
                        </div>
                        <div class='col-lg-3 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="province" class="form-control" id="province" disabled>
                                <label for="province">Province</label>
                            </div>
                        </div>
                        <div class='col-lg-3 col-12'>
                            <div class="md-form">
                                <input value="default" type="text" name="pays" class="form-control" id="pays" disabled>
                                <label for="pays">Pays</label>
                            </div>
                        </div>
                    </div>
                </c:if>

            </div>

            <c:if test="${sessionScope.role == 'etudiant'}">
                <div class='modal-footer'>
                    <form>
                        <input type="hidden" id="tache" name="tache" value="effectuerPostulationAction">
                        <input type="hidden" id="idOffre" name="idOffre">
                        <input type="hidden" id="msgConfirmation" value="Voulez-vous vraiment postuler à cette offre ?">
<!--                        <button type="submit" class="btn btn-block my-btn-outline-primary">Postuler</button>-->
                        <button type="button" onclick="remplirModalConfirmation();" class="btn btn-block my-btn-outline-primary" data-toggle="modal" data-target="#modalConfirmation">Postuler</button>
                    </form>
                </div>
            </c:if>

            <c:if test="${sessionScope.role == 'coordonnateur'}">
                <div class='modal-footer'>
                    <form>
                        <input type="hidden" id="tache" name="tache" value="effectuerAcceptationAction">
                        <input  type="hidden" id="idOffre" name="idOffre">
                        <input type="hidden" id="msgConfirmation" value="Voulez-vous vraiment accepter cette offre de stage pour la rendre accessible aux étudiants ?">
                        <button type="button" onclick="remplirModalConfirmation();" id="btnAccepter" data-toggle='modal' data-target='#modalConfirmation' class="btn btn-block my-btn-outline-primary">Accepter</button>
                    </form>
                    <form>
                        <input type="hidden" id="tacheDesactiver" name="tache" value="effectuerDesactivationAction">
                        <input  type="hidden" id="idOffre" name="idOffre">
                        <input type="hidden" id="msgConfirmationDesactiver" value="Voulez-vous vraiment désactiver cette offre de stage pour la rendre inaccessible aux étudiants ?">
                        <button type="button" onclick="remplirModalConfirmationDesactiver();" id="btnDesactiver" data-toggle='modal' data-target='#modalConfirmation' class="btn btn-block my-btn-outline-primary">Désactiver</button>
                    </form>
                    
                </div>
            </c:if>
                        
            <c:if test="${sessionScope.role == 'employeur'}">
                
                    <div class="modal-footer justify-content-center mt-5">
                        <input type='hidden' id='tache' name='tache' value='modifierOffreDeStage'>
                        <input type="hidden" id="idOffre" name="idOffre">
                        <input type="hidden"  name="document"  class="form-control" id="document"  placeholder="Document">
                        <input type="hidden" id="msgConfirmation" value="Voulez-vous enregistrer les modifications ?">
<!--                        <button type="submit" name="tache" value="modifierOffreDeStage" class="btn my-btn-outline-primary mr-5">Modifier</button>-->
                        <button type="button" onclick='remplirModalConfirmationModifierOffre()' data-toggle='modal' data-target='#modalConfirmation' class="btn my-btn-outline-primary mr-5">Modifier</button>
                        <button class="btn my-btn-outline-secondary">Annuler</button>
                    </div>
                </form>
            </c:if>
                
                
        </div>
    </div>
</div>
        
<!--modalDetailsOffres-->
