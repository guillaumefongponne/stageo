<%-- 
    Document   : modalDetailCandidature
    Created on : Nov 18, 2019, 6:51:29 PM
    Author     : guill
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!--modalDetailCandidature-->
<div class="modal fade right" id="modalDetailsCandidature" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-full-height modal-right modal-notify modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header deep-purple darken-2 justify-content-between">
                <h1 class="white-text">Détails de la candidature</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class='white-text'>&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <!--Informations sur l'offre de stage-->
                <div id="criteresStageContent">

                </div>
                <!--Informations sur la compagnie-->
                <div class="row">
                    <div class='col-lg-4 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="nom" class="form-control" id="nom" disabled>
                            <label for="nom">Nom</label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-12">
                        <div class="md-form">
                            <input value="default" type="text" name="prenom" class="form-control" id="prenom" disabled>
                            <label for="prenom">Prénom</label>
                        </div>
                    </div>
                    <div class='col-lg-4 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="domaine" class="form-control" id="domaine" disabled>
                            <label for="domaine">Fonction</label>
                        </div>
                    </div>
                </div>

                <div class='row'>
                    <div class='col-lg-6 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="email" class="form-control" id="email" disabled>
                            <label for="email">Courriel</label>
                        </div>
                    </div>
                    <div class='col-lg-6 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="tel" class="form-control" id="tel" disabled>
                            <label for="tel">Téléphone</label>
                        </div>
                    </div>
                </div>
                <div class='row'>
                    <div class='col-lg-4 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="superviseur" class="form-control" id="superviseur" disabled>
                            <label for="superviseur">Superviseur</label>
                        </div>
                    </div>
                    <div class='col-lg-4 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="telSuperviseur" class="form-control" id="telSuperviseur" disabled>
                            <label for="telSuperviseur">Telephone</label>
                        </div>
                    </div>
                    <div class='col-lg-4 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="courrielSuperviseur" class="form-control" id="courrielSuperviseur" disabled>
                            <label for="courrielSuperviseur">Courriel</label>
                        </div>
                    </div>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="nomEntreprise" class="form-control" id="nomEntreprise" disabled>
                    <label for="nomEntreprise">Nom de l'entreprise</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="titreStage" class="form-control" id="titreStage" disabled>
                    <label for="titreStage">Titre du stage</label>
                </div>
                <div class='row'>
                    <div class='col-lg-6 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="personneRessource" class="form-control" id="personneRessource" disabled>
                            <label for="personneRessource">Personne ressource</label>
                        </div>
                    </div>
                    <div class='col-lg-6 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="telEntreprise" class="form-control" id="telEntreprise" disabled>
                            <label for="telEntreprise">Téléphone de l'entreprise</label>
                        </div>
                    </div>
                </div>

                <div class="md-form">
                    <input value="default" type="text" name="pageWeb" class="form-control" id="pageWeb" disabled>
                    <label for="pageWeb">Site Web</label>
                </div>

                <div class="md-form">
                    <input value="default" type="text" name="lienGoogleMaps" class="form-control" id="lienGoogleMaps" disabled>
                    <label for="lienGoogleMaps">Localisation (Google Maps)</label>
                </div>
                <div class='row'>
                    <div class='col-lg-6 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="dateDebut" class="form-control" id="dateDebut" disabled>
                            <label for="dateDebut">Date Début</label>
                        </div>
                    </div>
                    <div class='col-lg-6 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="dateFin" class="form-control" id="dateFin" disabled>
                            <label for="dateFin">Date Fin</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class='col-lg-6 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="kilometrage" class="form-control" id="kilometrage" disabled>
                            <label for="kilometrage">Kilometrage</label>
                        </div>
                    </div>
                    <div class='col-lg-4 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="remuneration" class="form-control" id="remuneration" disabled>
                            <label for="remuneration">Rémuneration</label>
                        </div>
                    </div>
                </div>

                <div class="md-form">
                    <input value="default" type="text" name="adresseLigne1" class="form-control" id="adresseLigne1" disabled>
                    <label for="adresseLigne1">Adresse ligne 1</label>
                </div>
                <div class="md-form">
                    <input value="default" type="text" name="adresseLigne2" class="form-control" id="adresseLigne2" disabled>
                    <label for="adresseLigne2">Adresse ligne 2</label>
                </div>
                <div class='row'>
                    <div class='col-lg-4 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="ville" class="form-control" id="ville" disabled>
                            <label for="ville">Ville</label>
                        </div>
                    </div>
                    <div class='col-lg-4 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="codePostal" class="form-control" id="codePostal" disabled>
                            <label for="codePostal">Code postal</label>
                        </div>
                    </div>
                    <div class='col-lg-4 col-12'>
                        <div class="md-form">
                            <input value="default" type="text" name="province" class="form-control" id="province" disabled>
                            <label for="province">Province</label>
                        </div>
                    </div>
                </div>
            </div>
            <c:if test="${sessionScope.role == 'coordonnateur'}">
                <div class='modal-footer'>
                    <button id="acceptOffre" data-toggle="modal" data-target="#modalListeProfesseur" class="btn my-btn-outline-primary">
                        Assigner un professeur
                    </button>
                    <form action="*.do?tache=genererConventionStage" method="post">
                        <input type="hidden" id="idOffre" name="idOffre">
                        <input type="hidden" id="idEtudiant" name="idEtudiant">
                        <button name="tache" value="genererConventionStage" class="btn my-btn-outline-primary">Générer la convention de stage</button>
                    </form>
                    
                </div>
            </c:if>

        </div>
    </div>
</div>
<!--modalDetailCandidature-->
