<%-- 
    Document   : modalConfirmationModify
    Created on : Dec 8, 2019, 5:15:30 PM
    Author     : guill
--%>

<!--Modal: modalConfirmationModify -->
<div class="modal fade" id="modalConfirmModify" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-sm modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content text-center">
      <!--Header-->
      <div class="modal-header d-flex justify-content-center">
        <p class="heading">�tes-vous s�r ?</p>
      </div>

      <!--Body-->
      <div class="modal-body">
        <i class="fas fa-edit fa-4x animated rotateIn"></i>
        <p class='text-center'>Modifier ?</p>
      </div>

      <!--Footer-->
      <div class="modal-footer flex-center">
        <a href="" class="btn btn-outline-info">Oui</a>
        <a type="button" class="btn btn-info waves-effect" data-dismiss="modal">Non</a>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!--Modal: modalConfirmationModify-->
