<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.Utilisateur"%>
<%@page import="com.stageo.dao.UtilisateurDAO"%>
<%@page import="com.stageo.dao.EtudiantDAO"%>
<%@page import="com.util.Util"%>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="candiDAO" class="com.stageo.dao.CandidatureDAO">
    <jsp:setProperty name="candiDAO" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="daoOffreStage" scope="page" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="daoOffreStage" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="etudao" class="com.stageo.dao.EtudiantDAO">
    <jsp:setProperty name="etudao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="listeUser" value="${dao.findAll()}"/>
<c:set var="listeEtudiant" value="${etudao.findAll()}"/>
<c:set var="listeOffres" value="${daoOffreStage.findByIdEmployeur(sessionScope.connecte)}"/>
<c:set var="listeCandidatures" value="${candiDAO.findByEmployeur(sessionScope.connecte)}" />

<jsp:include page="/WEB-INF/vue/modals/modalDetailsEtudiants.jsp"></jsp:include>

<div class="container" >
    <div class="row">
        <div class="col-12 py-5">
            <h3><strong>Candidatures reçues</strong></h3>
        </div>
        <!-- Barre de recherche -->
        <div class='col-lg-6 col-12'>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text"><i class="fas fa-search"></i></label>
                </div>
                <input id="inputRecherche" onkeyup = "rechercherCandidature()"type="text" class="form-control" placeholder="Rechercher une candidature">
            </div>
        </div>
        <!-- Fin de la barre de recherche -->         
        
    </div>

    <div class='row'>
        <div class='col-12'>
            <table id='table' class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th onclick="sortTable(0, 'table')">Nom</th>
                        <th onclick="sortTable(1, 'table')">Prénom</th>
                        <th onclick="sortTable(2, 'table')">Titre de l'offre</th>
                        <th>Détails</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listeCandidatures}" var="candidature">
                        <c:set var="etudiant" value="${etudao.read(candidature.idEtudiant)}"/>
                        <c:set var="offre" value="${daoOffreStage.read(candidature.idOffre)}"/>
                        <tr>
                            <td>${etudiant.nom}</td>
                            <td>${etudiant.prenom}</td>
                            <td hidden>${etudiant.email}</td>
                            <td hidden>${etudiant.telephone}</td>
                            <td>${offre.titre}</td>
                            <td hidden>${candidature.idEtudiant}</td>
                            <td hidden>${candidature.idOffre}</td>
                            <td hidden>${Util.toUTF8(etudiant.ville)}</td>
                            <td hidden>${Util.toUTF8(etudiant.profilEtudes)}</td>
                            <td hidden>${Util.toUTF8(etudiant.stageSession)}</td>
                            <td hidden>${Util.toUTF8(etudiant.anglaisEcrit)}</td>
                            <td hidden>${Util.toUTF8(etudiant.anglaisParle)}</td>
                            <td hidden>${Util.toUTF8(etudiant.permisConduire)}</td>
                            <td hidden>${Util.toUTF8(etudiant.demarcheEnCours)}</td>
                            <td hidden>${Util.toUTF8(etudiant.login365)}</td>
                            <td hidden>${Util.toUTF8(etudiant.numeroDA)}</td>
                            <td hidden>${Util.toUTF8(etudiant.moyenTransport)}</td>
                            <td hidden>${Util.toUTF8(etudiant.preferences)}</td>
                            
                            <td> 
                                <button type="button" class="btn red darken-2 white-text" data-toggle="modal" data-target="#modalDetailsEtudiants">
                                    <i class="fas fa-align-left"></i>
                                    <span>Afficher détails</span>
                                </button>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>                            
</div>
<jsp:include page="/WEB-INF/vue/modals/modalConfirmation.jsp"></jsp:include>



<!-- Fin footer -->
<script>
    var table = document.getElementById('table');

    for (var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function ()
        {
            //rIndex = this.rowIndex;
            document.getElementById("prenom").value = this.cells[0].innerHTML;
            document.getElementById("nom").value = this.cells[1].innerHTML;
            document.getElementById("email").value = this.cells[2].innerHTML;
            document.getElementById("tel").value = this.cells[3].innerHTML;
            document.getElementById("candidatureID").value = this.cells[5].innerHTML;
            document.getElementById("offreID").value = this.cells[6].innerHTML;
            document.getElementById("ville").value = this.cells[7].innerHTML;
            document.getElementById("profilEtudes").value = this.cells[8].innerHTML;
            document.getElementById("stageSession").value = this.cells[9].innerHTML;
            document.getElementById("anglaisEcrit").value = this.cells[10].innerHTML;
            document.getElementById("anglaisParle").value = this.cells[11].innerHTML;
            document.getElementById("permisConduire").value = this.cells[12].innerHTML;
            document.getElementById("demarcheEnCours").value = this.cells[13].innerHTML;
            document.getElementById("login365").value = this.cells[14].innerHTML;
            document.getElementById("numeroDA").value = this.cells[15].innerHTML;
            document.getElementById("moyenTransport").value = this.cells[16].innerHTML;
            document.getElementById("preferences").innerHTML = this.cells[17].innerHTML;
            
            
        };
    }
    
function rechercherCandidature() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("inputRecherche");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");

    for (i = 1; i < tr.length; i++) {
        var td0 = tr[i].getElementsByTagName("td")[0].textContent.toUpperCase();
        var td1 = tr[i].getElementsByTagName("td")[1].textContent.toUpperCase();
        var td4 = tr[i].getElementsByTagName("td")[4].textContent.toUpperCase();
        if (td0.indexOf(filter) > -1 || td1.indexOf(filter) > -1 || td4.indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }      
    }
}

</script>
