<%-- 
    Document   : liste-entreprises-administrateur
    Created on : 2019-11-07, 15:39:15
    Author     : soleil123
--%>

<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.*"%>
<%@page import="com.util.Util"%>


<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="udao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="udao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoCompagnie" scope="page" class="com.stageo.dao.CompagnieDAO">
    <jsp:setProperty name="daoCompagnie" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoOffreStage" scope="page" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="daoOffreStage" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoAdresse" scope="page" class="com.stageo.dao.AdresseDAO">
    <jsp:setProperty name="daoAdresse" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="daoEmployeur" scope="page" class="com.stageo.dao.EmployeurDAO">
    <jsp:setProperty name="daoEmployeur" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="listeCompagniesActives" value="${udao.findByEtatCompteActif()}"/>
<c:set var="listeOffres" value="${daoOffreStage.findByIdEmployeur(sessionScope.connecte)}"/>

<c:set var="listeCompagniesInactives" value="${udao.findByEtatCompteInactif()}"/>

<jsp:include page="/WEB-INF/vue/modals/modalDetailsEmployeurs.jsp"></jsp:include>

<section class="container my-2">
    <article class="row py-5">
        <div class="col-lg-6 col-12">
            <h1>Employeurs Actifs</h1>
        </div>
        
        <!-- Barre de recherche -->
        <div class="col-lg-6 col-12">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text"><i class="fas fa-search"></i></label>
                </div>
                <input id="inputRecherche" onkeyup ="rechercherEntrepriseActif()"type="text" class="form-control" placeholder="Recherche par mot-clef...">
            </div>
        </div>
        <!-- Fin de la barre de recherche -->
    </article>
    
    <article class="row">
        <div class="col-12">
            <table id="table" class="table table-hover">
                <thead>
                    <tr>
                        <th onclick="sortTable(0, 'table')">Nom</th>
                        <th onclick="sortTable(1, 'table')">Prénom</th>
                        <th onclick="sortTable(2, 'table')">Courriel</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listeCompagniesActives}" var="compagnie">
                            <tr>
                                <td>${compagnie.nom}</td>
                                <td>${compagnie.prenom}</td>
                                <td>${compagnie.email}</td>
                                <td>
                                    <form action="*.do?tache=desactiverCompteEmployeur" method="post">
                                        <button type="submit" class="btn btn-danger" name="tache" value="desactiverCompteEmployeur">Désactiver</button>
                                        <input class="form-control" type="hidden" name="idEmployeurInput" value="${compagnie.idUtilisateur}">
                                    </form>
                                </td>
                                
                                <td hidden>${Util.toUTF8(compagnie.telephone)}</td>
                                <td hidden>${Util.toUTF8((daoEmployeur.read(compagnie.idUtilisateur)).fonction)}</td>
                                
                                <td hidden>${Util.toUTF8((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).nom)}</td>
                                <td hidden>${Util.toUTF8((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).secteur)}</td>
                                <td hidden>${Util.toUTF8((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).pageWeb)}</td>
                                <td hidden>${Util.toUTF8((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).telEntreprise)}</td>
                                <td hidden>${Util.toUTF8((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).lienGoogleMaps)}</td>
                                <td hidden>${Util.toUTF8((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).taille)}</td>
                                <td hidden>${Util.toUTF8((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).kilometrage)}</td>
                                <td hidden>${Util.toUTF8((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).nbEmployes)}</td>
                                
                                <td hidden>${Util.toUTF8((daoAdresse.read((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).adresse.idAdresse)).adresseLigne1)}</td>
                                <td hidden>${Util.toUTF8((daoAdresse.read((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).adresse.idAdresse)).adresseLigne2)}</td>
                                <td hidden>${Util.toUTF8((daoAdresse.read((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).adresse.idAdresse)).ville)}</td>
                                <td hidden>${Util.toUTF8((daoAdresse.read((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).adresse.idAdresse)).codePostal)}</td>
                                <td hidden>${Util.toUTF8((daoAdresse.read((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).adresse.idAdresse)).province)}</td>
                                <td hidden>${Util.toUTF8((daoAdresse.read((daoCompagnie.read((daoEmployeur.read(compagnie.idUtilisateur)).compagnie.idCompagnie)).adresse.idAdresse)).pays)}</td>
                                
                            </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </article>
</section>

<section class="container my-2">
    <article class="row py-5">
        <div class="col-lg-6 col-12">
            <h1>Employeurs Inactifs</h1>
        </div>
        
        <!-- Barre de recherche -->
        <div class="col-lg-6 col-12">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text"><i class="fas fa-search"></i></label>
                </div>
                <input id="inputRechercheInactif" onkeyup="rechercherEntrepriseInactif()" type="text" class="form-control" placeholder="Recherche par mot-clef...">
            </div>
        </div>
        <!-- Fin de la barre de recherche -->
    </article>
    
    <article class="row">
        <div class="col-12">
            <table id="tableinactif" class="table table-hover">
                <thead>
                    <tr>
                        <th onclick="sortTable(0, 'tableinactif')">Nom</th>
                        <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listeCompagniesInactives}" var="compagnie">
                        <tr>
                            <td>${compagnie.nom}</td>
                            <td>
                                <form action="*.do?tache=activerCompteEmployeur" method="post">
                                    <button type="submit" class="btn btn-success" name="tache" value="activerCompteEmployeur">Activer</button>
                                    <input class="form-control" type="hidden" name="idEmployeurInput" value="${compagnie.idUtilisateur}">
                                </form>
                            </td> 
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </article>
</section>

<script>
    var nbComp = 0;

    var table = document.getElementById('table');

    for (var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function ()
        {
            
            //rIndex = this.rowIndex;
            document.getElementById("prenom").value = this.cells[1].innerHTML;
            document.getElementById("nom").value = this.cells[0].innerHTML;
            document.getElementById("email").value = this.cells[2].innerHTML;
            document.getElementById("tel").value = this.cells[4].innerHTML;
            document.getElementById("fonction").value = this.cells[5].innerHTML; 
            document.getElementById("nomEntreprise").value = this.cells[6].innerHTML; 
            document.getElementById("secteur").value = this.cells[7].innerHTML; 
            document.getElementById("pageWeb").value = this.cells[8].innerHTML; 
            document.getElementById("telEntreprise").value = this.cells[9].innerHTML; 
            document.getElementById("lienGoogleMaps").value = this.cells[10].innerHTML; 
            document.getElementById("taille").value = this.cells[11].innerHTML; 
            document.getElementById("kilometrage").value = this.cells[12].innerHTML; 
            document.getElementById("nbEmployes").value = this.cells[13].innerHTML; 
            document.getElementById("adresseLigne1").value = this.cells[14].innerHTML; 
            document.getElementById("adresseLigne2").value = this.cells[15].innerHTML;
            document.getElementById("ville").value = this.cells[16].innerHTML;
            document.getElementById("codePostal").value = this.cells[17].innerHTML;
            document.getElementById("province").value = this.cells[18].innerHTML;
            document.getElementById("pays").value = this.cells[19].innerHTML;
//            $('#' + this.cells[4].innerHTML).children("td[name='crit']").each(function (index) {
//                document.getElementById("criteresStageContent").innerHTML += "<h2 class='Catpill'><span class='badge badge-pill mb-1'>" + $(this).text() + "</span></h2>";
//            });
            $(".Catpill").addClass("blue")
                    .css({"display": "inline"});
            
            $('#modalDetailsEmployeurs').modal('show');
        };
    }
    
 function rechercherEntrepriseActif() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("inputRecherche");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");

    for (i = 1; i < tr.length; i++) {
        var td0 = tr[i].getElementsByTagName("td")[0].textContent.toUpperCase();
        var td1 = tr[i].getElementsByTagName("td")[1].textContent.toUpperCase();
        var td2 = tr[i].getElementsByTagName("td")[2].textContent.toUpperCase();
        if (td0.indexOf(filter) > -1 || td1.indexOf(filter) > -1 || td2.indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }      
    }
    }
    
 function rechercherEntrepriseInactif() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("inputRechercheInactif");
    filter = input.value.toUpperCase();
    table = document.getElementById("tableinactif");
    tr = table.getElementsByTagName("tr");

    for (i = 1; i < tr.length; i++) {
        var td0 = tr[i].getElementsByTagName("td")[0].textContent.toUpperCase();
        var td1 = tr[i].getElementsByTagName("td")[1].textContent.toUpperCase();
        if (td0.indexOf(filter) > -1 || td1.indexOf(filter) > -1 ) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }      
    }
 }    
    

</script>

<style>
    th{
        cursor:pointer;
    }
</style>


        <!-- Fin de la section de recherche des stages -->
        
<!-- Fin footer -->
