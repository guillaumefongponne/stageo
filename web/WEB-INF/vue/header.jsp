<%-- 
    Document   : header
    Created on : Nov 17, 2019, 11:51:30 AM
    Author     : guill
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:useBean id="userDAO" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="userDAO" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="assignDAO" class="com.stageo.dao.AssignationDAO">
    <jsp:setProperty name="assignDAO" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="etuDAO" class="com.stageo.dao.EtudiantDAO">
    <jsp:setProperty name="etuDAO" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="profDAO" class="com.stageo.dao.ProfesseurDAO">
    <jsp:setProperty name="profDAO" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="coDAO" class="com.stageo.dao.CoordonnateurDAO">
    <jsp:setProperty name="coDAO" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="empDAO" class="com.stageo.dao.EmployeurDAO">
    <jsp:setProperty name="empDAO" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="trouve" value="TROUVE"/>
<c:set var="listeNewAssign" value="${etuDAO.findStageTrouve(trouve)}"/>

<c:if test="${!empty sessionScope.connecte}">
    <c:set var="user" value="${userDAO.read(sessionScope.connecte)}"></c:set>
</c:if>

<c:set var="etudiant" scope="page" value="${etudao.read(sessionScope.connecte)}"/>

<c:set var="nbrAcceptation" value="0"> </c:set>
<c:forEach items="${etudiant.liste_candidature}" var="items" >
    <c:if test="${!items.getStatut().equals('En attente')}">
        <c:set var="nbrAcceptation" value="${nbrAcceptation + 1}"></c:set>
    </c:if>
</c:forEach>
<!DOCTYPE html>

<div class="overlay"></div>
<!-- Header Navbar -->
<section class='fixed-top'>
    <nav class="navbar navbar-expand-lg navbar-dark deep-purple darken-2" role="navigation" id="menu">
    <a class="navbar-brand mb-3 d-none d-md-block" href="?tache=afficherPageAccueil">
        <img  class="d-inline-block align-top" id="logo" src="./images/logoCegep.png"/>
    </a>
    <a class="navbar-brand mb-3 d-md-none" href="?tache=afficherPageAccueil">
        <img  class="d-inline-block align-top" id="logo" src="./images/logoCouper.png"/>
    </a>

    <c:choose>
        <c:when test="${!empty sessionScope.connecte}">
            <c:choose>
                <c:when test="${user.role == 'etudiant'}">
                    <c:set var="profilUser" value="${(etuDAO.read(user.idUtilisateur)).profilModifie}"></c:set>
                </c:when>
                <c:when test="${user.role == 'professeur' or user.role == 'coordonnateur'}">
                    <c:set var="profilUser" value="${(profDAO.read(user.idUtilisateur)).profilModifie}"></c:set>
                </c:when>
                <c:when test="${user.role == 'employeur'}">
                    <c:set var="profilUser" value="${(empDAO.read(user.idUtilisateur)).profilModifie}"></c:set>
                </c:when>
            </c:choose>
            
            <div class="collapse navbar-collapse" id="navCollapse">
                <ul class="navbar-nav">
                    <c:if test="${!empty profilUser or user.role == 'administrateur'}">
                        <c:if test="${sessionScope.role.equals('etudiant')}">
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageCandidaturesEtudiant">Candidatures</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageListeStage">Offres de stage</a>
                            </li>
                        </c:if>

                        <c:if test="${!sessionScope.role.equals('etudiant') and !sessionScope.role.equals('employeur')}">
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageListeStagiaires">Liste des étudiants</a>
                            </li>
                        </c:if>
                        <c:if test="${sessionScope.role.equals('employeur')}">
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageCandidaturesEmployeur">Candidatures reçues</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageCreationStage">Demande d'ajout de stage</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageListeStage">Mes stages</a>
                            </li>
                        </c:if>
                        <c:if test="${sessionScope.role.equals('coordonnateur')}">
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageListeEntreprises">Liste des entreprises</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageListeStage">Liste des stages</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageListeProfesseurs">Liste des professeurs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageListeAssignation">Assignations
                                    <c:if test="${fn:length(listeNewAssign) gt 0}">
                                        <span class="badge">${fn:length(listeNewAssign)}</span>
                                    </c:if>
                                </a>
                            </li>

                        </c:if> 
                        <c:if test="${sessionScope.role.equals('professeur')}">                                
                            <li class="nav-item">
                                <a class="nav-link" href="">Contacter un étudiant</a>
                            </li>
                        </c:if>                             

                        <c:if test="${sessionScope.role.equals('administrateur')}"> 
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageListeEntreprises">Liste des entreprises</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageListeStage">Liste des stages</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageListeProfesseurs">Liste des professeurs</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageInscription">Créer un compte...</a>
                            </li>
                        </c:if>
                    </c:if>
                </ul>
            </div>
            

            <ul class="navbar-nav ml-auto ml-lg-0">
                <div class="dropdown">
                    <a id="dropdownUser" class='nav-link dropdown-toggle' data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ${user.nom}
                        <i class="fas fa-user px-2 mr-4"></i>
                    </a>
                    <div class='dropdown-menu dropdown-menu-right' aria-labelledby="dropdownUser">
                        <button class='dropdown-item' disabled>
                            <h6>@${user.nomUtilisateur}</h6>
                            <h6>${fn:toUpperCase(user.role)}</h6>
                        </button>
                        <div class="dropdown-divider"></div>
                        <a class='dropdown-item' href="*.do?tache=afficherPageProfil">Profil</a>
                        <a class='dropdown-item' href="*.do?tache=afficherPageDocument">Documents</a>
                        <div class="dropdown-divider"></div>
                        <a class='dropdown-item' href="*.do?tache=effectuerDeconnexion">Déconnexion</a>
                    </div>
                </div>
            </ul>

            <button type="button" class="navbar-toggler first-button ml-lg-auto ml-0 d-lg-none d-block" data-toggle="collapse" data-target="#navCollapse"
                    aria-controls="navCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <div class="animated-icon2"><span></span><span></span><span></span><span></span></div>
            </button>

        </c:when>
        <c:otherwise>
            <div class="miniConnexion ml-auto">
                <form id="formConnection" class="form-inline my-2 my-lg-0 ml-auto d-none d-md-block" action="*.do?tache=effectuerConnexion" method = "post" >
                    <input type="text" class="form-control mr-sm-2" name="nomUtilisateurConnexion" placeholder="Nom d'utilisateur" required>
                    <input type="password" class= "form-control mr-sm-2"  name="motDePasseConnexion" placeholder="Mot de passe" required>
                    <button type="submit" name="tache" value="effectuerConnexion" class="btn my-btn-outline-primary my-2 my-sm-0">Se connecter</button>
                </form>
            </div>
        </c:otherwise>
    </c:choose>
</nav>
    <c:if test="${!empty sessionScope.connecte}">
        <c:if test="${empty profilUser and user.role != 'administrateur'}">
            <section id='alert-profil' class="alert alert-info m-0" role="alert">
                <h4 class="alert-heading">Profil incomplet</h4>
                <p>
                    Pour avoir accès aux fonctionnalités du site, votre profil doit être complet.
                    Ainsi, veuillez compléter votre profil avant de continuer.
                </p>
            </section>
        </c:if>
    </c:if>
    
</section>

