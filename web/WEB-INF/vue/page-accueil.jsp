<!DOCTYPE html>
    
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="Accueil">
    <div class="container pt-5">
        <div class="row card-deck">
            <div class="col-md-6 col-12 py-5 white-text">
                <h3 class="font-weight-bold">Plateforme de gestion des stages du Collège de Rosemont</h3>
                <h4>Cette plateforme vous permettra de : </h4>
                <h6>Chercher des stages</h6>
                <h6>Postuler</h6>
            </div>
            
            <div class='col-md-6 col-12'>
                <section class="form-elegant">
                    <form id="formConnection" class="my-2 my-lg-0 ml-auto" action="*.do?tache=effectuerConnexion" method="post">
                        <div class="card">
                            <div class="card-body mx-4">
                                <!--Header-->
                                <div class="text-center">
                                    <h3 class="dark-grey-text mb-5"><strong>Connexion</strong></h3>
                                </div>

                                <!--Body-->
                                <div class="md-form">
                                    <input type="text" class="form-control mr-sm-2" name="nomUtilisateurConnexion" id='nomUtilisateurConnexion' required>
                                    <label for="nomUtilisateurConnexion">Nom d'utilisateur</label>
                                </div>

                                <div class="md-form pb-3">
                                    <input type="password" class= "form-control mr-sm-2" name="motDePasseConnexion" id='motDePasseConnexion' required>
                                    <label for="motDePasseConnexion">Mot de passe</label>
                                </div>

                                <div class="text-center mb-3">
                                    <button type="submit" name="tache" value="effectuerConnexion" class="btn my-btn-outline-primary my-2 my-sm-0">Se connecter</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </section>
            </div>
        </div>
    </div>
</div>

<div class="container py-5">
    <article class='row py-5'>
        <div class='col-12 text-center'>
            <h3>Une plateforme pour tous les types d'utilisateurs</h3>
        </div>
    </article>
    
    <article class='row py-2'>
        <div class="col-lg-6 col-12">
            <h5 class="font-weight-bold">Étudiant</h5>
            <p>
                Chercher et trouver des stages
                <br />
                Postuler sur les stages
            </p>
            
            <h5 class="font-weight-bold">Entreprise/Employeur</h5>
            <p>
                Ajouter des stages dans la plateforme avec l'autorisation du coordonnateur
                <br />
                Consulter les stages déposés
                <br />
                Choisir des étudiants ayant postulés
            </p>
            
            <h5 class="font-weight-bold">Professeur</h5>
            <p>
                Consulter les informations de l'étudiant pris en charge
            </p>
            
            <h5 class="font-weight-bold">Coordonnateur</h5>
            <p>
                Consulter toutes les listes pour la gestion (étudiants, professeurs, employeurs, offres de stage, candidatures)
                <br />
                Assigner des professeurs pour superviser les étudiants en stage
                <br />
                Générer la convention de stage
            </p>
        </div>
        
        <div class='col-lg-6 col-12 text-center wow jackInTheBox' data-wow-iteration='5'>
            <img src='images/logoCouper.png' class="img-fluid" width="350px">
        </div>
    </article>
    
    <article class='row py-5'>
        <div class='col-12 text-center'>
            <h3>Pour un étudiant</h3>
        </div>
    </article>
    
    <article class='row justify-content-around py-5'>
        <section class="card-deck">
            <article class="card mb-4 wow rotateInUpLeft">
                <div class="card-header elegant-color white-text text-center">
                    <h4>Chercher et trouver des stages</h4>
                </div>
                <div class="card-body">
                    <p>
                        La plateforme permet de chercher des stages. Les stages présents
                        sont des stages proposés par des employeurs qui ont contacté le
                        collège.
                    </p>
                </div>
            </article>
            
            <article class="card mb-4 wow rotateInUpRight">
                <div class="card-header elegant-color white-text text-center">
                    <h4>Postuler</h4>
                </div>
                <div class="card-body">
                    <p>
                        Le site offre la possibilité de postuler directement. Il suffit 
                        tout simplement de trouver les stages qui vous interessent et de
                        postuler. Préparez vos documents (CV, lettre de motivation).
                    </p>
                </div>
            </article>
        </section>
    </article>
    
    <article class='row py-5'>
        <div class='col-12 text-center'>
            <h3>Pour un employeur</h3>
        </div>
    </article>
    
    <article class='row justify-content-around py-5'>
        <section class="card-deck">
            <article class="card mb-4 wow slideInLeft">
                <div class="card-header elegant-color white-text text-center">
                    <h4>Ajouter des stages</h4>
                </div>
                <div class="card-body">
                    <p>
                        Le site offre la possibilité d'ajouter un stage en lui donnant
                        des informations concernant le stage ajouté. Pour que les étudiants 
                        puissent envoyer leur candidature, le coordonnateur doit approuver votre
                        demande d'ajout de stage.
                    </p>
                </div>
            </article>
            
            <article class="card mb-4 wow slideInUp">
                <div class="card-header elegant-color white-text text-center">
                    <h4>Consulter les stages</h4>
                </div>
                <div class="card-body">
                    <p>
                        Vous avez la possibilité de consulter les stages que vous avez ajoutés.
                    </p>
                </div>
            </article>
            
            <article class="card mb-4 wow slideInRight">
                <div class="card-header elegant-color white-text text-center">
                    <h4>Choisir des étudiants ayant postulés</h4>
                </div>
                <div class="card-body">
                    <p>
                        Choisir parmi les étudiants qui ont postulés pour leur donner une chance
                        d'avoir un stage dans l'entreprise.
                    </p>
                </div>
            </article>
        </section>
    </article>
    
    <article class='row py-5'>
        <div class='col-12 text-center'>
            <h3>Pour un professeur</h3>
        </div>
    </article>
    
    <article class='row justify-content-around'>
        <section class="card-deck">
            <article class="card mb-4 wow zoomIn">
                <div class="card-header elegant-color white-text text-center">
                    <h4>Consulter les informations de l'étudiant pris en charge</h4>
                </div>
                <div class="card-body">
                    <p>
                        L'enseignant pourra consulter les informations des étudiants
                        qu'il supervise. Cela lui permettra de le contacter pour les
                        rapports de chaque semaine. Il pourra aussi lui rendre visite
                        pour superviser lors de la mi-session.
                    </p>
                </div>
            </article>
        </section>
    </article>
    
    <article class='row py-5'>
        <div class='col-12 text-center'>
            <h3>Pour un coordonnateur</h3>
        </div>
    </article>
    
    <article class='row justify-content-around'>
        <section class="card-deck">
            <article class="card mb-4 wow rotateInUpLeft">
                <div class="card-header elegant-color white-text text-center">
                    <h4>Consulter toutes les listes pour la gestion</h4>
                </div>
                <div class="card-body">
                    <p>
                        Le coordonnateur a accès à toutes les listes que ce soit la liste
                        des étudiants, la liste des employeurs, la liste des professeurs
                        et la liste des offres de stage pour faire en faire la gestion.
                    </p>
                </div>
            </article>
            
            <article class="card mb-4 wow zoomIn">
                <div class="card-header elegant-color white-text text-center">
                    <h4>Assigner des professeurs</h4>
                </div>
                <div class="card-body">
                    <p>
                        Lorsque l'étudiant confirme faire son stage dans une entreprise,
                        le coordonnateur peut dès ce moment assigner un professeur pour
                        qu'il supervise et prenne en charge l'étudiant en question.
                    </p>
                </div>
            </article>
            
            <article class="card mb-4 wow rotateInUpRight">
                <div class="card-header elegant-color white-text text-center">
                    <h4>Générer la convention de stage</h4>
                </div>
                <div class="card-body">
                    <p>
                        La convention de stage est un moyen pour le collège, l'entreprise
                        et l'étudiant de concevoir un contrat écrit qui permettra de 
                        mettre les termes et les règles à suivre durant le stage de 
                        l'étudiant.
                    </p>
                </div>
            </article>
        </section>
    </article>
    
    <article class='row py-5'>
        <div class='col-12 text-center'>
            <h3>Service disponible pour le département des TI</h3>
        </div>
    </article>
    
    <div class="row" >
        <div class="col-lg-6 col-12">
            <div class="card">
                <div class="card-header titreProfil2">
                    <h1 class="card-title mt-3">Informatique de gestion</h1>
                </div>
                <div class="card-body">
                    <p>Pour les programmeurs-analyste, que ce soit dans le domaine pour développer des applications Web ou mobiles, la variété de stages est très grande.</p>
                </div>
            </div>
        </div>
        
        <div class="col-lg-6 col-12 py-5 py-lg-0">
            <div class="card">
                <div class="card-header titreProfil2">
                    <h1 class="card-title mt-3">Gestion de réseaux</h1>
                </div>
                <div class="card-body">
                    <p>Pour les gestionnaires en réseaux, la maintenance et la surveillance des réseaux informatiques est en demande élevée parmi les grandes entreprises.</p>
                </div>
            </div>
        </div>          
    </div>
</div>
