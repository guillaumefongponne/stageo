<%-- 
    Document   : profil-professeur.jsp
    Created on : 2019-10-10, 15:25:18
    Author     : soleil123
--%>

<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.Utilisateur"%>
<%@page import="com.stageo.dao.UtilisateurDAO"%>
<%@page import="com.stageo.model.Critere"%>
<%@page import="com.stageo.dao.CritereDAO"%>
<%@page import="com.stageo.model.ListeChoix"%>
<%@page import="com.util.Util"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>

<jsp:useBean id="daoProfesseur" class="com.stageo.dao.ProfesseurDAO">
    <jsp:setProperty name="daoProfesseur" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="professeur" scope="page" value="${daoProfesseur.read(sessionScope.connecte)}"/>

<jsp:include page="modals/modal-modif-compte.jsp"></jsp:include>

<div class="jumbotron jumbotron-fluid stylish-color white-text">
    <section class="container">
        <article class="row justify-content-center">
            <div class="col-lg-6 col-12 text-center">
                <h1><i class="fas fa-user-circle" style="font-size:150px"></i></h1>
                <h3>${Util.toUTF8(professeur.prenom)} ${Util.toUTF8(professeur.nom)}</h3>
                <h4>@${professeur.nomUtilisateur}</h4>
                <h5>${professeur.email}</h5>
                <h5>${fn:toUpperCase(professeur.role)}</h5>
                <p>
                    Membre depuis : ${professeur.profilCree}
                    <br>
                    Dernière modification : ${professeur.profilModifie}
                </p>
                <a data-toggle="modal" href="#monModalMotDePasse" class="btn btn-success btn-sm" role="button">Changer mon mot de passe</a>
                <a data-toggle="modal" href="#monModalNomUtilisateur" class="btn btn-success btn-sm" role="button">Changer mon nom d'utilisateur</a>
            </div>
        </article>
    </section>
</div>

<section class="container">
    <form action="*.do" method="post" class="needs-validation" novalidate>
        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                <h1><strong>Informations générales</strong></h1>
            </div>

            <div class="col-12 col-lg-6">
                <section class="container-fluid p-0">
                    <article class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Nom</label>
                                <input required type="text" class="form-control" name="nom" value="${Util.toUTF8(professeur.nom)}">
                                <div class="invalid-feedback">Veuillez entrer votre nom.</div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Prénom</label>
                                <input required type="text" class="form-control" name="prenom" value="${Util.toUTF8(professeur.prenom)}">
                                <div class="invalid-feedback">Veuillez entrer votre pr&eacute;nom.</div>
                            </div>
                        </div>
                    </article>
                </section>
                
                <div class="form-group">
                    <label for="">Adresse courriel</label>
                    <label class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Format : abcd@nom-de-domaine.com"></label>                    
                    <input required type="email" class="form-control" name="email" value="${professeur.email}">
                    <div class="invalid-feedback">Veuillez entrer une adresse courriel valide.</div>
                </div>
                <div class="form-group">
                    <label for="">Numéro de téléphone</label>
                    <label class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Format : XXX-XXX-XXXX"></label>                    
                    <input required type="tel" class="form-control" name="tel" value="${professeur.telephone}" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}">
                    <div class="invalid-feedback">Veuillez entrer un num&eacute;ro de t&eacute;l&eacute;phone valide.</div>
                </div>
                <div class="form-group">
                    <label for="">Ville</label>
                    <input required type="text" class="form-control" name="ville" value="${Util.toUTF8(professeur.ville)}">
                    <div class="invalid-feedback">Veuillez entrer votre ville.</div>
                </div>
            </div>
        </article>

        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                <h1><strong>Langues</strong></h1>
            </div>

            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <label for="">Anglais écrit</label>
                    <select required class="form-control" name="anglaisEcrit" >
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListeChoixAnglais()}" var="choix">
                            <c:choose>
                                <c:when test="${professeur.anglaisEcrit == choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez entrer votre niveau d'anglais &eacute;crit.</div>
                </div>
                <div class="form-group">
                    <label for="">Anglais parlé</label>
                    <select required class="form-control" name="anglaisParle">
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListeChoixAnglais()}" var="choix">
                         6   <c:choose>
                                <c:when test="${professeur.anglaisParle== choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez entrer votre niveau d'anglais parl&eacute;.</div>
                </div>

            </div>
        </article>    

        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                <h1><strong>Préférences Stages</strong></h1>
            </div>

            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <label for="">Moyens de transport</label>
                    <select required class="form-control" name="moyenTransport">
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListeMoyenTransport()}" var="choix">
                            <c:choose>
                                <c:when test="${professeur.moyenTransport == choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez entrer votre moyen de transport.</div>
                </div>
                <div class="form-group">
                    <label for="">Informations partagées</label>
                    <select required class="form-control" name="infosPartagees">
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListeInfosPartagees()}" var="choix">
                            <c:choose>
                                <c:when test="${Util.toUTF8(professeur.infosPartagees) == choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez entrer les informations que vous d&eacute;sirez partager.</div>
                </div>
                <div class="form-group">
                    <label for="">Préférence de supervision</label>
                    <select required class="form-control" name="preferenceSupervision">
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListePreferenceSupervision()}" var="choix">
                            <c:choose>
                                <c:when test="${Util.toUTF8(professeur.preferenceSupervision) == choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez choisir votre pr&eacute;f&eacute;rence de supervision</div>
                </div>
                <div class="form-group">
                    <label for="">Disponible pour superviser un stage d'été</label>
                    <select required class="form-control" name="dispoStageEte">
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListeOuiNon()}" var="choix">
                            <c:choose>
                                <c:when test="${professeur.dispoStageEte == choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez sp&eacute;cifier votre disponibilt&eacute; pour un stage d'&eacute;t&eacute;.</div>
                </div>
                
            </div>
        </article>
        
        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                
            </div>
            
            <div class="col-12 col-lg-6 d-flex justify-content-between">
                <button type="submit" class="btn btn-success" name="tache" value="effectuerModificationUtilisateur">
                    Enregistrer les modifications
                </button>
                <button type="submit" class="btn btn-light" name="tache" value="afficherPageProfil">Annuler</button>
            </div>
            
        </article>
    </form>
</section>
