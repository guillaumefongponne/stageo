/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicole
 */
public class EtudiantTest {
    Etudiant instance;
    
    public EtudiantTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() throws Exception {
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test of Constructeur7Parametres method, of class Etudiant.
     */
    @Test
    public void testConstructeur7Parametres() {
        System.out.println("constructeur7Parametres");
        List<Critere> listeCritere = new LinkedList<Critere>();
        List<Candidature> liste_candidature = new LinkedList<Candidature>();
        instance = new Etudiant("123", "aaaa@gmail.com", "Anonyme",
                "Anonyme", "Moi-même", "Moi-même", "Etudiant");        
        assertEquals(instance.getIdUtilisateur(), "123");
        assertEquals(instance.getEmail(), "aaaa@gmail.com");
        assertEquals(instance.getNomUtilisateur() , "Anonyme");
        assertEquals(instance.getMotDePasse(), "Anonyme");
        assertEquals(instance.getNom() , "Moi-même");
        assertEquals(instance.getPrenom() , "Moi-même");
        assertEquals(instance.getRole() , "Etudiant");
    }

    /**
     * Test of setCv method, of class Etudiant.
     */
    @Test
    public void testSetCv() {
        System.out.println("setCv");
        CV cv = new CV();
        Etudiant instance = new Etudiant();
        instance.setCv(cv);
        
        assertEquals(instance.getCv() , cv);
    }

    /**
     * Test of setStatutRecherche method, of class Etudiant.
     */
    @Test
    public void testSetStatutRecherche() {
        System.out.println("setStatutRecherche");
        String statutRecherche = "En cours";
        Etudiant instance = new Etudiant();
        instance.setStatutRecherche(statutRecherche);
        
        assertEquals(instance.getStatutRecherche(), statutRecherche);
    }

    /**
     * Test of setListeCritere method, of class Etudiant.
     */
    @Test
    public void testSetListeCritere() {
        System.out.println("setListeCritere");
        List<Critere> liste_critere = new LinkedList<Critere>();
        Etudiant instance = new Etudiant();
        instance.setListeCritere(liste_critere);
        
        assertEquals(instance.getListeCritere() , liste_critere);
    }

    /**
     * Test of setListe_candidature method, of class Etudiant.
     */
    @Test
    public void testSetListe_candidature() {
        System.out.println("setListe_candidature");
        List<Candidature> liste_candidature = new LinkedList<Candidature>();
        Etudiant instance = new Etudiant();
        instance.setListe_candidature(liste_candidature);
        
        assertEquals(instance.getListe_candidature() , liste_candidature);
    }
    
    /**
     * Test of setVille method, of class Etudiant.
     */
    @Test
    public void testSetVille() {
        System.out.println("setVille");
        String ville = "Montreal";
        Etudiant instance = new Etudiant();
        instance.setVille(ville);
        
        assertEquals(instance.getVille() , ville);
    }

    /**
     * Test of setProfilEtudes method, of class Etudiant.
     */
    @Test
    public void testSetProfilEtudes() {
        System.out.println("setProfilEtudes");
        String profilEtudes = "profil";
        Etudiant instance = new Etudiant();
        instance.setProfilEtudes(profilEtudes);
        
        assertEquals(instance.getProfilEtudes() , profilEtudes);
    }

    /**
     * Test of setStageSession method, of class Etudiant.
     */
    @Test
    public void testSetStageSession() {
        System.out.println("setStageSession");
        String stageSession = "Automne19";
        Etudiant instance = new Etudiant();
        instance.setStageSession(stageSession);
        
        assertEquals(instance.getStageSession() , stageSession);
    }

    /**
     * Test of setAnglaisEcrit method, of class Etudiant.
     */
    @Test
    public void testSetAnglaisEcrit() {
        System.out.println("setAnglaisEcrit");
        String anglaisEcrit = "Moyen";
        Etudiant instance = new Etudiant();
        instance.setAnglaisEcrit(anglaisEcrit);
        
        assertEquals(instance.getAnglaisEcrit() , anglaisEcrit);
    }

    /**
     * Test of setAnglaisParle method, of class Etudiant.
     */
    @Test
    public void testSetAnglaisParle() {
        System.out.println("setAnglaisParle");
        String anglaisParle = "Moyen";
        Etudiant instance = new Etudiant();
        instance.setAnglaisParle(anglaisParle);
        
        assertEquals(instance.getAnglaisParle() , anglaisParle);
    }

    /**
     * Test of setPermisConduire method, of class Etudiant.
     */
    @Test
    public void testSetPermisConduire() {
        System.out.println("setPermisConduire");
        String permisConduire = "123456789";
        Etudiant instance = new Etudiant();
        instance.setPermisConduire(permisConduire);
        
        assertEquals(instance.getPermisConduire() , permisConduire);
    }

    /**
     * Test of setDemarcheEnCours method, of class Etudiant.
     */
    @Test
    public void testSetDemarcheEnCours() {
        System.out.println("setDemarcheEnCours");
        String demarcheEnCours = "Demarche";
        Etudiant instance = new Etudiant();
        instance.setDemarcheEnCours(demarcheEnCours);
        
        assertEquals(instance.getDemarcheEnCours() , demarcheEnCours);
    }

    /**
     * Test of setLogin365 method, of class Etudiant.
     */
    @Test
    public void testSetLogin365() {
        System.out.println("setLogin365");
        String login365 = "987654321";
        Etudiant instance = new Etudiant();
        instance.setLogin365(login365);
        
        assertEquals(instance.getLogin365() , login365);
    }

    /**
     * Test of setNumeroDA method, of class Etudiant.
     */
    @Test
    public void testSetNumeroDA() {
        System.out.println("setNumeroDA");
        String numeroDA = "1676830";
        Etudiant instance = new Etudiant();
        instance.setNumeroDA(numeroDA);
        
        assertEquals(instance.getNumeroDA() , numeroDA);
    }

    /**
     * Test of setMoyenTransport method, of class Etudiant.
     */
    @Test
    public void testSetMoyenTransport() {
        System.out.println("setMoyenTransport");
        String moyenTransport = "public";
        Etudiant instance = new Etudiant();
        instance.setMoyenTransport(moyenTransport);
        
        assertEquals(instance.getMoyenTransport() , moyenTransport);
    }

    /**
     * Test of setPreferences method, of class Etudiant.
     */
    @Test
    public void testSetPreferences() {
        System.out.println("setPreferences");
        String preferences = "php";
        Etudiant instance = new Etudiant();
        instance.setPreferences(preferences);
        
        assertEquals(instance.getPreferences() , preferences);
    }

    /**
     * Test of setProfilCree method, of class Etudiant.
     */
    @Test
    public void testSetProfilCree() {
        System.out.println("setProfilCree");
        String profilCree = "profil";
        Etudiant instance = new Etudiant();
        instance.setProfilCree(profilCree);
        
        assertEquals(instance.getProfilCree() , profilCree);
    }

    /**
     * Test of setProfilModifie method, of class Etudiant.
     */
    @Test
    public void testSetProfilModifie() {
        System.out.println("setProfilModifie");
        String profilModifie = "modifie";
        Etudiant instance = new Etudiant();
        instance.setProfilModifie(profilModifie);
        
        assertEquals(instance.getProfilModifie() , profilModifie);
    }
    
}
