/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicole
 */
public class CompagnieTest {
    Compagnie instance;
    
    public CompagnieTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() throws Exception {
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test of Constructeur4Parametres method, of class Compagnie.
     */
    @Test
    public void testConstructeur4Parametres() {
        System.out.println("constructeur4Parametres");
        Adresse ad = new Adresse();
        instance = new Compagnie("1234", "Exemple", "www.exemple.com", ad);
        
        assertEquals(instance.getIdCompagnie(), "1234");
        assertEquals(instance.getNom() , "Exemple");
        assertEquals(instance.getPageWeb() , "www.exemple.com");
        assertEquals(instance.getAdresse() , ad);
    }
    
     /**
     * Test of Constructeur3Parametres method, of class Compagnie.
     */
    @Test
    public void testConstructeur3Parametres() {
        System.out.println("constructeur3Parametres");
        String uniqueID = UUID.randomUUID().toString();
        Adresse adr = new Adresse();
        instance = new Compagnie("Exemple", "www.exemple.com", adr);
        
        assertEquals(instance.getNom() , "Exemple");
        assertEquals(instance.getPageWeb() , "www.exemple.com");
        assertEquals(instance.getAdresse() , adr);
    }

    /**
     * Test of setIdCompagnie method, of class Compagnie.
     */
    @org.junit.Test
    public void testSetIdCompagnie() {
        System.out.println("setIdCompagnie");
        String idCompagnie = "";
        Compagnie instance = new Compagnie();
        instance.setIdCompagnie(idCompagnie);
        
        assertEquals(instance.getIdCompagnie(), idCompagnie);
    }

    /**
     * Test of setNom method, of class Compagnie.
     */
    @org.junit.Test
    public void testSetNom() {
        System.out.println("setNom");
        String nom = "Nom";
        Compagnie instance = new Compagnie();
        instance.setNom(nom);
        
        assertEquals(instance.getNom(), "Nom");
    }

    /**
     * Test of setPageWeb method, of class Compagnie.
     */
    @org.junit.Test
    public void testSetPageWeb() {
        System.out.println("setPageWeb");
        String pageWeb = "www.pageweb.com";
        Compagnie instance = new Compagnie();
        instance.setPageWeb(pageWeb);
        
        assertEquals(instance.getPageWeb(), "www.pageweb.com");
    }

    /**
     * Test of setAdresse method, of class Compagnie.
     */
    @org.junit.Test
    public void testSetAdresse() {
        System.out.println("setAdresse");
        Adresse adresse = new Adresse();
        Compagnie instance = new Compagnie();
        instance.setAdresse(adresse);
        
        assertEquals(instance.getAdresse(), adresse);
    }

    /**
     * Test of setSecteur method, of class Compagnie.
     */
    @org.junit.Test
    public void testSetSecteur() {
        System.out.println("setSecteur");
        String secteur = "informatique";
        Compagnie instance = new Compagnie();
        instance.setSecteur(secteur);
        
        assertEquals(instance.getSecteur(), secteur);
    }

    /**
     * Test of setTelEntreprise method, of class Compagnie.
     */
    @org.junit.Test
    public void testSetTelEntreprise() {
        System.out.println("setTelEntreprise");
        String telEntreprise = "514-888-8888";
        Compagnie instance = new Compagnie();
        instance.setTelEntreprise(telEntreprise);
        
        assertEquals(instance.getTelEntreprise(), telEntreprise);
    }

    /**
     * Test of setLienGoogleMaps method, of class Compagnie.
     */
    @org.junit.Test
    public void testSetLienGoogleMaps() {
        System.out.println("setLienGoogleMaps");
        String lienGoogleMaps = "www.lienGoogleMaps.com";
        Compagnie instance = new Compagnie();
        instance.setLienGoogleMaps(lienGoogleMaps);
        
        assertEquals(instance.getLienGoogleMaps(), lienGoogleMaps);
    }

    /**
     * Test of setKilometrage method, of class Compagnie.
     */
    @org.junit.Test
    public void testSetKilometrage() {
        System.out.println("setKilometrage");
        int kilometrage = 80;
        Compagnie instance = new Compagnie();
        instance.setKilometrage(kilometrage);
        
        assertEquals(instance.getKilometrage(), kilometrage);
    }

    /**
     * Test of setTaille method, of class Compagnie.
     */
    @org.junit.Test
    public void testSetTaille() {
        System.out.println("setTaille");
        String taille = "Grande";
        Compagnie instance = new Compagnie();
        instance.setTaille(taille);
        
        assertEquals(instance.getTaille(), taille);
    }

    /**
     * Test of setNbEmployes method, of class Compagnie.
     */
    @org.junit.Test
    public void testSetNbEmployes() {
        System.out.println("setNbEmployes");
        int nbEmployes = 250;
        Compagnie instance = new Compagnie();
        instance.setNbEmployes(nbEmployes);
        
        assertEquals(instance.getNbEmployes(), nbEmployes);
    }
    
}
