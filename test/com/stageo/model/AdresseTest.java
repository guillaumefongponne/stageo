/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicole
 */
public class AdresseTest {
    Adresse instance;
    
    public AdresseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() throws Exception {
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test of Constructeur6Parametres method, of class Adresse.
     */
    @Test
    public void testConstructeur6Parametres() {
        System.out.println("constructeur6Parametres");
        String uniqueID = UUID.randomUUID().toString();
        instance = new Adresse(uniqueID, "481 rue Blanche Thibaudeau", "6055 28eme Avenue",
                "Terrebonne", "Québec", "J6Y 1Z2", "Canada");        
        assertEquals(instance.getIdAdresse(), uniqueID);
        assertEquals(instance.getAdresseLigne1() , "481 rue Blanche Thibaudeau");
        assertEquals(instance.getAdresseLigne2() , "6055 28eme Avenue");
        assertEquals(instance.getVille() , "Terrebonne");
        assertEquals(instance.getProvince() , "Québec");
        assertEquals(instance.getCodePostal() , "J6Y 1Z2");
        assertEquals(instance.getPays() , "Canada");
    }
    
    /**
     * Test of Constructeur7Parametres method, of class Adresse.
     */
    @Test
    public void testConstructeur7Parametres() {
        System.out.println("constructeur7Parametres");        
        instance = new Adresse("123", "481 rue Blanche Thibaudeau", "6055 28eme Avenue",
                "Terrebonne", "Québec", "J6Y 1Z2", "Canada");        
        assertEquals(instance.getIdAdresse(), "123");
        assertEquals(instance.getAdresseLigne1() , "481 rue Blanche Thibaudeau");
        assertEquals(instance.getAdresseLigne2() , "6055 28eme Avenue");
        assertEquals(instance.getVille() , "Terrebonne");
        assertEquals(instance.getProvince() , "Québec");
        assertEquals(instance.getCodePostal() , "J6Y 1Z2");
        assertEquals(instance.getPays() , "Canada");
    }

    /**
     * Test of setIdAdresse method, of class Adresse.
     */
    @Test
    public void testSetIdAdresse() {
        System.out.println("setIdAdresse");
        String idAdresse = "";
        Adresse instance = new Adresse();
        instance.setIdAdresse(idAdresse);
        
        assertEquals(instance.getIdAdresse(), idAdresse);
    }

    /**
     * Test of setAdresseLigne1 method, of class Adresse.
     */
    @Test
    public void testSetAdresseLigne1() {
        System.out.println("setAdresseLigne1");
        String adresseLigne1 = "6400 16eme Avenue";
        Adresse instance = new Adresse();
        instance.setAdresseLigne1(adresseLigne1);
        
        assertEquals(instance.getAdresseLigne1(), adresseLigne1);
    }

    /**
     * Test of setAdresseLigne2 method, of class Adresse.
     */
    @Test
    public void testSetAdresseLigne2() {
        System.out.println("setAdresseLigne2");
        String adresseLigne2 = "258 26eme Avenue";
        Adresse instance = new Adresse();
        instance.setAdresseLigne2(adresseLigne2);
        
        assertEquals(instance.getAdresseLigne2(), adresseLigne2);
    }

    /**
     * Test of setVille method, of class Adresse.
     */
    @Test
    public void testSetVille() {
        System.out.println("setVille");
        String ville = "Montréeal";
        Adresse instance = new Adresse();
        instance.setVille(ville);
        
        assertEquals(instance.getVille(), ville);
    }

    /**
     * Test of setProvince method, of class Adresse.
     */
    @Test
    public void testSetProvince() {
        System.out.println("setProvince");
        String province = "Quebec";
        Adresse instance = new Adresse();
        instance.setProvince(province);
        
        assertEquals(instance.getProvince(), province);
    }

    /**
     * Test of setCodePostal method, of class Adresse.
     */
    @Test
    public void testSetCodePostal() {
        System.out.println("setCodePostal");
        String codePostal = "H1T 3H7";
        Adresse instance = new Adresse();
        instance.setCodePostal(codePostal);
        
        assertEquals(instance.getCodePostal(), codePostal);
    }

    /**
     * Test of setPays method, of class Adresse.
     */
    @Test
    public void testSetPays() {
        System.out.println("setPays");
        String pays = "Canada";
        Adresse instance = new Adresse();
        instance.setPays(pays);
        
        assertEquals(instance.getPays(), pays);
    }
    
}
