/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.io.InputStream;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicole
 */
public class OffreStageTest {
    OffreStage instance;
    
    public OffreStageTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() throws Exception {
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test of ConstructeurAvecParametres method, of class OffreStage.
     */
    @Test
    public void testConstructeurAvecParametres() {
        System.out.println("constructeurAvecParametres");
        Timestamp datedebut, datefin, date;
        datedebut = Timestamp.valueOf(LocalDateTime.of(2020, 01, 06, 8, 00));
        datefin = Timestamp.valueOf(LocalDateTime.of(2020, 06, 06, 15, 00));
        date = Timestamp.valueOf(LocalDateTime.now());
        
        instance = new OffreStage("PHP", "Stage en PHP", "www.php.com", "Aaae", "123456ed", datedebut, 
                datefin, date, 8, 1);        
        assertEquals(instance.getTitre(), "PHP");
        assertEquals(instance.getDescription(), "Stage en PHP");
        assertEquals(instance.getLienWeb() , "www.php.com");
        assertEquals(instance.getLienDocument(), "Aaae");
        assertEquals(instance.getIdEmployeur() , "123456ed");
        assertEquals(instance.getDateDebut() , datedebut);
        assertEquals(instance.getDateFin() , datefin);
        assertEquals(instance.getDate() , date);
        assertEquals(instance.getNbVues() , 8);
        assertEquals(instance.getActive() , 1);
        
    }

    /**
     * Test of setStatutOffre method, of class OffreStage.
     */
    @Test
    public void testSetStatutOffre() {
        System.out.println("setStatutOffre");
        boolean statutOffre = true;
        OffreStage instance = new OffreStage();
        instance.setStatutOffre(statutOffre);
        
        assertEquals(instance.isStatutOffre() , statutOffre);
    }

    /**
     * Test of setDateDebut method, of class OffreStage.
     */
    @Test
    public void testSetDateDebut() {
        System.out.println("setDateDebut");
        Timestamp dateDebut = Timestamp.valueOf(LocalDateTime.of(2020, 01, 06, 8, 00));
        OffreStage instance = new OffreStage();
        instance.setDateDebut(dateDebut);
        
        assertEquals(instance.getDateDebut() , dateDebut);
    }

    /**
     * Test of setDateFin method, of class OffreStage.
     */
    @Test
    public void testSetDateFin() {
        System.out.println("setDateFin");
        Timestamp dateFin = Timestamp.valueOf(LocalDateTime.of(2020, 06, 06, 15, 00));
        OffreStage instance = new OffreStage();
        instance.setDateFin(dateFin);
        
        assertEquals(instance.getDateFin() , dateFin);
    }

    /**
     * Test of setDureeEnJours method, of class OffreStage.
     */
    @Test
    public void testSetDureeEnJours() {
        System.out.println("setDureeEnJours");
        int dureeEnJours = 60;
        OffreStage instance = new OffreStage();
        instance.setDureeEnJours(dureeEnJours);
        
        assertEquals(instance.getDureeEnJours() , dureeEnJours);
        
    }

    /**
     * Test of setRemunere method, of class OffreStage.
     */
    @Test
    public void testSetRemunere() {
        System.out.println("setRemunere");
        int remunere = 20;
        OffreStage instance = new OffreStage();
        instance.setRemunere(remunere);
        
        assertEquals(instance.getRemunere() , remunere);
    }

    /**
     * Test of setIdOffre method, of class OffreStage.
     */
    @Test
    public void testSetIdOffre() {
        System.out.println("setIdOffre");
        String idOffre = "89523";
        OffreStage instance = new OffreStage();
        instance.setIdOffre(idOffre);
        
        assertEquals(instance.getIdOffre() , idOffre);
    }

    /**
     * Test of setTitre method, of class OffreStage.
     */
    @Test
    public void testSetTitre() {
        System.out.println("setTitre");
        String titre = "Java";
        OffreStage instance = new OffreStage();
        instance.setTitre(titre);
        
        assertEquals(instance.getTitre() , titre);
    }

    /**
     * Test of setDescription method, of class OffreStage.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "Java programmation";
        OffreStage instance = new OffreStage();
        instance.setDescription(description);
        
        assertEquals(instance.getDescription() , description);
    }

    /**
     * Test of setLienWeb method, of class OffreStage.
     */
    @Test
    public void testSetLienWeb() {
        System.out.println("setLienWeb");
        String lienWeb = "www.what.com";
        OffreStage instance = new OffreStage();
        instance.setLienWeb(lienWeb);
        
        assertEquals(instance.getLienWeb() , lienWeb);
    }

    /**
     * Test of setLienDocument method, of class OffreStage.
     */
    @Test
    public void testSetLienDocument() {
        System.out.println("setLienDocument");
        String lienDocument = "abcdef";
        OffreStage instance = new OffreStage();
        instance.setLienDocument(lienDocument);
        
        assertEquals(instance.getLienDocument() , lienDocument);
    }

    /**
     * Test of setIdEmployeur method, of class OffreStage.
     */
    @Test
    public void testSetIdEmployeur() {
        System.out.println("setIdEmployeur");
        String idEmployeur = "452369hg";
        OffreStage instance = new OffreStage();
        instance.setIdEmployeur(idEmployeur);
        
        assertEquals(instance.getIdEmployeur() , idEmployeur);
    }

    /**
     * Test of setDate method, of class OffreStage.
     */
    @Test
    public void testSetDate() {
        System.out.println("setDate");
        Timestamp date = Timestamp.valueOf(LocalDateTime.now());
        OffreStage instance = new OffreStage();
        instance.setDate(date);
        
        assertEquals(instance.getDate() , date);
    }

    /**
     * Test of setNbVues method, of class OffreStage.
     */
    @Test
    public void testSetNbVues() {
        System.out.println("setNbVues");
        int nbVues = 10;
        OffreStage instance = new OffreStage();
        instance.setNbVues(nbVues);
        
        assertEquals(instance.getNbVues() , nbVues);
    }
    /**
     * Test of setActive method, of class OffreStage.
     */
    @Test
    public void testSetActive() {
        System.out.println("setActive");
        int active = 0;
        OffreStage instance = new OffreStage();
        instance.setActive(active);
        
        assertEquals(instance.getActive() , active);
    }

    /**
     * Test of setFichier method, of class OffreStage.
     */
    @Test
    public void testSetFichier() {
        System.out.println("setFichier");
        InputStream fichier = null;
        OffreStage instance = new OffreStage();
        instance.setFichier(fichier);
        
        assertEquals(instance.getFichier() , fichier);
    }

    /**
     * Test of setListeCritere method, of class OffreStage.
     */
    @Test
    public void testSetListeCritere() {
        System.out.println("setListeCritere");
        List<Critere> liste_critere = new LinkedList<Critere>();
        Etudiant instance = new Etudiant();
        instance.setListeCritere(liste_critere);
        
        assertEquals(instance.getListeCritere() , liste_critere);
    }

    /**
     * Test of setStatut method, of class OffreStage.
     */
    @Test
    public void testSetStatut() {
        System.out.println("setStatut");
        int statut = 0;
        OffreStage instance = new OffreStage();
        instance.setStatut(statut);
        
        assertEquals(instance.getStatut() , statut);
    }
    
}
